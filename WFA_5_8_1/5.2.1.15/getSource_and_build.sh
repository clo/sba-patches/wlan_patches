#!/bin/bash
#Copyright © 2018, Qualcomm Innovation Center, Inc. All rights reserved.  Confidential and proprietary.
#Copyright © 2018, Intrinsyc Technologies Corporation.
#$DateTime: 2019/01/16 12:00:00 $

UNDER='\e[4m'
RED='\e[31;1m'
GREEN='\e[32;1m'
YELLOW='\e[33;1m'
BLUE='\e[34;1m'
MAGENTA='\e[35;1m'
CYAN='\e[36;1m'
WHITE='\e[37;1m'
ENDCOLOR='\e[0m'
ITCVER="O_v2.0"
WORKDIR=`pwd`
CAFTAG="LA.UM.6.4.r1-06900-8x98.0"
BUILDROOT="${WORKDIR}/APQ8098_${CAFTAG}_OpenQ835-${ITCVER}"
PATCH_DIR="${WORKDIR}/patches"
WLAN_PATCH_DIR="$WORKDIR/wlan_patches"
WLAN_PROP_PATCH_DIR="$WORKDIR/wlan_prop_patches"
DB_PRODUCT_STRING="APQ8098 Snapdragon 835 OpenQ Development Board"

# Find absolute path for the script
pushd $(dirname $0) > /dev/null 2>&1
SCRIPTSDIR=$(pwd)
popd 2> /dev/null 2>&1
MANIFEST_SCRIPT="${SCRIPTSDIR}/manifestUpdate_WFA.sh"

# how many processors to use, use them all
NPROC=`/usr/bin/nproc`

function manifest_update() {
	MANIFEST=$1
	# Replace CAF code with custom code
	echo ""
	echo "#####################################################################"
	echo "[$(date '+%Y/%m/%d %H:%M:%S')] Updating manifest with custom projects"
	echo "#####################################################################"
	echo ""
	if [ ! -f "${MANIFEST}" ]; then
		echo "ERROR: Missing ${MANIFEST}"
		exit 1
	fi
	cp -fpv ${MANIFEST} ${MANIFEST}.orig.$$
	pwd
	echo "$GREEN INFO: bash $MANIFEST_SCRIPT ${MANIFEST}"
	bash $MANIFEST_SCRIPT ${MANIFEST}
	if [ "$?" != "0" -o ! -s ${MANIFEST}.out ]; then
		echo "$RED ERROR: $MANIFEST_SCRIPT FAILED or ${MANIFEST}.out is empty"
		exit 1
	fi
	cp -fpv ${MANIFEST}.out ${MANIFEST}
}

function download_CAF_CODE() {
# Do repo sanity test
if [ $? -eq 0 ]
then
	echo "Downloading code please wait.."
	repo init -q -u git://codeaurora.org/platform/manifest.git -b release -m ${CAFTAG}.xml
	if [ $? -eq 0 ]
	then
		echo -e "$GREEN Repo initialization done..$ENDCOLOR"
	else
		echo -e "$RED!!!Error Repo initialization !!!$ENDCOLOR"
		echo
		exit 1
	fi
	
	echo "INFO: Customizing manifest for wlan projects in ${BUILDROOT}/.repo/manifests/${CAFTAG}.xml"
	manifest_update "${BUILDROOT}/.repo/manifests/${CAFTAG}.xml"
	

	echo "INFO: Syncing $CAFTAG sources"
	repo sync -cj${NPROC} --no-tags

	if [ $? -eq 0 ]
	then
		echo -e "$GREEN Downloading done..$ENDCOLOR"
	else
		echo -e "$RED!!!Error Downloading code!!!$ENDCOLOR"
		echo
		exit 1
	fi	
else
	echo "repo tool problem, make sure you have setup your build environment"
	echo "1) http://source.android.com/source/initializing.html"
	echo "2) http://source.android.com/source/downloading.html (Installing Repo Section Only)"
	exit -1
fi
}

#  Function to check result for failures
check_result() {
if [ $? -ne 0 ]
then
	echo
	echo -e "$RED FAIL: Current working dir:$(pwd) $ENDCOLOR"
	echo
	exit 1
else
	echo -e "$GREEN DONE! $ENDCOLOR"
fi
}

# Function to autoapply patches to CAF code
apply_android_patches()
{

	echo "Applying patches ..."
	if [ ! -e $PATCH_DIR ]
	then
		echo -e "$RED $PATCH_DIR : Not Found $ENDCOLOR"
		return
	fi
	cd $PATCH_DIR
	patch_root_dir="$PATCH_DIR"
	android_patch_list=$(find . -type f -name "*.patch" | sort) &&
	for android_patch in $android_patch_list; do
		android_project=$(dirname $android_patch)
		echo -e "$YELLOW   applying patches on $android_project ... $ENDCOLOR"
		cd $BUILDROOT/$android_project
		if [ $? -ne 0 ]; then
			echo -e "$RED $android_project does not exist in BUILDROOT:$BUILDROOT $ENDCOLOR"
			exit 1
		fi
		git am --3way $patch_root_dir/$android_patch
		check_result
	done
}

# Function to autoapply qualcomm wlan patches to CAF code
apply_wlan_patches()
{

	echo "Applying WLAN patches ..."
	if [ ! -e $WLAN_PATCH_DIR ]
	then
		echo -e "$RED $WLAN_PATCH_DIR : Not Found $ENDCOLOR"
		exit 1
	fi
	cd $WLAN_PATCH_DIR
	patch_root_dir="$WLAN_PATCH_DIR"
	wlan_patch_list=$(find . -type f -name "*.patch" | sort) &&
	for wlan_patch in $wlan_patch_list; do
		wlan_project=$(dirname $wlan_patch)
		echo -e "$YELLOW   applying wlan patches on $android_project ... $ENDCOLOR"
		cd $BUILDROOT/$wlan_project
		if [ $? -ne 0 ]; then
			echo -e "$RED $wlan_project does not exist in BUILDROOT:$BUILDROOT $ENDCOLOR"
			exit 1
		fi
		echo Install $patch_root_dir/$wlan_patch in `pwd`
		echo Executing git am $patch_root_dir/$wlan_patch
		git am $patch_root_dir/$wlan_patch
		check_result
	done

	echo "Fixing supplicant src symlink ..."
	if [ -d "${BUILDROOT}/external/wpa_supplicant_8" ]; then
		pushd $BUILDROOT/external/wpa_supplicant_8/hostapd
		if [ ! -L "src" ]; then ln -sv ../src src; fi
		popd
		pushd $BUILDROOT/external/wpa_supplicant_8/wpa_supplicant
		if [ ! -L "src" ]; then ln -sv ../src src; fi
		popd
	else
		echo -e "$RED $BUILDROOT/external/wpa_supplicant_8 : Not Found $ENDCOLOR"
		exit 1
	fi
}

#  Function to check whether host utilities exists
check_program() {
for cmd in "$@"
do
	which ${cmd} > /dev/null 2>&1
	if [ $? -ne 0 ]
	then
		echo
		echo -e "$RED Cannot find command \"${cmd}\" $ENDCOLOR"
		echo
		exit 1
	fi
done
}

#Main Script starts here
#Note: Check necessary program for installation
echo
echo -e "$CYAN Product                   : $DB_PRODUCT_STRING $ENDCOLOR"
echo -e "$MAGENTA Intrinsyc Release Version : $ITCVER $ENDCOLOR"
echo -e "$MAGENTA WorkDir                   : $WORKDIR $ENDCOLOR"
echo -e "$MAGENTA Build Root                : $BUILDROOT $ENDCOLOR"
echo -e "$MAGENTA Patch Dir                 : $PATCH_DIR $ENDCOLOR"
echo -e "$MAGENTA CodeAurora TAG            : $CAFTAG $ENDCOLOR"
echo -n "Checking necessary program for installation......"
echo
check_program tar repo git patch
if [ -e $BUILDROOT ]
then
	cd $BUILDROOT
else
	mkdir $BUILDROOT
	cd $BUILDROOT
fi

#1 Download code
download_CAF_CODE
cd $BUILDROOT

#2 Apply OpenQ Snapdragon 835 Development Patches
apply_android_patches

#2.1 Apply OpenQ Snapdragon 835 Development wlan Patches
apply_wlan_patches

#3 Extract the proprietary objs
cd $BUILDROOT
echo -e "$YELLOW   Extracting proprietary binary package to $BUILDROOT ... $ENDCOLOR"
tar -xzvf ../proprietary.tar.gz -C vendor/qcom/

#4 Build
echo -e "$YELLOW   Building Source code from $BUILDROOT ... $ENDCOLOR"
if [[ -z "${BUILD_NUMBER}" ]]; then export BUILD_NUMBER=$(date +%m%d%H%M); fi
ITC_ID=OpenQ835-${ITCVER} ./build.sh msm8998 -j $(nproc) -s 0 $@
