#!/bin/bash
#
# SCRIPT TO OVERRIDE PROJECTS IN A GIVEN MANIFEST .XML FILE
#
# Contact: Pdhavali@qti.qualcomm.com
#
# $Id: //depot/software/swbuild/bin/la_integ/utils/WFA/OpenQ835/manifestUpdate_WFA.sh#1 $
# $DateTime: 2018/02/07 12:53:22 $
# $Author: pdhavali $
#
# BUILD13_EXTERNAL

input_file=$1
output_file="${input_file}.out"

# Array to hold projects that need to be replaced or added to defaut manifest.xml
declare -A replace

# Replace manifest entries for these projects
# replace["kernel/msm-4.4"]='<project name="kernel/msm-4.4" revision="1f3c49d418238e20a9eb256920b8f6fb149643bb" upstream="refs/heads/kernel.lnx.4.4-r15-rel"/>'

replace["platform/external/wpa_supplicant_8"]='<project name="hostap" remote="supp" path="external/wpa_supplicant_8" revision="a22e235fd0df7005dcb26e55f8b537fe182b93ad" upstream="master"/>'

# replace["platform/vendor/qcom/cobalt"]='<project name="platform/vendor/qcom/cobalt" remote="caf" path="device/qcom/msm8998" revision="f4eef25c34eb1710ff5abe5bb1f0bbe42747af03" upstream="refs/heads/qcom-devices.lnx.1.0-r15-rel"/>'

replace["platform/vendor/qcom-opensource/wlan/utils/sigma-dut"]='<project name="qca/sigma-dut" remote="github" path="vendor/qcom/opensource/wlan/utils/sigma-dut" revision="a119988446e323afeb9ae35aaea181bf46fe7c12" upstream="master"/>'

replace["platform/vendor/qcom-opensource/wlan/qcacld-3.0"]='<project name="platform/vendor/qcom-opensource/wlan/qcacld-3.0" remote="caf" path="vendor/qcom/opensource/wlan/qcacld-3.0" revision="9dae0a4488df25e64a8b14a9ea0265cc4d4fe5ac" upstream="refs/heads/LA.HB.1.1.5_rb1.16"/>'

replace["platform/vendor/qcom-opensource/wlan/qca-wifi-host-cmn"]='<project name="platform/vendor/qcom-opensource/wlan/qca-wifi-host-cmn" remote="caf" path="vendor/qcom/opensource/wlan/qca-wifi-host-cmn" revision="d8532ff2a05a4e7f29433f275530c9ab1c0c2b85" upstream="refs/heads/LA.HB.1.1.5_rb1.16"/>'

replace["platform/vendor/qcom-opensource/wlan/fw-api"]='<project name="platform/vendor/qcom-opensource/wlan/fw-api" remote="caf" path="vendor/qcom/opensource/wlan/fw-api" revision="aa7e8919450582c0ebfee942bb418b58677594e6" upstream="refs/heads/LA.HB.1.1.5_rb1.16"/>'


tmpfile=$(mktemp /tmp/prjtmpXXXX)
rm -fv $output_file

while IFS= read -r line
do

	# Skip </manifest> until all lines are spit out
	if echo "$line" | egrep -q "</manifest>";
	then
		continue
	fi

	# Process only <project line and for others show them as-is
	if echo "$line" | egrep -qv "<project ";
	then
		echo "$line" >> $output_file
		continue
	fi

	linetmp=$(echo "$line" | sed -e 's/"//g')

	echo "$linetmp" | fmt -1 | grep name= | awk -F= '{print $2}' > $tmpfile
	project=$(cat $tmpfile | col -b)

	# If project needs to be replacen, skip it here
	if [ "${replace[$project]}" == "" ]; then
		echo "$line" >> $output_file
	fi
done < "$input_file"
rm -f $tmpfile

# Name=caf pointing to https://source.codeaurora.org/quic/la/ already exists in $input_file
echo ' <!-- START: Qualcomm additional project mappings -->'             >> $output_file
echo ' <remote fetch="git://git-android.quicinc.com/" name="qcremote"/>' >> $output_file
echo ' <remote fetch="git://github.com/" name="github"/>'                >> $output_file
echo ' <remote fetch="git://w1.fi/" name="supp" />'                      >> $output_file

# Finally append the new projects that are not in CAF
for prj in ${!replace[@]}
do
	echo "  ${replace[$prj]}"                            >> $output_file
done
echo ' <!-- END  : Qualcomm additional project mappings -->' >> $output_file
echo "</manifest>"                                           >> $output_file

