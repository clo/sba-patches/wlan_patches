From 21f410efb38c855b18d688dda59b5935b6242079 Mon Sep 17 00:00:00 2001
From: Anurag Das <anurdas@codeaurora.org>
Date: Mon, 23 Jul 2018 16:49:19 +0530
Subject: [PATCH] Add support for Transport discovery AD type (1/2)

Add support for Transport discovery AD type in
BLE advertisement data.

Change-Id: I76ef89856746ea5d88af77142a38398b33393449
---
 core/java/android/bluetooth/le/AdvertiseData.java  | 45 +++++++++++++++++++---
 .../android/bluetooth/le/AdvertiseData.java.rej    | 12 ++++++
 .../android/bluetooth/le/BluetoothLeUtils.java     | 41 ++++++++++++++++++++
 3 files changed, 93 insertions(+), 5 deletions(-)
 create mode 100644 core/java/android/bluetooth/le/AdvertiseData.java.rej

diff --git a/core/java/android/bluetooth/le/AdvertiseData.java b/core/java/android/bluetooth/le/AdvertiseData.java
index ff0db9a..5bdd27e 100644
--- a/core/java/android/bluetooth/le/AdvertiseData.java
+++ b/core/java/android/bluetooth/le/AdvertiseData.java
@@ -47,17 +47,20 @@ public final class AdvertiseData implements Parcelable {
     private final Map<ParcelUuid, byte[]> mServiceData;
     private final boolean mIncludeTxPowerLevel;
     private final boolean mIncludeDeviceName;
+    private final byte[] mTransportDiscoveryData;
 
     private AdvertiseData(List<ParcelUuid> serviceUuids,
             SparseArray<byte[]> manufacturerData,
             Map<ParcelUuid, byte[]> serviceData,
             boolean includeTxPowerLevel,
-            boolean includeDeviceName) {
+            boolean includeDeviceName,
+            byte[] transportDiscoveryData) {
         mServiceUuids = serviceUuids;
         mManufacturerSpecificData = manufacturerData;
         mServiceData = serviceData;
         mIncludeTxPowerLevel = includeTxPowerLevel;
         mIncludeDeviceName = includeDeviceName;
+        mTransportDiscoveryData = transportDiscoveryData;
     }
 
     /**
@@ -98,12 +101,20 @@ public final class AdvertiseData implements Parcelable {
     }
 
     /**
+     * Returns an array of Transport Discovery data.
+     * @hide
+     */
+    public byte[] getTransportDiscoveryData() {
+        return mTransportDiscoveryData;
+    }
+
+    /**
      * @hide
      */
     @Override
     public int hashCode() {
         return Objects.hash(mServiceUuids, mManufacturerSpecificData, mServiceData,
-                mIncludeDeviceName, mIncludeTxPowerLevel);
+                mIncludeDeviceName, mIncludeTxPowerLevel, mTransportDiscoveryData);
     }
 
     /**
@@ -122,7 +133,8 @@ public final class AdvertiseData implements Parcelable {
                 BluetoothLeUtils.equals(mManufacturerSpecificData, other.mManufacturerSpecificData) &&
                 BluetoothLeUtils.equals(mServiceData, other.mServiceData) &&
                         mIncludeDeviceName == other.mIncludeDeviceName &&
-                        mIncludeTxPowerLevel == other.mIncludeTxPowerLevel;
+                        mIncludeTxPowerLevel == other.mIncludeTxPowerLevel &&
+                        BluetoothLeUtils.equals(mTransportDiscoveryData, other.mTransportDiscoveryData);
     }
 
     @Override
@@ -131,7 +143,8 @@ public final class AdvertiseData implements Parcelable {
                 + BluetoothLeUtils.toString(mManufacturerSpecificData) + ", mServiceData="
                 + BluetoothLeUtils.toString(mServiceData)
                 + ", mIncludeTxPowerLevel=" + mIncludeTxPowerLevel + ", mIncludeDeviceName="
-                + mIncludeDeviceName + "]";
+                + mIncludeDeviceName + ", mTransportDiscoveryData="
+                + BluetoothLeUtils.toString(mTransportDiscoveryData)+ "]";
     }
 
     @Override
@@ -170,6 +183,10 @@ public final class AdvertiseData implements Parcelable {
         }
         dest.writeByte((byte) (getIncludeTxPowerLevel() ? 1 : 0));
         dest.writeByte((byte) (getIncludeDeviceName() ? 1 : 0));
+        if(mTransportDiscoveryData != null) {
+            dest.writeInt(mTransportDiscoveryData.length);
+            dest.writeByteArray(mTransportDiscoveryData);
+        }
     }
 
     public static final Parcelable.Creator<AdvertiseData> CREATOR =
@@ -212,6 +229,11 @@ public final class AdvertiseData implements Parcelable {
                     }
                     builder.setIncludeTxPowerLevel(in.readByte() == 1);
                     builder.setIncludeDeviceName(in.readByte() == 1);
+                    int transportDiscoveryDataSize = in.readInt();
+                    if (transportDiscoveryDataSize > 0) {
+                        byte[] transportDiscoveryData = in.createByteArray();
+                        builder.addTransportDiscoveryData(transportDiscoveryData);
+                    }
                     return builder.build();
                 }
             };
@@ -226,6 +248,7 @@ public final class AdvertiseData implements Parcelable {
         private Map<ParcelUuid, byte[]> mServiceData = new ArrayMap<ParcelUuid, byte[]>();
         private boolean mIncludeTxPowerLevel;
         private boolean mIncludeDeviceName;
+        private byte[] mTransportDiscoveryData;
 
         /**
          * Add a service UUID to advertise data.
@@ -300,11 +323,23 @@ public final class AdvertiseData implements Parcelable {
         }
 
         /**
+         * Add Transport Discovery data
+         * @hide
+         */
+        public Builder addTransportDiscoveryData(byte[] transportDiscoveryData) {
+            if ((transportDiscoveryData == null) || (transportDiscoveryData.length == 0)) {
+                throw new IllegalArgumentException("transportDiscoveryData is null");
+            }
+            mTransportDiscoveryData = transportDiscoveryData;
+            return this;
+        }
+
+        /**
          * Build the {@link AdvertiseData}.
          */
         public AdvertiseData build() {
             return new AdvertiseData(mServiceUuids, mManufacturerSpecificData, mServiceData,
-                    mIncludeTxPowerLevel, mIncludeDeviceName);
+                    mIncludeTxPowerLevel, mIncludeDeviceName, mTransportDiscoveryData);
         }
     }
 }
diff --git a/core/java/android/bluetooth/le/AdvertiseData.java.rej b/core/java/android/bluetooth/le/AdvertiseData.java.rej
new file mode 100644
index 0000000..cc7bf34
--- /dev/null
+++ b/core/java/android/bluetooth/le/AdvertiseData.java.rej
@@ -0,0 +1,12 @@
+--- core/java/android/bluetooth/le/AdvertiseData.java
++++ core/java/android/bluetooth/le/AdvertiseData.java
+@@ -134,7 +145,8 @@
+                     other.mManufacturerSpecificData)
+                 && BluetoothLeUtils.equals(mServiceData, other.mServiceData)
+                 && mIncludeDeviceName == other.mIncludeDeviceName
+-                && mIncludeTxPowerLevel == other.mIncludeTxPowerLevel;
++                && mIncludeTxPowerLevel == other.mIncludeTxPowerLevel
++                && BluetoothLeUtils.equals(mTransportDiscoveryData, other.mTransportDiscoveryData);
+     }
+ 
+     @Override
diff --git a/core/java/android/bluetooth/le/BluetoothLeUtils.java b/core/java/android/bluetooth/le/BluetoothLeUtils.java
index c40256b..19e6abe 100644
--- a/core/java/android/bluetooth/le/BluetoothLeUtils.java
+++ b/core/java/android/bluetooth/le/BluetoothLeUtils.java
@@ -77,6 +77,28 @@ public class BluetoothLeUtils {
     }
 
     /**
+     * Returns a string composed from a byte array.
+     */
+    static <T> String toString(byte[] data) {
+        if (data == null) {
+            return "null";
+        }
+        if (data.length == 0) {
+            return "{}";
+        }
+        StringBuilder buffer = new StringBuilder();
+        buffer.append('{');
+        for(int i=0; i < data.length; i++) {
+            buffer.append(data[i]);
+            if ((i+1) < data.length) {
+                buffer.append(", ");
+            }
+        }
+        buffer.append('}');
+        return buffer.toString();
+    }
+
+    /**
      * Check whether two {@link SparseArray} equal.
      */
     static boolean equals(SparseArray<byte[]> array, SparseArray<byte[]> otherArray) {
@@ -126,6 +148,25 @@ public class BluetoothLeUtils {
     }
 
     /**
+     * Check whether two byte arrays are equal.
+     */
+    static <T> boolean equals(byte[] data, byte[] otherData) {
+        if (data == otherData) {
+            return true;
+        }
+        if (data == null || otherData == null) {
+            return false;
+        }
+        if (data.length != otherData.length) {
+            return false;
+        }
+        if (!Objects.deepEquals(data, otherData)) {
+            return false;
+        }
+        return true;
+    }
+
+    /**
      * Ensure Bluetooth is turned on.
      *
      * @throws IllegalStateException If {@code adapter} is null or Bluetooth state is not
-- 
1.9.1

