This folder contains incremental WLAN patches on top OpenQ-835 BSP.

To apply these patches and build from BSP follow this procedure:

1) Download below patches into 'wlan_patches' folder in BSP 'Source_Package' 
   folder:
   * kernel/msm-4.4/msm-4.4.patch 
   * vendor/qcom/opensource/wlan/qcacld-3.0/qcacld-3.0.patch
   * vendor/qcom/opensource/wlan/fw-api/fw-api.patch
   * vendor/qcom/opensource/wlan/qca-wifi-host-cmn/qca-wifi-host-cmn.patch
   * device/qcom/msm8998/msm8998.patch
   * external/wpa_supplicant_8/wpa_supplicant_8.patch

2) Download and build script updates:
   * Copy 'getSource_and_build.sh' to BSP into 'Source_Package' folder.
   * Copy 'manifestUpdate_WFA.sh'  to BSP into 'Source_Package' folder.

3) Run above custom 'getSource_and_build.sh'
   * This calls manifestUpdate_WFA.sh and fetches full OpenQ-835 Android 
     build from codeaurora, applies above patches and starts the build.
