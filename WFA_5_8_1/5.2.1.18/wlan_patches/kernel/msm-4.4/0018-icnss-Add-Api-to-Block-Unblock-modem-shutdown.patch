From 6fa8e40ecd28c940cc12cc0956d78ec97b820b28 Mon Sep 17 00:00:00 2001
From: Sandeep Singh <sandsing@codeaurora.org>
Date: Fri, 8 Feb 2019 15:46:53 +0530
Subject: [PATCH] icnss: Add Api to Block/Unblock modem shutdown

Add API to Block/Unblock modem graceful shutdown.

Change-Id: I69b061fc7d25762b2c36d9590802addfc170f91f
Signed-off-by: Sandeep Singh <sandsing@codeaurora.org>
---
 drivers/soc/qcom/icnss.c | 49 ++++++++++++++++++++++++++++--------------------
 include/soc/qcom/icnss.h |  9 ++-------
 2 files changed, 31 insertions(+), 27 deletions(-)

diff --git a/drivers/soc/qcom/icnss.c b/drivers/soc/qcom/icnss.c
index 3803aaa..ce2409c 100644
--- a/drivers/soc/qcom/icnss.c
+++ b/drivers/soc/qcom/icnss.c
@@ -301,7 +301,7 @@ enum icnss_driver_state {
 	ICNSS_FW_DOWN,
 	ICNSS_DRIVER_UNLOADING,
 	ICNSS_REJUVENATE,
-	ICNSS_DRIVER_LOADING,
+	ICNSS_BLOCK_SHUTDOWN,
 };
 
 struct ce_irq_list {
@@ -494,7 +494,7 @@ static struct icnss_priv {
 	u8 requesting_sub_system;
 	u16 line_number;
 	char function_name[QMI_WLFW_FUNCTION_NAME_LEN_V01 + 1];
-	struct completion driver_probed;
+	struct completion unblock_shutdown;
 } *penv;
 
 #ifdef CONFIG_ICNSS_DEBUG
@@ -1173,6 +1173,21 @@ bool icnss_is_fw_ready(void)
 }
 EXPORT_SYMBOL(icnss_is_fw_ready);
 
+void icnss_block_shutdown(bool status)
+{
+	if (!penv)
+		return;
+
+	if (status) {
+		set_bit(ICNSS_BLOCK_SHUTDOWN, &penv->state);
+		reinit_completion(&penv->unblock_shutdown);
+	} else {
+		clear_bit(ICNSS_BLOCK_SHUTDOWN, &penv->state);
+		complete(&penv->unblock_shutdown);
+	}
+}
+EXPORT_SYMBOL(icnss_block_shutdown);
+
 bool icnss_is_fw_down(void)
 {
 	if (!penv)
@@ -2197,8 +2212,7 @@ static int icnss_call_driver_probe(struct icnss_priv *priv)
 
 	icnss_hw_power_on(priv);
 
-	set_bit(ICNSS_DRIVER_LOADING, &priv->state);
-	reinit_completion(&penv->driver_probed);
+	icnss_block_shutdown(true);
 	while (probe_cnt < ICNSS_MAX_PROBE_CNT) {
 		ret = priv->ops->probe(&priv->pdev->dev);
 		probe_cnt++;
@@ -2208,13 +2222,11 @@ static int icnss_call_driver_probe(struct icnss_priv *priv)
 	if (ret < 0) {
 		icnss_pr_err("Driver probe failed: %d, state: 0x%lx, probe_cnt: %d\n",
 			     ret, priv->state, probe_cnt);
-		complete(&penv->driver_probed);
-		clear_bit(ICNSS_DRIVER_LOADING, &penv->state);
+		icnss_block_shutdown(false);
 		goto out;
 	}
 
-	complete(&penv->driver_probed);
-	clear_bit(ICNSS_DRIVER_LOADING, &penv->state);
+	icnss_block_shutdown(false);
 	set_bit(ICNSS_DRIVER_PROBED, &priv->state);
 
 	return 0;
@@ -2340,8 +2352,7 @@ static int icnss_driver_event_register_driver(void *data)
 	if (ret)
 		goto out;
 
-	set_bit(ICNSS_DRIVER_LOADING, &penv->state);
-	reinit_completion(&penv->driver_probed);
+	icnss_block_shutdown(true);
 	while (probe_cnt < ICNSS_MAX_PROBE_CNT) {
 		ret = penv->ops->probe(&penv->pdev->dev);
 		probe_cnt++;
@@ -2351,13 +2362,11 @@ static int icnss_driver_event_register_driver(void *data)
 	if (ret) {
 		icnss_pr_err("Driver probe failed: %d, state: 0x%lx, probe_cnt: %d\n",
 			     ret, penv->state, probe_cnt);
-		clear_bit(ICNSS_DRIVER_LOADING, &penv->state);
-		complete(&penv->driver_probed);
+		icnss_block_shutdown(false);
 		goto power_off;
 	}
 
-	complete(&penv->driver_probed);
-	clear_bit(ICNSS_DRIVER_LOADING, &penv->state);
+	icnss_block_shutdown(false);
 	set_bit(ICNSS_DRIVER_PROBED, &penv->state);
 
 	return 0;
@@ -2581,8 +2590,8 @@ static int icnss_modem_notifier_nb(struct notifier_block *nb,
 		return NOTIFY_OK;
 
 	if (code == SUBSYS_BEFORE_SHUTDOWN && !notif->crashed &&
-	    test_bit(ICNSS_DRIVER_LOADING, &priv->state)) {
-		if (!wait_for_completion_timeout(&priv->driver_probed,
+	    test_bit(ICNSS_BLOCK_SHUTDOWN, &priv->state)) {
+		if (!wait_for_completion_timeout(&priv->unblock_shutdown,
 						 PROBE_TIMEOUT))
 			icnss_pr_err("wlan driver probe timeout\n");
 	}
@@ -4010,8 +4019,8 @@ static int icnss_stats_show_state(struct seq_file *s, struct icnss_priv *priv)
 		case ICNSS_DRIVER_UNLOADING:
 			seq_puts(s, "DRIVER UNLOADING");
 			continue;
-		case ICNSS_DRIVER_LOADING:
-			seq_puts(s, "WLAN DRIVER LOADING");
+		case ICNSS_BLOCK_SHUTDOWN:
+			seq_puts(s, "BLOCK SHUTDOWN");
 		}
 
 		seq_printf(s, "UNKNOWN-%d", i);
@@ -4685,7 +4694,7 @@ static int icnss_probe(struct platform_device *pdev)
 
 	icnss_wlan_init_mac();
 	device_create_file(dev, &dev_attr_icnss_mac_addr);
-	init_completion(&priv->driver_probed);
+	init_completion(&priv->unblock_shutdown);
 
 	icnss_pr_info("Platform driver probed successfully\n");
 
@@ -4711,7 +4720,7 @@ static int icnss_remove(struct platform_device *pdev)
 
 	icnss_debugfs_destroy(penv);
 
-	complete_all(&penv->driver_probed);
+	complete_all(&penv->unblock_shutdown);
 
 	icnss_modem_ssr_unregister_notifier(penv);
 
diff --git a/include/soc/qcom/icnss.h b/include/soc/qcom/icnss.h
index a28f8a8..366bef7 100644
--- a/include/soc/qcom/icnss.h
+++ b/include/soc/qcom/icnss.h
@@ -1,4 +1,4 @@
-/* Copyright (c) 2015-2017, The Linux Foundation. All rights reserved.
+/* Copyright (c) 2015-2019, The Linux Foundation. All rights reserved.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License version 2 and
@@ -158,10 +158,5 @@ extern bool icnss_is_rejuvenate(void);
 extern int icnss_set_wlan_mac_address(const u8 *in, const uint32_t len);
 extern u8 *icnss_get_wlan_mac_address(struct device *dev, uint32_t *num);
 extern int icnss_trigger_recovery(struct device *dev);
-extern void cnss_set_cc_source(enum cnss_cc_src cc_source);
-extern enum cnss_cc_src cnss_get_cc_source(void);
-extern int icnss_get_driver_load_cnt(void);
-extern void icnss_increment_driver_load_cnt(void);
-extern void icnss_set_cc_source(enum cnss_cc_src cc_source);
-extern enum cnss_cc_src icnss_get_cc_source(void);
+extern void icnss_block_shutdown(bool status);
 #endif /* _ICNSS_WLAN_H_ */
-- 
2.7.4

