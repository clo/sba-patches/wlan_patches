From 0f41274d91dffd0fa5f7f8bcb98467591fca253c Mon Sep 17 00:00:00 2001
From: Baochen Qiang <quic_bqiang@quicinc.com>
Date: Tue, 7 Mar 2023 14:18:23 +0800
Subject: [PATCH 19/22] wifi: ath12k: use aligned DMA address in TX

Currently with IOMMU enabled, DMA address of skb->data in TX
path may be not aligned to 128 bytes, leading to a low PCIe
link efficiency.

Copy skb->data to a newly allocated page so that we can get
a 128-bytes-aligned DMA address.

Test result shows a 11.8% increase in TCP TX throughput with
this change.

Tested-on: WCN7850 hw2.0 PCI WLAN.HMT.1.0-03427-QCAHMTSWPL_V1.0_V2.0_SILICONZ-1.15378.4

Signed-off-by: Baochen Qiang <quic_bqiang@quicinc.com>
---
 drivers/net/wireless/ath/ath12k/core.h  |  1 +
 drivers/net/wireless/ath/ath12k/dp.h    |  2 ++
 drivers/net/wireless/ath/ath12k/dp_tx.c | 26 +++++++++++++++++++++++--
 3 files changed, 27 insertions(+), 2 deletions(-)

diff --git a/drivers/net/wireless/ath/ath12k/core.h b/drivers/net/wireless/ath/ath12k/core.h
index 7eee0ffae646..073300b6d343 100644
--- a/drivers/net/wireless/ath/ath12k/core.h
+++ b/drivers/net/wireless/ath/ath12k/core.h
@@ -83,6 +83,7 @@ struct ath12k_skb_cb {
 	dma_addr_t paddr_ext_desc;
 	u32 cipher;
 	u8 flags;
+	unsigned long tx_page;
 };
 
 struct ath12k_skb_rxcb {
diff --git a/drivers/net/wireless/ath/ath12k/dp.h b/drivers/net/wireless/ath/ath12k/dp.h
index 7c5dafce5a68..2407604568c5 100644
--- a/drivers/net/wireless/ath/ath12k/dp.h
+++ b/drivers/net/wireless/ath/ath12k/dp.h
@@ -254,6 +254,8 @@ struct ath12k_pdev_dp {
 /* Invalid TX Bank ID value */
 #define DP_INVALID_BANK_ID -1
 
+#define DP_TX_BUFFER_ALIGN_SIZE	128
+
 struct ath12k_dp_tx_bank_profile {
 	u8 is_configured;
 	u32 num_users;
diff --git a/drivers/net/wireless/ath/ath12k/dp_tx.c b/drivers/net/wireless/ath/ath12k/dp_tx.c
index 3bdbe7160e6a..4db96a5bc70d 100644
--- a/drivers/net/wireless/ath/ath12k/dp_tx.c
+++ b/drivers/net/wireless/ath/ath12k/dp_tx.c
@@ -146,6 +146,8 @@ int ath12k_dp_tx(struct ath12k *ar, struct ath12k_vif *arvif,
 	u8 ring_selector, ring_map = 0;
 	bool tcl_ring_retry;
 	bool msdu_ext_desc = false;
+	unsigned char *tx_page = NULL;
+	unsigned char *tx_buf;
 
 	if (test_bit(ATH12K_FLAG_CRASH_FLUSH, &ar->ab->dev_flags))
 		return -ESHUTDOWN;
@@ -241,12 +243,23 @@ int ath12k_dp_tx(struct ath12k *ar, struct ath12k_vif *arvif,
 		goto fail_remove_tx_buf;
 	}
 
-	ti.paddr = dma_map_single(ab->dev, skb->data, skb->len, DMA_TO_DEVICE);
+	tx_buf = skb->data;
+	if (!IS_ALIGNED((unsigned long)skb->data, DP_TX_BUFFER_ALIGN_SIZE)) {
+		tx_page = (unsigned char *)__get_free_page(GFP_ATOMIC);
+		if (!tx_page) {
+			ath12k_warn(ab, "failed to alloc page for tx\n");
+			goto map_buffer;
+		}
+		memcpy(tx_page, skb->data, skb->len);
+		tx_buf = tx_page;
+	}
+map_buffer:
+	ti.paddr = dma_map_single(ab->dev, tx_buf, skb->len, DMA_TO_DEVICE);
 	if (dma_mapping_error(ab->dev, ti.paddr)) {
 		atomic_inc(&ab->soc_stats.tx_err.misc_fail);
 		ath12k_warn(ab, "failed to DMA map data Tx buffer\n");
 		ret = -ENOMEM;
-		goto fail_remove_tx_buf;
+		goto fail_free_page;
 	}
 
 	tx_desc->skb = skb;
@@ -255,6 +268,7 @@ int ath12k_dp_tx(struct ath12k *ar, struct ath12k_vif *arvif,
 	ti.data_len = skb->len;
 	skb_cb->paddr = ti.paddr;
 	skb_cb->vif = arvif->vif;
+	skb_cb->tx_page = (unsigned long)tx_page;
 
 	if (msdu_ext_desc) {
 		skb_ext_desc = dev_alloc_skb(sizeof(struct hal_tx_msdu_ext_desc));
@@ -314,6 +328,9 @@ int ath12k_dp_tx(struct ath12k *ar, struct ath12k_vif *arvif,
 		goto fail_unmap_dma;
 	}
 
+	ath12k_dbg(ab, ATH12K_DBG_DP_TX, "tx ring id %u buffer vaddr 0x%lx paddr 0x%llx len 0x%x\n",
+		   ti.ring_id, (unsigned long)tx_buf, ti.paddr, skb->len);
+
 	ath12k_hal_tx_cmd_desc_setup(ab, hal_tcl_desc, &ti);
 
 	ath12k_hal_srng_access_end(ab, tcl_ring);
@@ -331,6 +348,8 @@ int ath12k_dp_tx(struct ath12k *ar, struct ath12k_vif *arvif,
 	dma_unmap_single(ab->dev, ti.paddr, ti.data_len, DMA_TO_DEVICE);
 	dma_unmap_single(ab->dev, skb_cb->paddr_ext_desc,
 			 sizeof(struct hal_tx_msdu_ext_desc), DMA_TO_DEVICE);
+fail_free_page:
+	free_page((unsigned long)tx_page);
 
 fail_remove_tx_buf:
 	ath12k_dp_tx_release_txbuf(dp, tx_desc, pool_id);
@@ -353,6 +372,7 @@ static void ath12k_dp_tx_free_txbuf(struct ath12k_base *ab,
 	if (skb_cb->paddr_ext_desc)
 		dma_unmap_single(ab->dev, skb_cb->paddr_ext_desc,
 				 sizeof(struct hal_tx_msdu_ext_desc), DMA_TO_DEVICE);
+	free_page(skb_cb->tx_page);
 
 	dev_kfree_skb_any(msdu);
 
@@ -385,6 +405,7 @@ ath12k_dp_tx_htt_tx_complete_buf(struct ath12k_base *ab,
 	if (skb_cb->paddr_ext_desc)
 		dma_unmap_single(ab->dev, skb_cb->paddr_ext_desc,
 				 sizeof(struct hal_tx_msdu_ext_desc), DMA_TO_DEVICE);
+	free_page(skb_cb->tx_page);
 
 	memset(&info->status, 0, sizeof(info->status));
 
@@ -460,6 +481,7 @@ static void ath12k_dp_tx_complete_msdu(struct ath12k *ar,
 	if (skb_cb->paddr_ext_desc)
 		dma_unmap_single(ab->dev, skb_cb->paddr_ext_desc,
 				 sizeof(struct hal_tx_msdu_ext_desc), DMA_TO_DEVICE);
+	free_page(skb_cb->tx_page);
 
 	rcu_read_lock();
 
-- 
2.25.1

