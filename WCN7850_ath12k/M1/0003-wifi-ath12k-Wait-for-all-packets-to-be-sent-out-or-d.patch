From 60fc9574a07208ce8dc1154e802f01c97dd6497b Mon Sep 17 00:00:00 2001
From: Baochen Qiang <quic_bqiang@quicinc.com>
Date: Tue, 28 Mar 2023 16:18:11 +0800
Subject: [PATCH 03/22] wifi: ath12k: Wait for all packets to be sent out or
 droped before suspend

In order to send out all packets before going to suspend, current codes
adds a 500ms delay as a workaround. It is a rough estimate and may not
work.

The logic of the fix is to check packet counters, if counters become
zero, then all packets are sent out or droped.

Signed-off-by: Baochen Qiang <quic_bqiang@quicinc.com>
---
 drivers/net/wireless/ath/ath12k/core.c | 20 ++++++++++++++++----
 drivers/net/wireless/ath/ath12k/mac.c  |  6 ++++++
 drivers/net/wireless/ath/ath12k/mac.h  |  1 +
 3 files changed, 23 insertions(+), 4 deletions(-)

diff --git a/drivers/net/wireless/ath/ath12k/core.c b/drivers/net/wireless/ath/ath12k/core.c
index a89e66653f04..fe992a6767ee 100644
--- a/drivers/net/wireless/ath/ath12k/core.c
+++ b/drivers/net/wireless/ath/ath12k/core.c
@@ -19,17 +19,29 @@ unsigned int ath12k_debug_mask;
 module_param_named(debug_mask, ath12k_debug_mask, uint, 0644);
 MODULE_PARM_DESC(debug_mask, "Debugging mask");
 
+static inline struct ath12k_pdev *ath12k_core_get_single_pdev(struct ath12k_base *ab)
+{
+	WARN_ON(!ab->hw_params->single_pdev_only);
+
+	return &ab->pdevs[0];
+}
+
 int ath12k_core_suspend(struct ath12k_base *ab)
 {
+	struct ath12k_pdev *pdev;
+	struct ath12k *ar;
 	int ret;
 
 	if (!ab->hw_params->supports_suspend)
 		return -EOPNOTSUPP;
 
-	/* TODO: there can frames in queues so for now add delay as a hack.
-	 * Need to implement to handle and remove this delay.
-	 */
-	msleep(500);
+	pdev = ath12k_core_get_single_pdev(ab);
+	ar = pdev->ar;
+	ret = ath12k_mac_wait_tx_complete(ar);
+	if (ret) {
+		ath12k_warn(ab, "failed to wait tx complete: %d\n", ret);
+		return ret;
+	}
 
 	ret = ath12k_dp_rx_pktlog_stop(ab, true);
 	if (ret) {
diff --git a/drivers/net/wireless/ath/ath12k/mac.c b/drivers/net/wireless/ath/ath12k/mac.c
index 74dfe352a354..28b5650e45cd 100644
--- a/drivers/net/wireless/ath/ath12k/mac.c
+++ b/drivers/net/wireless/ath/ath12k/mac.c
@@ -6036,6 +6036,12 @@ static int ath12k_mac_flush_tx_complete(struct ath12k *ar)
         return ret;
 }
 
+int ath12k_mac_wait_tx_complete(struct ath12k *ar)
+{
+	ath12k_mac_drain_tx(ar);
+	return ath12k_mac_flush_tx_complete(ar);
+}
+
 static void ath12k_mac_op_flush(struct ieee80211_hw *hw, struct ieee80211_vif *vif,
 				u32 queues, bool drop)
 {
diff --git a/drivers/net/wireless/ath/ath12k/mac.h b/drivers/net/wireless/ath/ath12k/mac.h
index 57f4295420bb..d0d48133e926 100644
--- a/drivers/net/wireless/ath/ath12k/mac.h
+++ b/drivers/net/wireless/ath/ath12k/mac.h
@@ -73,4 +73,5 @@ int ath12k_mac_tx_mgmt_pending_free(int buf_id, void *skb, void *ctx);
 enum rate_info_bw ath12k_mac_bw_to_mac80211_bw(enum ath12k_supported_bw bw);
 enum ath12k_supported_bw ath12k_mac_mac80211_bw_to_ath12k_bw(enum rate_info_bw bw);
 enum hal_encrypt_type ath12k_dp_tx_get_encrypt_type(u32 cipher);
+int ath12k_mac_wait_tx_complete(struct ath12k *ar);
 #endif
-- 
2.25.1

