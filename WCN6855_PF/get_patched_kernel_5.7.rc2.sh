# shell script to download 5.7.rc2 kernel source and patch it
#

#Download kernel
git clone https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git
cd ./linux
git checkout v5.7-rc2 -b myv5.7-rc2

#patch it
git am ../kernel_patches/0001-net-qrtr-Add-MHI-transport-layer.patch
git am ../kernel_patches/0002-net-qrtr-Do-not-depend-on-ARCH_QCOM.patch
git am ../kernel_patches/0003-qualcomm_qmi_helpers.patch
git am ../kernel_patches/0004-cfg80211-Use-new-wiphy-flag-WIPHY_FLAG_DFS_OFFLOAD.patch
git am ../kernel_patches/0005-cfg80211-Miscellenous-changes-for-bringing-up-cld-2..patch
git am ../kernel_patches/0006-fixing-dfs-master-issue.patch
git am ../kernel_patches/0007-cfg80211-SAE-OWE-enable-patch.patch
git am ../kernel_patches/0008-cfg80211-Indicate-support-6GHz-band-in-kernel.patch
git am ../kernel_patches/0009-x86-kernel-reserve-CMA-memory-space-under-4G.patch
git am ../kernel_patches/0010-bus-mhi-core-Handle-firmware-load-using-state-worker.patch
git am ../kernel_patches/0011-HSP-add-RDDM-feature.patch
git am ../kernel_patches/0012-6GHz-Add-support-to-validate-6GHz-channels-for-HE-ca.patch
git am ../kernel_patches/0013-cfg80211-Adjust-6ghz-frequencies-per-channelization.patch
git am ../kernel_patches/0014-cnss2-add-header-file-cnss2.h.patch
git am ../kernel_patches/0015-Allow-mgmt-frames-on-nan-interface.patch

#update the config
sudo cp ../kernel_patches/config-5.7.0-rc2+ .config

#build the krenel image
sudo make-kpkg -j8 --initrd kernel_image kernel_headers


