enable_boot_args(){
	local need_reboot=0
	local grub_file=/etc/default/grub
	sudo grep "crash_kexec_post_notifiers=Y" $grub_file > /dev/null
	if [ $? -ne 0 ];then
		echo "Enabling crash_kexec_post_notifiers..."
		sudo sed 's/crash_kexec_post_notifiers=N/crash_kexec_post_notifiers=Y/g' -i $grub_file
		sudo grep "crash_kexec_post_notifiers=Y" $grub_file > /dev/null
		if [ $? -ne 0 ];then
			sudo sed '/GRUB_CMDLINE_LINUX_DEFAULT=\"/s/\"/&crash_kexec_post_notifiers=Y /' -i $grub_file
		fi
		sudo grep "crash_kexec_post_notifiers=Y" $grub_file > /dev/null
		if [ $? -ne 0 ];then
			echo "Failed to enable crash_kexec_post_notifiers"
		else
			echo "crash_kexec_post_notifiers is enabled"
			need_reboot=1
		fi
	fi

	sudo grep "intel_idle.max_cstate=0 processor.max_cstate=0 idle=poll" $grub_file > /dev/null
	if [ $? -ne 0 ];then
		echo "Setting pm args..."
		sudo sed '/GRUB_CMDLINE_LINUX_DEFAULT=\"/s/\"/&intel_idle.max_cstate=0 processor.max_cstate=0 idle=poll /' -i $grub_file
		sudo grep "intel_idle.max_cstate=0 processor.max_cstate=0 idle=poll" $grub_file > /dev/null
		if [ $? -ne 0 ];then
			echo "Failed to set pm args"
		else
			echo "Pm args are set"
			need_reboot=1
		fi
	fi

	if [ $need_reboot -eq 1 ];then
			sudo update-grub
			echo "Need re-power laptop to complete the installation. Please power-on and then execute insmod_build.sh to start Napier"
			echo -n "Power off?[Y/N]"
			read arg
			if [ "$arg" = "Y" ];then
				sudo poweroff
			fi
	fi
}

echo "Copying Firmware Binaries........."
if [ -d "./wlan_firmware/" ]
then
   echo "copy FW start....."
   sudo cp -f ./wlan_firmware/amss20.bin /lib/firmware/
   sudo cp -f ./wlan_firmware/m3.bin /lib/firmware/
   sudo cp -f ./wlan_firmware/bdwlan.elf /lib/firmware/
   sudo cp -f ./wlan_firmware/bdwlan01.e01 /lib/firmware/
   sudo cp -f ./wlan_firmware/bdwlan01.e02 /lib/firmware/
   sudo cp -f ./wlan_firmware/bdwlan01.e03 /lib/firmware/
   sudo cp -f ./wlan_firmware/bdwlan01.e06 /lib/firmware/
   sudo cp -f ./wlan_firmware/bdwlan01.e0101 /lib/firmware/
   sudo cp -f ./wlan_firmware/bdwlan01.e0102 /lib/firmware/
   sudo cp -f ./wlan_firmware/bdwlan01.e0103 /lib/firmware/
   sudo cp -f ./wlan_firmware/bdwlan_wcn685x_2p0_NFA725.elf /lib/firmware/
   sudo cp -f ./wlan_firmware/bdwlan_wcn685x_2p0_NFA765.elf /lib/firmware/
   sudo cp -f ./wlan_firmware/regdb.bin /lib/firmware/

fi

echo "Copying Host Binaries........."
if [ -d "./cld_ini/" ]
then
   echo "copy Host start....."
   sudo mkdir -p /lib/firmware/wlan
   sudo cp -f ./cld_ini/qcom_cfg.ini /lib/firmware/wlan/qcom_cfg.ini
fi

echo "Setting boot args......"
enable_boot_args

sh insmod_modules.sh
