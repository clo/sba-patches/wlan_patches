From e15465522fc1769e085ff2c4967fe5a18aecb97d Mon Sep 17 00:00:00 2001
From: bings <bings@codeaurora.org>
Date: Sun, 12 Apr 2020 15:58:31 +0800
Subject: [PATCH 54/63] cfg80211: Miscellenous changes for bringing up cld-2.0
 on v5.7

mac80211 implement HS2.0 gratuitous ARP/unsolicited NA dropping
Changes include adding support for the following

Signed-off-by: Kang Xu <kangxu@codeaurora.org>
---
 include/net/cfg80211.h | 35 +++++++++++++++++++++++++++++
 net/wireless/reg.c     |  1 +
 net/wireless/util.c    | 51 ++++++++++++++++++++++++++++++++++++++++++
 3 files changed, 87 insertions(+)

diff --git a/include/net/cfg80211.h b/include/net/cfg80211.h
index 3af2e8bb27e7..23f948e4475a 100644
--- a/include/net/cfg80211.h
+++ b/include/net/cfg80211.h
@@ -7600,6 +7600,41 @@ void cfg80211_pmsr_complete(struct wireless_dev *wdev,
 bool cfg80211_iftype_allowed(struct wiphy *wiphy, enum nl80211_iftype iftype,
 			     bool is_4addr, u8 check_swif);
 
+ /**
+ * regulatory_hint_user - hint to the wireless core a regulatory domain
+ * which the driver has received from an application
+ * @alpha2: the ISO/IEC 3166 alpha2 the driver claims its regulatory domain
+ *  should be in. If @rd is set this should be NULL. Note that if you
+ *  set this to NULL you should still set rd->alpha2 to some accepted
+ *  alpha2.
+ * @user_reg_hint_type: the type of user regulatory hint.
+ *
+ * Wireless drivers can use this function to hint to the wireless core
+ * the current regulatory domain as specified by trusted applications,
+ * it is the driver's responsibilty to estbalish which applications it
+ * trusts.
+ *
+ * The wiphy should be registered to cfg80211 prior to this call.
+ * For cfg80211 drivers this means you must first use wiphy_register(),
+ * for mac80211 drivers you must first use ieee80211_register_hw().
+ *
+ * Drivers should check the return value, its possible you can get
+ * an -ENOMEM or an -EINVAL.
+ *
+ * Return: 0 on success. -ENOMEM, -EINVAL.
+ */
+int regulatory_hint_user(const char *alpha2,
+             enum nl80211_user_reg_hint_type user_reg_hint_type);
+
+/**
+ * cfg80211_is_gratuitous_arp_unsolicited_na - packet is grat. ARP/unsol. NA
+ * @skb: the input packet, must be an ethernet frame already
+ *
+ * Return: %true if the packet is a gratuitous ARP or unsolicited NA packet.
+ * This is used to drop packets that shouldn't occur because the AP implements
+ * a proxy service.
+ */
+bool cfg80211_is_gratuitous_arp_unsolicited_na(struct sk_buff *skb);
 
 /* Logging, debugging and troubleshooting/diagnostic helpers. */
 
diff --git a/net/wireless/reg.c b/net/wireless/reg.c
index d476d4da0d09..5ff620560d37 100644
--- a/net/wireless/reg.c
+++ b/net/wireless/reg.c
@@ -2960,6 +2960,7 @@ int regulatory_hint_user(const char *alpha2,
 
 	return 0;
 }
+EXPORT_SYMBOL(regulatory_hint_user);
 
 int regulatory_hint_indoor(bool is_indoor, u32 portid)
 {
diff --git a/net/wireless/util.c b/net/wireless/util.c
index 6590efbbcbb9..18e1cc234ae0 100644
--- a/net/wireless/util.c
+++ b/net/wireless/util.c
@@ -1983,6 +1983,57 @@ const unsigned char bridge_tunnel_header[] __aligned(2) =
 	{ 0xaa, 0xaa, 0x03, 0x00, 0x00, 0xf8 };
 EXPORT_SYMBOL(bridge_tunnel_header);
 
+bool cfg80211_is_gratuitous_arp_unsolicited_na(struct sk_buff *skb)
+{
+    const struct ethhdr *eth = (void *)skb->data;
+    const struct {
+        struct arphdr hdr;
+        u8 ar_sha[ETH_ALEN];
+        u8 ar_sip[4];
+        u8 ar_tha[ETH_ALEN];
+        u8 ar_tip[4];
+    } __packed *arp;
+    const struct ipv6hdr *ipv6;
+    const struct icmp6hdr *icmpv6;
+
+    switch (eth->h_proto) {
+    case cpu_to_be16(ETH_P_ARP):
+        /* can't say - but will probably be dropped later anyway */
+        if (!pskb_may_pull(skb, sizeof(*eth) + sizeof(*arp)))
+            return false;
+
+        arp = (void *)(eth + 1);
+
+        if ((arp->hdr.ar_op == cpu_to_be16(ARPOP_REPLY) ||
+             arp->hdr.ar_op == cpu_to_be16(ARPOP_REQUEST)) &&
+            !memcmp(arp->ar_sip, arp->ar_tip, sizeof(arp->ar_sip)))
+            return true;
+        break;
+    case cpu_to_be16(ETH_P_IPV6):
+        /* can't say - but will probably be dropped later anyway */
+        if (!pskb_may_pull(skb, sizeof(*eth) + sizeof(*ipv6) +
+                    sizeof(*icmpv6)))
+            return false;
+
+        ipv6 = (void *)(eth + 1);
+        icmpv6 = (void *)(ipv6 + 1);
+
+        if (icmpv6->icmp6_type == NDISC_NEIGHBOUR_ADVERTISEMENT &&
+            !memcmp(&ipv6->saddr, &ipv6->daddr, sizeof(ipv6->saddr)))
+            return true;
+        break;
+    default:
+        /*
+         * no need to support other protocols, proxy service isn't
+         * specified for any others
+         */
+        break;
+    }
+
+    return false;
+}
+EXPORT_SYMBOL(cfg80211_is_gratuitous_arp_unsolicited_na);
+
 /* Layer 2 Update frame (802.2 Type 1 LLC XID Update response) */
 struct iapp_layer2_update {
 	u8 da[ETH_ALEN];	/* broadcast */
-- 
2.17.1

