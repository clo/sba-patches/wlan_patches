#
# To generate platform/cld driver and get ko
#

cd ./platform_src
#compile platform driver
make -j8

cd ../cld/qcacld-3.0
patch -p1 < ../../Patch-Makefile.patch
make CONFIG_CNSS2=y CONFIG_CNSS_KIWI=y CONFIG_SLUB_DEBUG_ON=n CONFIG_SLUB_DEBUG=n CONFIG_LINUX_QCMBR=y DEVELOPER_DISABLE_BUILD_TIMESTAMP=y CONFIG_RX_FISA=n WLAN_PROFILE=kiwi_v2 -j8

