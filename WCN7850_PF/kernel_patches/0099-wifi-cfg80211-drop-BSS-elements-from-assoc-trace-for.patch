From f9c810c6ce6f46661d19d7430a4f5b29e4901eef Mon Sep 17 00:00:00 2001
From: Johannes Berg <johannes.berg@intel.com>
Date: Wed, 22 Jun 2022 11:02:00 +0200
Subject: [PATCH 099/165] wifi: cfg80211: drop BSS elements from assoc trace
 for now

For multi-link operation, this cannot work as the req->bss pointer
will be NULL, and we'll need to do more work on this to really add
tracing for the MLO case here. Drop the BSS elements for now as
they're not the most useful thing, and it's hard to size things
correctly for the MLO case (without adding a lot of code that's
also executed when tracing isn't enabled.)

Reported-by: Dan Carpenter <dan.carpenter@oracle.com>
Signed-off-by: Johannes Berg <johannes.berg@intel.com>
---
 net/wireless/rdev-ops.h | 11 +----------
 net/wireless/trace.h    | 13 ++-----------
 2 files changed, 3 insertions(+), 21 deletions(-)

diff --git a/net/wireless/rdev-ops.h b/net/wireless/rdev-ops.h
index 6221a996c19f..53f5a0126dfd 100644
--- a/net/wireless/rdev-ops.h
+++ b/net/wireless/rdev-ops.h
@@ -469,18 +469,9 @@ static inline int rdev_assoc(struct cfg80211_registered_device *rdev,
 			     struct net_device *dev,
 			     struct cfg80211_assoc_request *req)
 {
-	const struct cfg80211_bss_ies *bss_ies;
 	int ret;
 
-	/*
-	 * Note: we might trace not exactly the data that's processed,
-	 * due to races and the driver/mac80211 getting a newer copy.
-	 */
-	rcu_read_lock();
-	bss_ies = rcu_dereference(req->bss->ies);
-	trace_rdev_assoc(&rdev->wiphy, dev, req, bss_ies);
-	rcu_read_unlock();
-
+	trace_rdev_assoc(&rdev->wiphy, dev, req);
 	ret = rdev->ops->assoc(&rdev->wiphy, dev, req);
 	trace_rdev_return_int(&rdev->wiphy, ret);
 	return ret;
diff --git a/net/wireless/trace.h b/net/wireless/trace.h
index e78bffbc6f95..c50e8a04199e 100644
--- a/net/wireless/trace.h
+++ b/net/wireless/trace.h
@@ -1232,9 +1232,8 @@ TRACE_EVENT(rdev_auth,
 
 TRACE_EVENT(rdev_assoc,
 	TP_PROTO(struct wiphy *wiphy, struct net_device *netdev,
-		 struct cfg80211_assoc_request *req,
-		 const struct cfg80211_bss_ies *bss_ies),
-	TP_ARGS(wiphy, netdev, req, bss_ies),
+		 struct cfg80211_assoc_request *req),
+	TP_ARGS(wiphy, netdev, req),
 	TP_STRUCT__entry(
 		WIPHY_ENTRY
 		NETDEV_ENTRY
@@ -1242,9 +1241,6 @@ TRACE_EVENT(rdev_assoc,
 		MAC_ENTRY(prev_bssid)
 		__field(bool, use_mfp)
 		__field(u32, flags)
-		__dynamic_array(u8, bss_elements, bss_ies->len)
-		__field(bool, bss_elements_bcon)
-		__field(u64, bss_elements_tsf)
 		__dynamic_array(u8, elements, req->ie_len)
 		__array(u8, ht_capa, sizeof(struct ieee80211_ht_cap))
 		__array(u8, ht_capa_mask, sizeof(struct ieee80211_ht_cap))
@@ -1264,11 +1260,6 @@ TRACE_EVENT(rdev_assoc,
 		MAC_ASSIGN(prev_bssid, req->prev_bssid);
 		__entry->use_mfp = req->use_mfp;
 		__entry->flags = req->flags;
-		if (bss_ies->len)
-			memcpy(__get_dynamic_array(bss_elements),
-			       bss_ies->data, bss_ies->len);
-		__entry->bss_elements_bcon = bss_ies->from_beacon;
-		__entry->bss_elements_tsf = bss_ies->tsf;
 		if (req->ie)
 			memcpy(__get_dynamic_array(elements),
 			       req->ie, req->ie_len);
-- 
2.25.1

