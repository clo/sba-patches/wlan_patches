From 012e0adfeacaf3362b151d8e908a9a4142855f40 Mon Sep 17 00:00:00 2001
From: Shivani Baranwal <quic_shivbara@quicinc.com>
Date: Tue, 9 Jul 2024 11:45:34 +0530
Subject: [PATCH 1/7] TODO: P2P2: Assisted DFS for P2P2 GO in 5 GHz

TODO: Number of comments over email

Add support to allow P2P GO operate on a 5GHz DFS channel. The
implementation here allows P2P Group comprised of DFS Client devices
(i.e., which might not have radar detection capability) to operate in
the vicinity an AP that is a DFS Owner. This is referred to as AP
assisted DFS channel operation. Here, the devices in the P2P Group
shall operate on the same channel as the DFS Owner AP.

The P2P GO and P2P Client shall meet the following requirements:

- The P2P GO's WLAN STA interface is concurrently associated with an AP
   that is a DFS Owner.
- The P2P client's WLAN STA interface is concurrently associated with an
  AP that is a DFS Owner operating in the same band and channel or the
  P2P client can hear (e.g., receive beacons) the same AP that the P2P
  GO's WLAN STA interface is associated with.

Signed-off-by: Shivani Baranwal <quic_shivbara@quicinc.com>
---
 src/common/ieee802_11_defs.h      |   8 +++
 src/p2p/p2p.c                     | 115 +++++++++++++++++++++++++++++-
 src/p2p/p2p.h                     |  20 +++++-
 src/p2p/p2p_build.c               |  44 ++++++++++++
 src/p2p/p2p_go_neg.c              |  68 ++++++++++++++++++
 src/p2p/p2p_group.c               |   8 +++
 src/p2p/p2p_i.h                   |  17 +++++
 src/p2p/p2p_utils.c               |  50 +++++++++++++
 wpa_supplicant/config.c           |   2 +
 wpa_supplicant/config.h           |   2 +
 wpa_supplicant/config_file.c      |   4 +-
 wpa_supplicant/events.c           |  50 +++++++++++++
 wpa_supplicant/p2p_supplicant.c   |  65 ++++++++++++++++-
 wpa_supplicant/p2p_supplicant.h   |   8 +++
 wpa_supplicant/wpa_supplicant_i.h |   1 +
 15 files changed, 455 insertions(+), 7 deletions(-)

diff --git a/src/common/ieee802_11_defs.h b/src/common/ieee802_11_defs.h
index 4755facab..8af6bbaad 100644
--- a/src/common/ieee802_11_defs.h
+++ b/src/common/ieee802_11_defs.h
@@ -3143,6 +3143,14 @@ struct ieee80211_s1g_beacon_compat {
 	le32 tsf_completion;
 } STRUCT_PACKED;
 
+struct ieee80211_dfs_ap_info_list {
+	u8 flag;
+	u8 bssid[ETH_ALEN];
+	u8 country[3];
+	u8 op_class;
+	u8 op_chan;
+};
+
 #ifdef _MSC_VER
 #pragma pack(pop)
 #endif /* _MSC_VER */
diff --git a/src/p2p/p2p.c b/src/p2p/p2p.c
index 425975e34..850beef28 100644
--- a/src/p2p/p2p.c
+++ b/src/p2p/p2p.c
@@ -1428,7 +1428,15 @@ static int p2p_prepare_channel_pref(struct p2p_data *p2p,
 
 	p2p_dbg(p2p, "Prepare channel pref - force_freq=%u pref_freq=%u go=%d",
 		force_freq, pref_freq, go);
-	if (p2p_freq_to_channel(freq, &op_class, &op_channel) < 0) {
+
+	if (p2p->cfg->is_p2p_dfs_chan &&
+	    p2p->cfg->is_p2p_dfs_chan(p2p->cfg->cb_ctx, freq, 0, 0)) {
+		if (ieee80211_freq_to_channel_ext(freq, 0, CONF_OPER_CHWIDTH_80MHZ,
+				&op_class, &op_channel) == NUM_HOSTAPD_MODES) {
+			p2p_dbg(p2p, "Unsupported frequency %u MHz", freq);
+			return -1;
+		}
+	} else if (p2p_freq_to_channel(freq, &op_class, &op_channel) < 0) {
 		p2p_dbg(p2p, "Unsupported frequency %u MHz", freq);
 		return -1;
 	}
@@ -1567,6 +1575,8 @@ static void p2p_prepare_channel_best(struct p2p_data *p2p)
 int p2p_prepare_channel(struct p2p_data *p2p, struct p2p_device *dev,
 			unsigned int force_freq, unsigned int pref_freq, int go)
 {
+	struct p2p_channels p2p_chanlist;
+
 	p2p_dbg(p2p, "Prepare channel - force_freq=%u pref_freq=%u go=%d",
 		force_freq, pref_freq, go);
 	if (force_freq || pref_freq) {
@@ -1576,6 +1586,18 @@ int p2p_prepare_channel(struct p2p_data *p2p, struct p2p_device *dev,
 	} else {
 		p2p_prepare_channel_best(p2p);
 	}
+
+	if (p2p->cfg->is_p2p_dfs_chan &&
+	    p2p->cfg->is_p2p_dfs_chan(p2p->cfg->cb_ctx, 0,
+	    p2p->op_reg_class, p2p->op_channel)) {
+		p2p_dfs_channel_filter(p2p, &p2p->channels, p2p->dfs_ap_list,
+				       p2p->num_dfs_ap, &p2p_chanlist);
+		p2p_channels_dump(p2p, "channel list after filtering DFS "
+				  " channels with WLAN AP info attr channles",
+				  &p2p_chanlist);
+		p2p_copy_channels(&p2p->channels, &p2p_chanlist, p2p->allow_6ghz);
+	}
+
 	p2p_channels_dump(p2p, "prepared channels", &p2p->channels);
 	if (go)
 		p2p_channels_remove_freqs(&p2p->channels, &p2p->no_go_freq);
@@ -2800,6 +2822,20 @@ int p2p_assoc_req_ie(struct p2p_data *p2p, const u8 *bssid, u8 *buf,
 	p2p_buf_add_device_info(tmp, p2p, peer);
 	p2p_buf_update_ie_hdr(tmp, lpos);
 
+	if (p2p->cfg->is_p2p_dfs_chan &&
+	    p2p->cfg->is_p2p_dfs_chan(p2p->cfg->cb_ctx, 0, p2p->op_reg_class,
+				      p2p->op_channel) &&
+	    !is_p2p_dfs_owner(p2p)) {
+		struct wpabuf *p2p2_ie;
+
+		p2p2_ie = wpabuf_alloc(255);
+		if (!p2p2_ie)
+			return -1;
+
+		p2p_group_build_p2p2_ie(p2p, p2p2_ie, 0);
+		tmp = wpabuf_concat(p2p2_ie, tmp);
+	}
+
 	tmplen = wpabuf_len(tmp);
 	if (tmplen > len)
 		res = -1;
@@ -3074,6 +3110,82 @@ void p2p_set_dev_addr(struct p2p_data *p2p, const u8 *addr)
 }
 
 
+bool is_p2p_dfs_chan_enabled(struct p2p_data *p2p)
+{
+	if (p2p)
+		return p2p->cfg->dfs_chan_enable;
+	return false;
+}
+
+
+bool is_p2p_dfs_owner(struct p2p_data *p2p)
+{
+	if (p2p)
+		return p2p->cfg->dfs_owner;
+	return false;
+}
+
+
+struct ieee80211_dfs_ap_info_list * p2p_dfs_get_ap_info(struct p2p_data *p2p,
+							const u8 *bssid)
+{
+	size_t i;
+
+	if (!p2p->dfs_ap_list)
+		return NULL;
+
+	for (i = 0; i < p2p->num_dfs_ap; i++) {
+		struct ieee80211_dfs_ap_info_list *dfs_ap =
+						 &p2p->dfs_ap_list[i];
+		if (ether_addr_equal(dfs_ap->bssid, bssid))
+			return dfs_ap;
+	}
+	return NULL;
+}
+
+
+void p2p_update_dfs_ap_info(struct p2p_data *p2p, const u8 *bssid, int freq,
+			    int flag, bool disconnect_evt, u8 *country)
+{
+	struct ieee80211_dfs_ap_info_list *dfs_ap;
+
+	dfs_ap = p2p_dfs_get_ap_info(p2p, bssid);
+
+	if (dfs_ap) {
+		wpa_printf(MSG_DEBUG, "Update the existing DFS AP info");
+	} else {
+		dfs_ap = os_realloc_array(p2p->dfs_ap_list, p2p->num_dfs_ap + 1,
+				sizeof(struct ieee80211_dfs_ap_info_list));
+		if (!dfs_ap) {
+			wpa_printf(MSG_DEBUG, "Unable to allocate dfs_ap memory");
+			return;
+		}
+
+		p2p->dfs_ap_list = dfs_ap;
+		dfs_ap = &p2p->dfs_ap_list[p2p->num_dfs_ap];
+		p2p->num_dfs_ap++;
+		os_memset(dfs_ap, 0, sizeof(*dfs_ap));
+	}
+
+	if (disconnect_evt)
+		dfs_ap->flag = 0;
+
+	/* skip if flag is already set by assoc event */
+	if (!dfs_ap->flag)
+		dfs_ap->flag = flag;
+
+	os_memcpy(dfs_ap->bssid, bssid, ETH_ALEN);
+
+	if (country)
+		os_memcpy(dfs_ap->country, country, 3);
+
+	ieee80211_freq_to_channel_ext(freq, 0,
+				      CONF_OPER_CHWIDTH_80MHZ,
+				      &dfs_ap->op_class,
+				      &dfs_ap->op_chan);
+}
+
+
 static void p2p_pairing_info_deinit(struct p2p_data *p2p)
 {
 #ifdef CONFIG_PASN
@@ -3225,6 +3337,7 @@ void p2p_deinit(struct p2p_data *p2p)
 	os_free(p2p->no_go_freq.range);
 	p2p_service_flush_asp(p2p);
 	p2p_pairing_info_deinit(p2p);
+	os_free(p2p->dfs_ap_list);
 
 	os_free(p2p);
 }
diff --git a/src/p2p/p2p.h b/src/p2p/p2p.h
index dda088ba5..e4cc8a43d 100644
--- a/src/p2p/p2p.h
+++ b/src/p2p/p2p.h
@@ -44,7 +44,7 @@ struct weighted_pcl;
 /**
  * P2P_MAX_REG_CLASSES - Maximum number of regulatory classes
  */
-#define P2P_MAX_REG_CLASSES 15
+#define P2P_MAX_REG_CLASSES 20
 
 /**
  * P2P_MAX_REG_CLASS_CHANNELS - Maximum number of channels per regulatory class
@@ -709,6 +709,11 @@ struct p2p_config {
 	 */
 	bool dfs_owner;
 
+	/**
+	 * dfs_chan_enable - Enable p2p Go to operate on dfs channel
+	 */
+	bool dfs_chan_enable;
+
 	/**
 	 * twt_power_mgmt - Enable TWT based power management for P2P
 	 */
@@ -751,7 +756,6 @@ struct p2p_config {
 	 */
 	void (*debug_print)(void *ctx, int level, const char *msg);
 
-
 	/* Callbacks to request lower layer driver operations */
 
 	/**
@@ -1390,6 +1394,14 @@ struct p2p_config {
 	 * Returns: 0 on success, -1 on failure
 	 */
 	int (*parse_data_element)(void *ctx, const u8 *data, size_t len);
+
+	/**
+	 *
+	 * is_p2p_dfs_chan - DFS channel check
+	 *
+	 * To check if a channel is DFS channel or not.
+	 */
+	int (*is_p2p_dfs_chan)(void *ctx, int freq, int op_class, int op_chan);
 };
 
 
@@ -2749,5 +2761,9 @@ void p2p_pasn_pmksa_set_pmk(struct p2p_data *p2p, const u8 *src, const u8 *dst,
 void p2p_set_store_pasn_ptk(struct p2p_data *p2p, u8 val);
 void p2p_pasn_store_ptk(struct p2p_data *p2p, struct wpa_ptk *ptk);
 int p2p_pasn_get_ptk(struct p2p_data *p2p, const u8 **buf, size_t *buf_len);
+bool is_p2p_dfs_chan_enabled(struct p2p_data *p2p);
+bool is_p2p_dfs_owner(struct p2p_data *p2p);
+void p2p_update_dfs_ap_info(struct p2p_data *p2p, const u8 *bssid, int freq,
+			    int flag, bool disconnect_evt, u8 *country);
 
 #endif /* P2P_H */
diff --git a/src/p2p/p2p_build.c b/src/p2p/p2p_build.c
index 015eed22c..2a61f5cf8 100644
--- a/src/p2p/p2p_build.c
+++ b/src/p2p/p2p_build.c
@@ -994,3 +994,47 @@ struct wpabuf * p2p_encaps_ie(const struct wpabuf *subelems, u32 ie_type)
 
 	return ie;
 }
+
+void p2p_buf_add_wlan_ap_info(struct wpabuf *buf,
+			      struct ieee80211_dfs_ap_info_list *dfs_ap_list,
+			      size_t list_size)
+{
+	u8 *len;
+	size_t i, size;
+
+	if (!list_size)
+		return;
+
+	wpabuf_put_u8(buf, P2P_ATTR_WLAN_AP_INFORMATION);
+	/* IE length to be filled */
+	len = wpabuf_put(buf, 2);
+
+	for (i = 0; i < list_size; i++) {
+		if (dfs_ap_list[i].flag != 1)
+			continue;
+
+		wpabuf_put_u8(buf, dfs_ap_list[i].flag);
+		wpabuf_put_data(buf, dfs_ap_list[i].bssid, ETH_ALEN);
+		wpabuf_put_data(buf, dfs_ap_list[i].country, 3);
+		wpabuf_put_u8(buf, dfs_ap_list[i].op_class);
+		wpabuf_put_u8(buf, dfs_ap_list[i].op_chan);
+	}
+
+	if (list_size > 4)
+		size = 4;
+	else
+		size = list_size;
+
+	for (i = 0; i < size; i++) {
+		if (dfs_ap_list[i].flag == 1)
+			continue;
+		wpabuf_put_u8(buf, dfs_ap_list[i].flag);
+		wpabuf_put_data(buf, dfs_ap_list[i].bssid, ETH_ALEN);
+		wpabuf_put_data(buf, dfs_ap_list[i].country, 3);
+		wpabuf_put_u8(buf, dfs_ap_list[i].op_class);
+		wpabuf_put_u8(buf, dfs_ap_list[i].op_chan);
+	}
+
+	/* Update attribute length */
+	WPA_PUT_LE16(len, (u8 *)wpabuf_put(buf, 0) - len - 2);
+}
diff --git a/src/p2p/p2p_go_neg.c b/src/p2p/p2p_go_neg.c
index ac6bbf75f..62fc396c8 100644
--- a/src/p2p/p2p_go_neg.c
+++ b/src/p2p/p2p_go_neg.c
@@ -140,6 +140,7 @@ struct wpabuf * p2p_build_go_neg_req(struct p2p_data *p2p,
 {
 	struct wpabuf *buf;
 	struct wpabuf *subelems;
+	u8 *len;
 	u8 group_capab;
 	size_t extra = 0;
 	u16 pw_id;
@@ -231,6 +232,15 @@ struct wpabuf * p2p_build_go_neg_req(struct p2p_data *p2p,
 	if (p2p->vendor_elem && p2p->vendor_elem[VENDOR_ELEM_P2P_GO_NEG_REQ])
 		wpabuf_put_buf(buf, p2p->vendor_elem[VENDOR_ELEM_P2P_GO_NEG_REQ]);
 
+	if (p2p->cfg->is_p2p_dfs_chan &&
+	    p2p->cfg->is_p2p_dfs_chan(p2p->cfg->cb_ctx, 0,
+	    p2p->op_reg_class, p2p->op_channel)) {
+		len = p2p_buf_add_p2p2_ie_hdr(buf);
+		p2p_buf_add_wlan_ap_info(buf, p2p->dfs_ap_list,
+					 p2p->num_dfs_ap);
+		p2p_buf_update_ie_hdr(buf, len);
+	}
+
 	buf = wpabuf_concat(buf, p2p_encaps_ie(subelems, P2P_IE_VENDOR_TYPE));
 	wpabuf_free(subelems);
 	return buf;
@@ -304,6 +314,7 @@ static struct wpabuf * p2p_build_go_neg_resp(struct p2p_data *p2p,
 {
 	struct wpabuf *buf;
 	struct wpabuf *subelems;
+	u8 *len;
 	u8 group_capab;
 	size_t extra = 0;
 	u16 pw_id;
@@ -421,6 +432,15 @@ static struct wpabuf * p2p_build_go_neg_resp(struct p2p_data *p2p,
 	if (p2p->vendor_elem && p2p->vendor_elem[VENDOR_ELEM_P2P_GO_NEG_RESP])
 		wpabuf_put_buf(buf, p2p->vendor_elem[VENDOR_ELEM_P2P_GO_NEG_RESP]);
 
+	if (p2p->cfg->is_p2p_dfs_chan &&
+	    p2p->cfg->is_p2p_dfs_chan(p2p->cfg->cb_ctx, 0,
+	    p2p->op_reg_class, p2p->op_channel)) {
+		len = p2p_buf_add_p2p2_ie_hdr(buf);
+		p2p_buf_add_wlan_ap_info(buf, p2p->dfs_ap_list,
+					 p2p->num_dfs_ap);
+		p2p_buf_update_ie_hdr(buf, len);
+	}
+
 	buf = wpabuf_concat(buf, p2p_encaps_ie(subelems, P2P_IE_VENDOR_TYPE));
 	wpabuf_free(subelems);
 	return buf;
@@ -1098,6 +1118,24 @@ skip:
 		 */
 		p2p_check_pref_chan(p2p, go, dev, &msg);
 
+		if (msg.wlan_ap_info) {
+			u8 *pos = (u8 *)msg.wlan_ap_info, match = 0, i;
+			if (p2p->cfg->is_p2p_dfs_chan &&
+			    p2p->cfg->is_p2p_dfs_chan(p2p->cfg->cb_ctx, 0,
+						      p2p->op_reg_class,
+						      p2p->op_channel)) {
+				for (i = 0; i < msg.wlan_ap_info_len; i += 12) {
+					if (*(pos + 10) == p2p->op_reg_class &&
+					    *(pos + 11) == p2p->op_channel) {
+						match = 1;
+						break;
+					}
+				}
+				if (match == 0)
+					goto fail;
+			}
+		}
+
 		if (msg.config_timeout) {
 			dev->go_timeout = msg.config_timeout[0];
 			dev->client_timeout = msg.config_timeout[1];
@@ -1185,6 +1223,7 @@ static struct wpabuf * p2p_build_go_neg_conf(struct p2p_data *p2p,
 {
 	struct wpabuf *buf;
 	struct wpabuf *subelems;
+	u8 *len;
 	struct p2p_channels res;
 	u8 group_capab;
 	size_t extra = 0;
@@ -1255,6 +1294,15 @@ static struct wpabuf * p2p_build_go_neg_conf(struct p2p_data *p2p,
 	if (p2p->vendor_elem && p2p->vendor_elem[VENDOR_ELEM_P2P_GO_NEG_CONF])
 		wpabuf_put_buf(buf, p2p->vendor_elem[VENDOR_ELEM_P2P_GO_NEG_CONF]);
 
+	if (p2p->cfg->is_p2p_dfs_chan &&
+	    p2p->cfg->is_p2p_dfs_chan(p2p->cfg->cb_ctx, 0,
+	    p2p->op_reg_class, p2p->op_channel)) {
+		len = p2p_buf_add_p2p2_ie_hdr(buf);
+		p2p_buf_add_wlan_ap_info(buf, p2p->dfs_ap_list,
+					 p2p->num_dfs_ap);
+		p2p_buf_update_ie_hdr(buf, len);
+	}
+
 	buf = wpabuf_concat(buf, p2p_encaps_ie(subelems, P2P_IE_VENDOR_TYPE));
 	wpabuf_free(subelems);
 	return buf;
@@ -1496,6 +1544,26 @@ skip:
 	if (go)
 		p2p_check_pref_chan(p2p, go, dev, &msg);
 
+	if (msg.wlan_ap_info) {
+		u16 match = 0, i;
+		const u8 *pos = msg.wlan_ap_info;
+
+		if (p2p->cfg->is_p2p_dfs_chan &&
+		    p2p->cfg->is_p2p_dfs_chan(p2p->cfg->cb_ctx, 0,
+					      p2p->op_reg_class,
+					      p2p->op_channel)) {
+			for (i = 0; i < msg.wlan_ap_info_len; i += 12) {
+				if (*(pos + 10) == p2p->op_reg_class &&
+				    *(pos + 11) == p2p->op_channel) {
+					match = 1;
+					break;
+				}
+			}
+			if (match == 0)
+				goto fail;
+		}
+	}
+
 	p2p_set_state(p2p, P2P_GO_NEG);
 	p2p_clear_timeout(p2p);
 
diff --git a/src/p2p/p2p_group.c b/src/p2p/p2p_group.c
index 2659787f3..cdcfccff2 100644
--- a/src/p2p/p2p_group.c
+++ b/src/p2p/p2p_group.c
@@ -215,6 +215,14 @@ struct wpabuf * p2p_group_build_p2p2_ie(struct p2p_data *p2p,
 	wpabuf_put_be32(p2p2_ie, P2P2_IE_VENDOR_TYPE);
 	wpa_printf(MSG_DEBUG, "P2P: * P2P2 IE header");
 	p2p_buf_add_pcea(p2p2_ie, p2p);
+
+	if (p2p->cfg->is_p2p_dfs_chan &&
+	    p2p->cfg->is_p2p_dfs_chan(p2p->cfg->cb_ctx, freq, p2p->op_reg_class,
+				      p2p->op_channel) &&
+	    !is_p2p_dfs_owner(p2p)) {
+		p2p_buf_add_wlan_ap_info(p2p2_ie, p2p->dfs_ap_list,
+					 p2p->num_dfs_ap);
+	}
 	*len = (u8 *) wpabuf_put(p2p2_ie, 0) - len - 1;
 
 	return p2p2_ie;
diff --git a/src/p2p/p2p_i.h b/src/p2p/p2p_i.h
index 6fde4f5b7..fd4e4bbe8 100644
--- a/src/p2p/p2p_i.h
+++ b/src/p2p/p2p_i.h
@@ -679,6 +679,16 @@ struct p2p_data {
 	 */
 	bool go_role;
 
+	/**
+	 * list of DFS APs
+	 */
+	struct ieee80211_dfs_ap_info_list *dfs_ap_list;
+
+	/**
+	 * num of DFS APs
+	 */
+	size_t num_dfs_ap;
+
 #ifdef CONFIG_TESTING_OPTIONS
 	/**
 	 * PASN PTK of recent auth
@@ -1083,6 +1093,13 @@ void p2p_sd_query_cb(struct p2p_data *p2p, int success);
 void p2p_pasn_initialize(struct p2p_data *p2p, struct p2p_device *dev,
 			 const u8 *addr, int freq, bool verify,
 			 bool derive_kek);
+void p2p_dfs_channel_filter(struct p2p_data *p2p,
+			    const struct p2p_channels *p2p_chan,
+			    const struct ieee80211_dfs_ap_info_list *ap_list,
+			    size_t num_dfs_ap, struct p2p_channels *res);
+void p2p_buf_add_wlan_ap_info(struct wpabuf *buf,
+			      struct ieee80211_dfs_ap_info_list *dfs_ap_list,
+			      size_t list_size);
 
 void p2p_dbg(struct p2p_data *p2p, const char *fmt, ...)
 PRINTF_FORMAT(2, 3);
diff --git a/src/p2p/p2p_utils.c b/src/p2p/p2p_utils.c
index c1f0084b8..35cbb16a4 100644
--- a/src/p2p/p2p_utils.c
+++ b/src/p2p/p2p_utils.c
@@ -611,3 +611,53 @@ void p2p_pref_channel_filter(const struct p2p_channels *p2p_chan,
 		res_reg->reg_class = reg->reg_class;
 	}
 }
+
+
+static int
+p2p_check_dfs_channel(int channel, u8 op_class,
+		      const struct ieee80211_dfs_ap_info_list *ap_list,
+		      unsigned int num_dfs_ap)
+{
+	unsigned int i;
+
+	for (i = 0; i < num_dfs_ap; i++) {
+		if (op_class == ap_list[i].op_class &&
+		    channel == ap_list[i].op_chan)
+			return 0;
+	}
+	return -1;
+}
+
+
+void p2p_dfs_channel_filter(struct p2p_data *p2p,
+			    const struct p2p_channels *p2p_chan,
+			    const struct ieee80211_dfs_ap_info_list *ap_list,
+			    size_t num_dfs_ap, struct p2p_channels *res)
+{
+	size_t i, j;
+
+	os_memset(res, 0, sizeof(*res));
+
+	for (i = 0; i < p2p_chan->reg_classes; i++) {
+		const struct p2p_reg_class *reg = &p2p_chan->reg_class[i];
+		struct p2p_reg_class *res_reg = &res->reg_class[i];
+
+		for (j = 0; j < reg->channels; j++) {
+			if (p2p->cfg->is_p2p_dfs_chan(p2p->cfg->cb_ctx, 0,
+						      reg->reg_class,
+						      reg->channel[j]) &&
+			    p2p_check_dfs_channel(reg->channel[j],
+						  reg->reg_class, ap_list,
+						  num_dfs_ap) < 0)
+				continue;
+
+			res_reg->channel[res_reg->channels++] =
+				reg->channel[j];
+		}
+
+		if (res_reg->channels == 0)
+			continue;
+		res->reg_classes++;
+		res_reg->reg_class = reg->reg_class;
+	}
+}
diff --git a/wpa_supplicant/config.c b/wpa_supplicant/config.c
index 9c5382f65..7e583d7a6 100644
--- a/wpa_supplicant/config.c
+++ b/wpa_supplicant/config.c
@@ -5509,6 +5509,8 @@ static const struct global_parse_data global_fields[] = {
 	{ INT_RANGE(p2p_twt_power_mgmt, 0, 1), 0 },
 	{ INT_RANGE(p2p_chan_switch_req_enable, 0, 1), 0 },
 	{ INT(p2p_reg_info), 0 },
+	{ INT(p2p_dfs_chan_enable), 0 },
+	{ INT(p2p_dfs_owner), 0 },
 	{ INT(dik_cipher), 0},
 	{ BIN(dik), 0 },
 #endif /* CONFIG_P2P */
diff --git a/wpa_supplicant/config.h b/wpa_supplicant/config.h
index 8df9eb583..dc2e481e5 100644
--- a/wpa_supplicant/config.h
+++ b/wpa_supplicant/config.h
@@ -908,6 +908,8 @@ struct wpa_config {
 	bool p2p_twt_power_mgmt;
 	bool p2p_chan_switch_req_enable;
 	int p2p_reg_info;
+	int p2p_dfs_chan_enable;
+	int p2p_dfs_owner;
 
 	struct wpabuf *wps_vendor_ext_m1;
 
diff --git a/wpa_supplicant/config_file.c b/wpa_supplicant/config_file.c
index 6a4d4c9ee..e0be98837 100644
--- a/wpa_supplicant/config_file.c
+++ b/wpa_supplicant/config_file.c
@@ -1400,7 +1400,9 @@ static void wpa_config_write_global(FILE *f, struct wpa_config *config)
 			config->p2p_chan_switch_req_enable);
 	if (config->p2p_reg_info)
 		fprintf(f, "p2p_reg_info=%d\n", config->p2p_reg_info);
-
+	if (config->p2p_dfs_chan_enable)
+		fprintf(f, "p2p_dfs_chan_enable=%d\n",
+			config->p2p_dfs_chan_enable);
 	if (WPA_GET_BE32(config->ip_addr_go))
 		fprintf(f, "ip_addr_go=%u.%u.%u.%u\n",
 			config->ip_addr_go[0], config->ip_addr_go[1],
diff --git a/wpa_supplicant/events.c b/wpa_supplicant/events.c
index a7c56f771..b5e31cecf 100644
--- a/wpa_supplicant/events.c
+++ b/wpa_supplicant/events.c
@@ -391,6 +391,7 @@ void wpa_supplicant_mark_disassoc(struct wpa_supplicant *wpa_s)
 	sme_clear_on_disassoc(wpa_s);
 	wpa_s->current_bss = NULL;
 	wpa_s->assoc_freq = 0;
+	wpa_s->assisted_dfs_go = false;
 
 	if (bssid_changed)
 		wpas_notify_bssid_changed(wpa_s);
@@ -2590,6 +2591,7 @@ static int _wpa_supplicant_event_scan_results(struct wpa_supplicant *wpa_s,
 
 	wpas_notify_scan_done(wpa_s, 1);
 
+	wpas_p2p_update_dfs_ap_info_list(wpa_s, scan_res);
 	if (ap) {
 		wpa_dbg(wpa_s, MSG_DEBUG, "Ignore scan results in AP mode");
 #ifdef CONFIG_AP
@@ -3796,6 +3798,35 @@ no_pfs:
 	}
 
 	wpa_s->assoc_freq = data->assoc_info.freq;
+	if (ieee80211_is_dfs(wpa_s->assoc_freq, wpa_s->hw.modes,
+			     wpa_s->hw.num_modes)) {
+		wpa_s->assisted_dfs_go = true;
+#ifdef CONFIG_P2P
+		if (wpa_s->global->p2p) {
+			u8 country[3];
+			const u8 *elem;
+
+			os_memset(country, 0, sizeof(country));
+			if (bssid_known) {
+				struct wpa_bss *bss =
+					wpa_bss_get_bssid_latest(wpa_s, bssid);
+
+				if (bss) {
+					elem = wpa_bss_get_ie(bss,
+							      WLAN_EID_COUNTRY);
+					if (elem && elem[1] >= 2) {
+						country[0] = *(elem + 2);
+						country[1] = *(elem + 3);
+						country[2] = 0x04;
+					}
+				}
+			}
+			p2p_update_dfs_ap_info(wpa_s->global->p2p, bssid,
+					       wpa_s->assoc_freq, 1, 0,
+					       country);
+		}
+#endif /* CONFIG_P2P */
+	}
 
 #ifndef CONFIG_NO_ROBUST_AV
 	wpas_handle_assoc_resp_qos_mgmt(wpa_s, data->assoc_info.resp_ies,
@@ -4736,6 +4767,25 @@ static void wpa_supplicant_event_disassoc_finish(struct wpa_supplicant *wpa_s,
 	wpa_sm_notify_disassoc(wpa_s->wpa);
 	ptksa_cache_flush(wpa_s->ptksa, wpa_s->bssid, WPA_CIPHER_NONE);
 
+#ifdef CONFIG_P2P
+	if (wpa_s->global->p2p) {
+		u8 country[3];
+
+		os_memset(country, 0, sizeof(country));
+		if (curr) {
+			const u8 *elem = wpa_bss_get_ie(curr, WLAN_EID_COUNTRY);
+
+			if (elem && elem[1] >= 2) {
+				country[0] = *(elem + 2);
+				country[1] = *(elem + 3);
+				country[2] = 0x04;
+			}
+		}
+		p2p_update_dfs_ap_info(wpa_s->global->p2p, bssid,
+				       wpa_s->assoc_freq, 0, 1, country);
+	}
+#endif /* CONFIG_P2P */
+
 	if (locally_generated)
 		wpa_s->disconnect_reason = -reason_code;
 	else
diff --git a/wpa_supplicant/p2p_supplicant.c b/wpa_supplicant/p2p_supplicant.c
index a6edbae96..f80e02790 100644
--- a/wpa_supplicant/p2p_supplicant.c
+++ b/wpa_supplicant/p2p_supplicant.c
@@ -2725,6 +2725,7 @@ wpas_p2p_init_group_interface(struct wpa_supplicant *wpa_s, int go)
 
 	wpas_p2p_clone_config(group_wpa_s, wpa_s);
 	group_wpa_s->p2p2 = wpa_s->p2p2;
+	group_wpa_s->assisted_dfs_go = wpa_s->assisted_dfs_go;
 
 	if (wpa_s->conf->p2p_interface_random_mac_addr) {
 		if (wpa_drv_set_mac_addr(group_wpa_s,
@@ -4412,8 +4413,11 @@ static int wpas_p2p_setup_channels(struct wpa_supplicant *wpa_s,
 		const struct oper_class_map *o = &global_op_class[op];
 		unsigned int ch;
 		struct p2p_reg_class *reg = NULL, *cli_reg = NULL;
+		bool check_dfs_supported =
+			(is_p2p_dfs_chan_enabled(wpa_s->global->p2p) &&
+			 is_dfs_global_op_class(o->op_class));
 
-		if (o->p2p == NO_P2P_SUPP ||
+		if ((!check_dfs_supported && o->p2p == NO_P2P_SUPP) ||
 		    (is_6ghz_op_class(o->op_class) && p2p_disable_6ghz))
 			continue;
 
@@ -4432,9 +4436,11 @@ static int wpas_p2p_setup_channels(struct wpa_supplicant *wpa_s,
 			    ch < 149 && ch + o->inc > 149)
 				ch = 149;
 
+			/* Allow DFS channels on GO */
+			wpa_s->p2p_go_allow_dfs = 1;
 			res = wpas_p2p_verify_channel(wpa_s, mode, o->op_class,
 						      ch, o->bw);
-			if (res == ALLOWED) {
+			if (res == ALLOWED || (res == RADAR && check_dfs_supported)) {
 				if (reg == NULL) {
 					if (cla == P2P_MAX_REG_CLASSES)
 						continue;
@@ -5551,6 +5557,19 @@ int wpas_p2p_mac_setup(struct wpa_supplicant *wpa_s)
 }
 
 
+static int wpas_p2p_dfs_chan(void *ctx, int freq, int op_class, int op_chan)
+{
+	struct wpa_supplicant *wpa_s = ctx;
+
+	if (freq == 0)
+		freq = ieee80211_chan_to_freq(NULL, op_class, op_chan);
+	if (ieee80211_is_dfs(freq, wpa_s->hw.modes, wpa_s->hw.num_modes))
+		return 1;
+
+	return 0;
+}
+
+
 /**
  * wpas_p2p_init - Initialize P2P module for %wpa_supplicant
  * @global: Pointer to global data from wpa_supplicant_init()
@@ -5622,6 +5641,9 @@ int wpas_p2p_init(struct wpa_global *global, struct wpa_supplicant *wpa_s)
 	p2p.prepare_data_element = wpas_p2p_prepare_data_element;
 	p2p.parse_data_element = wpas_p2p_parse_data_element;
 #endif /* CONFIG_PASN */
+	p2p.is_p2p_dfs_chan = wpas_p2p_dfs_chan;
+	p2p.dfs_chan_enable = wpa_s->conf->p2p_dfs_chan_enable;
+	p2p.dfs_owner = wpa_s->conf->p2p_dfs_owner;
 
 	os_memcpy(wpa_s->global->p2p_dev_addr, wpa_s->own_addr, ETH_ALEN);
 	os_memcpy(p2p.dev_addr, wpa_s->global->p2p_dev_addr, ETH_ALEN);
@@ -6599,7 +6621,7 @@ static int wpas_p2p_setup_freqs(struct wpa_supplicant *wpa_s, int freq,
 		else
 			ret = p2p_supported_freq_cli(wpa_s->global->p2p, freq);
 		if (!ret) {
-			if ((wpa_s->drv_flags & WPA_DRIVER_FLAGS_DFS_OFFLOAD) &&
+			if (is_p2p_dfs_chan_enabled(wpa_s->global->p2p) &&
 			    ieee80211_is_dfs(freq, wpa_s->hw.modes,
 					     wpa_s->hw.num_modes)) {
 				/*
@@ -11308,3 +11330,40 @@ void wpas_p2p_update_dev_addr(struct wpa_supplicant *wpa_s)
 	os_memcpy(wpa_s->global->p2p_dev_addr, wpa_s->own_addr, ETH_ALEN);
 	p2p_set_dev_addr(wpa_s->global->p2p, wpa_s->own_addr);
 }
+
+
+void wpas_p2p_update_dfs_ap_info_list(struct wpa_supplicant *wpa_s,
+				      struct wpa_scan_results *scan_res)
+{
+	size_t i;
+	struct p2p_data *p2p = wpa_s->global->p2p;
+	u8 country[3];
+
+	if (wpa_s->global->p2p_disabled || !p2p)
+		return;
+
+	for (i = 0; i < scan_res->num; i++) {
+		struct wpa_bss *bss;
+
+		bss = wpa_bss_get_bssid(wpa_s, scan_res->res[i]->bssid);
+		if (bss) {
+			const u8 *elem = wpa_bss_get_ie(bss, WLAN_EID_COUNTRY);
+
+			if (elem && elem[1] >= 2) {
+				country[0] = *(elem + 2);
+				country[1] = *(elem + 3);
+				country[2] = 0x04;
+			}
+		}
+		if (!ieee80211_is_dfs(scan_res->res[i]->freq, wpa_s->hw.modes,
+				      wpa_s->hw.num_modes))
+			continue;
+		if (scan_res->res[i]->flags & BIT(5)) {
+			p2p_update_dfs_ap_info(p2p, scan_res->res[i]->bssid,
+					       scan_res->res[i]->freq, 1, 0, country);
+		} else {
+			p2p_update_dfs_ap_info(p2p, scan_res->res[i]->bssid,
+					       scan_res->res[i]->freq, 0, 0, country);
+		}
+	}
+}
diff --git a/wpa_supplicant/p2p_supplicant.h b/wpa_supplicant/p2p_supplicant.h
index 888bce529..b2f9a358d 100644
--- a/wpa_supplicant/p2p_supplicant.h
+++ b/wpa_supplicant/p2p_supplicant.h
@@ -243,6 +243,8 @@ int wpas_p2p_pasn_auth_rx(struct wpa_supplicant *wpa_s,
 			  int freq);
 int wpas_p2p_get_pasn_ptk(struct wpa_supplicant *wpa_s, const u8 **ptk,
 			  size_t *ptk_len);
+void wpas_p2p_update_dfs_ap_info_list(struct wpa_supplicant *wpa_s,
+				      struct wpa_scan_results *scan_res);
 
 #else /* CONFIG_P2P */
 
@@ -386,6 +388,12 @@ static inline int wpas_p2p_get_pasn_ptk(struct wpa_supplicant *wpa_s,
 }
 #endif /* CONFIG_TESTING_OPTIONS */
 
+static inline void
+wpas_p2p_update_dfs_ap_info_list(struct wpa_supplicant *wpa_s,
+				 struct wpa_scan_results *scan_res)
+{
+}
+
 #endif /* CONFIG_P2P */
 
 #endif /* P2P_SUPPLICANT_H */
diff --git a/wpa_supplicant/wpa_supplicant_i.h b/wpa_supplicant/wpa_supplicant_i.h
index 38ad4857c..13d800841 100644
--- a/wpa_supplicant/wpa_supplicant_i.h
+++ b/wpa_supplicant/wpa_supplicant_i.h
@@ -1609,6 +1609,7 @@ struct wpa_supplicant {
 	bool last_scan_all_chan;
 	bool last_scan_non_coloc_6ghz;
 	bool support_6ghz;
+	bool assisted_dfs_go;
 
 	struct wpa_signal_info last_signal_info;
 
-- 
2.34.1

