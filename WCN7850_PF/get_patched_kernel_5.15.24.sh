# shell script to download 5.7.rc2 kernel source and patch it
#

#Download kernel
git clone https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git
cd ./linux
git checkout v5.15.24 -b myv5.15.24

#patch it
git am ../kernel_patches/*.patch

#update the config
sudo cp ../kernel_patches/config-5.15.24+ .config

#build the krenel image
sudo make-kpkg -j8 --initrd kernel_image kernel_headers


