From 8a161690c5a75aa216765f0ceb3b0112d399f2a3 Mon Sep 17 00:00:00 2001
From: Vinay Gannevaram <quic_vganneva@quicinc.com>
Date: Thu, 11 Jul 2024 10:22:13 +0530
Subject: [PATCH 01/16] sigma-dut: Add CAPI commands for USD Discovery

Add support for Unsynchronized service discovery

Change-Id: Ifb78aef24204b1334f1dd52a1cfb80ac0059c08f
Signed-off-by: Shivani Baranwal <quic_shivbara@quicinc.com>
---
 Android.mk  |   1 +
 Makefile    |   1 +
 p2p.c       |  15 ++
 sigma_dut.h |   6 +
 sta.c       |   9 ++
 usd.c       | 402 ++++++++++++++++++++++++++++++++++++++++++++++++++++
 utils.c     |   2 +
 7 files changed, 436 insertions(+)
 create mode 100644 usd.c

diff --git a/Android.mk b/Android.mk
index 12cf57a..81999b7 100644
--- a/Android.mk
+++ b/Android.mk
@@ -15,6 +15,7 @@ OBJS += atheros.c
 OBJS += ftm.c
 OBJS += dpp.c
 OBJS += dhcp.c
+OBJS += usd.c
 
 # Initialize CFLAGS to limit to local module
 CFLAGS =
diff --git a/Makefile b/Makefile
index f2a9918..479e313 100644
--- a/Makefile
+++ b/Makefile
@@ -54,6 +54,7 @@ OBJS += powerswitch.o
 OBJS += atheros.o
 OBJS += ftm.o
 OBJS += dpp.o
+OBJS += usd.o
 
 ifndef NO_TRAFFIC_AGENT
 CFLAGS += -DCONFIG_TRAFFIC_AGENT -DCONFIG_WFA_WMM_AC
diff --git a/p2p.c b/p2p.c
index 80ccbfc..d5104f2 100644
--- a/p2p.c
+++ b/p2p.c
@@ -865,6 +865,21 @@ noa_done:
 		}
 	}
 
+	val  = get_param(cmd, "UnsyncServDsc");
+	if (val && strcasecmp(val, "On") == 0) {
+	/* Support USD op */
+		dut->generic_service_disc = true;
+		sigma_dut_print(dut, DUT_MSG_INFO,
+				"Unsynchronized Service Discovery ON");
+	}
+
+	val  = get_param(cmd, "WFDR2Capabilities");
+	if (val && strcasecmp(val, "On") == 0) {
+	/* Support WiFi Direct R2 capabilities */
+		dut->p2p_r2_capable = true;
+		sigma_dut_print(dut, DUT_MSG_INFO,
+				"WiFi Direct R2 Capabilities ON");
+	}
 	return 1;
 }
 
diff --git a/sigma_dut.h b/sigma_dut.h
index 9833d98..643454b 100644
--- a/sigma_dut.h
+++ b/sigma_dut.h
@@ -1080,6 +1080,7 @@ struct sigma_dut {
 		PROGRAM_LOCR2,
 		PROGRAM_EHT,
 		PROGRAM_PR,
+		PROGRAM_P2P,
 	} program;
 
 	enum device_type {
@@ -1264,6 +1265,9 @@ struct sigma_dut {
 	int group_ciphers_capa; /* bitmap of enum sigma_cipher_suites values */
 	int group_mgmt_ciphers_capa; /* bitmap of enum sigma_cipher_suites
 				      * values */
+	bool p2p_r2_capable;
+	bool generic_service_disc;
+	char event_name[255];
 };
 
 
@@ -1563,4 +1567,6 @@ enum sigma_cmd_result dev_start_test_log(struct sigma_dut *dut,
 /* dnssd.c */
 int mdnssd_init(struct sigma_dut *dut);
 
+int usd_cmd_sta_exec_action(struct sigma_dut *dut, struct sigma_conn *conn,
+			    struct sigma_cmd *cmd);
 #endif /* SIGMA_DUT_H */
diff --git a/sta.c b/sta.c
index 0fd5e24..eb0ad77 100644
--- a/sta.c
+++ b/sta.c
@@ -11482,6 +11482,7 @@ static enum sigma_cmd_result cmd_sta_exec_action(struct sigma_dut *dut,
 						 struct sigma_cmd *cmd)
 {
 	const char *program = get_param(cmd, "Prog");
+	const char *method = get_param(cmd, "MethodType");
 
 	if (program && !get_param(cmd, "interface"))
 		return -1;
@@ -11490,6 +11491,14 @@ static enum sigma_cmd_result cmd_sta_exec_action(struct sigma_dut *dut,
 		return nan_cmd_sta_exec_action(dut, conn, cmd);
 #endif /* ANDROID_NAN */
 
+	if (program && strcasecmp(program, "P2P") == 0) {
+		dut->program = sigma_program_to_enum(program);
+		if (method &&
+		    (strcasecmp(method, "ADVERTISE") == 0 ||
+		     strcasecmp(method, "SEEK") == 0))
+			return usd_cmd_sta_exec_action(dut, conn, cmd);
+	}
+
 	if (program && strcasecmp(program, "Loc") == 0)
 		return loc_cmd_sta_exec_action(dut, conn, cmd);
 
diff --git a/usd.c b/usd.c
new file mode 100644
index 0000000..27a5227
--- /dev/null
+++ b/usd.c
@@ -0,0 +1,402 @@
+/*
+ * Sigma Control API DUT (USD functionality)
+ * Copyright (c) 2024, Qualcomm Innovation Center, Inc.
+ * All Rights Reserved.
+ * Licensed under the Clear BSD license. See README for more details.
+ */
+
+#include "sigma_dut.h"
+#include <sys/stat.h>
+#include "wpa_ctrl.h"
+#include "wpa_helpers.h"
+
+static int publish_id;
+static int subscribe_id;
+static char *p2p_peer_dev_addr;
+static struct wpa_ctrl *ctrl;
+
+static int usd_event(struct sigma_dut *dut,
+		     struct sigma_conn *conn,
+		     struct wpa_ctrl *ctrl,
+		     const char *intf)
+{
+	int res;
+	char buf[512], *pos;
+	const char *events[] = {
+		"P2P-DEVICE-FOUND",
+		NULL
+	};
+
+	res = get_wpa_cli_events(dut, ctrl, events, buf, sizeof(buf));
+	if (res < 0) {
+		send_resp(dut, conn, SIGMA_ERROR,
+			  "ErrorCode,USD operation did not complete");
+		return 0;
+	}
+
+	if (strstr(buf, "P2P-DEVICE-FOUND")) {
+		pos = strstr(buf, " subscribe_id=");
+		if (pos)
+			subscribe_id = atoi(pos + 14);
+
+		pos = strstr(buf, " publish_id=");
+		if (pos)
+			publish_id = atoi(pos + 12);
+
+		p2p_peer_dev_addr = strstr(buf, " address=");
+		if (!p2p_peer_dev_addr) {
+			sigma_dut_print(dut, DUT_MSG_ERROR,
+					"No P2P Device Address found");
+			return -2;
+		}
+		p2p_peer_dev_addr += 9;
+		if (strlen(p2p_peer_dev_addr) < 17) {
+			sigma_dut_print(dut, DUT_MSG_ERROR,
+					"Too short P2P Device Addr '%s'",
+					p2p_peer_dev_addr);
+			return -2;
+		}
+		p2p_peer_dev_addr[17] = '\0';
+		return 0;
+	}
+	return 0;
+}
+
+int p2p_cmd_sta_get_events(struct sigma_dut *dut, struct sigma_conn *conn,
+			   struct sigma_cmd *cmd)
+{
+	char resp_buf[512];
+	const char *action = get_param(cmd, "Action");
+	const char *ifname = get_param(cmd, "interface");
+	int ret = 0;
+
+	if (!action)
+		return 0;
+
+	if (!ifname)
+		ifname = get_station_ifname(dut);
+
+	memset(resp_buf, 0, sizeof(resp_buf));
+	/* Check action for start, stop and get events. */
+	if (strcasecmp(action, "Start") == 0) {
+		subscribe_id = 0;
+		publish_id = 0;
+		p2p_peer_dev_addr = NULL;
+		dut->event_name[0] = '\0';
+		ctrl = open_wpa_mon(ifname);
+		if (!ctrl) {
+			sigma_dut_print(dut, DUT_MSG_ERROR,
+					"Failed to open wpa_supplicant monitor connection");
+			return -2;
+		}
+		send_resp(dut, conn, SIGMA_COMPLETE, NULL);
+	} else if (strcasecmp(action, "Stop") == 0) {
+		subscribe_id = 0;
+		publish_id = 0;
+		p2p_peer_dev_addr = NULL;
+		dut->event_name[0] = '\0';
+		if (ctrl) {
+			wpa_ctrl_detach(ctrl);
+			wpa_ctrl_close(ctrl);
+			ctrl = NULL;
+		}
+		send_resp(dut, conn, SIGMA_COMPLETE, NULL);
+	} else if (strcasecmp(action, "Get") == 0) {
+		ret = usd_event(dut, conn, ctrl, ifname);
+		if (ret < 0) {
+			sigma_dut_print(dut, DUT_MSG_ERROR, "Failed to get USD event");
+			return -2;
+		}
+		sigma_dut_print(dut, DUT_MSG_DEBUG, "USD respbuf new %s,%d,%d,%s",
+				dut->event_name, subscribe_id, publish_id,
+				p2p_peer_dev_addr);
+		snprintf(resp_buf, sizeof(resp_buf),
+			 "%s,%d,%d,%s", dut->event_name, subscribe_id,
+			 publish_id, p2p_peer_dev_addr);
+		send_resp(dut, conn, SIGMA_COMPLETE, resp_buf);
+	} else {
+		send_resp(dut, conn, SIGMA_COMPLETE, "EventList,NONE");
+	}
+	return 0;
+}
+
+int sigma_usd_publish(struct sigma_dut *dut, struct sigma_conn *conn,
+		      struct sigma_cmd *cmd)
+{
+	const char *program = get_param(cmd, "Prog");
+	const char *ifname = get_param(cmd, "interface");
+	const char *service_name = get_param(cmd, "ServiceName");
+	const char *ssi = get_param(cmd, "ServSpecificInfoPayload");
+	const char *serv_proto_type = get_param(cmd, "ServProtoType");
+	const char *oper_chan = get_param(cmd, "AdvertiseChannel");
+	const char *bootstrapmethod = get_param(cmd, "PairingBootstrapMethod");
+	char buf[3000], ssi_hex[2048];
+	int i, len = 0, ret = 0, chan, freq = 0, ssi_len = 0;
+
+	if (!ifname)
+		ifname = get_station_ifname(dut);
+
+	memset(buf, 0, sizeof(buf));
+	if (bootstrapmethod) {
+		snprintf(buf, sizeof(buf), "P2P_SET pairing_setup 1");
+		if (wpa_command(ifname, buf) < 0) {
+			send_resp(dut, conn, SIGMA_ERROR,
+				  "ErrorCode,Failed to set pairing setup");
+			return -1;
+		}
+
+		memset(buf, 0, sizeof(buf));
+		snprintf(buf, sizeof(buf), "P2P_SET pairing_cache 1");
+		if (wpa_command(ifname, buf) < 0) {
+			send_resp(dut, conn, SIGMA_ERROR,
+				  "ErrorCode,Failed to set pairing cache");
+			return -1;
+		}
+		memset(buf, 0, sizeof(buf));
+		snprintf(buf, sizeof(buf), "P2P_SET pairing_verification 1");
+		if (wpa_command(ifname, buf) < 0) {
+			send_resp(dut, conn, SIGMA_ERROR,
+				  "ErrorCode,Failed to enable pairing verification");
+			return -1;
+		}
+	}
+
+	memset(buf, 0, sizeof(buf));
+	if (service_name) {
+		len = snprintf(buf, sizeof(buf), "NAN_PUBLISH service_name=%s ttl=100",
+			       service_name);
+		if (len < 0 || len > sizeof(buf))
+			return -2;
+	}
+	if (strcasecmp(program, "P2P") == 0) {
+		len += snprintf(buf + len, sizeof(buf) - len, " p2p=1");
+		if (len < 0 || len > sizeof(buf))
+			return -2;
+	}
+	if (ssi) {
+		ssi_len = atoi(ssi);
+		for (i = 0; i < ssi_len; i++)
+			sprintf(ssi_hex + i * 2, "%02x", i);
+		ssi_hex[ssi_len * 2] = '\0';
+		len += snprintf(buf + len, sizeof(buf) - len, " ssi=%s",
+				ssi_hex);
+		if (len < 0 || len > sizeof(buf))
+			return -2;
+	}
+	if (serv_proto_type) {
+		len += snprintf(buf + len, sizeof(buf) - len,
+				" srv_proto_type=%s", serv_proto_type);
+		if (len < 0 || len > sizeof(buf))
+			return -2;
+	}
+	if (oper_chan) {
+		chan = atoi(oper_chan);
+		freq = channel_to_freq(dut, chan);
+		len += snprintf(buf + len, sizeof(buf) - len, " freq=%d",
+				freq);
+		if (len < 0 || len > sizeof(buf))
+			return -2;
+	} else {
+		len += snprintf(buf + len, sizeof(buf) - len, " freq=%s",
+				"2437");
+		if (len < 0 || len > sizeof(buf))
+			return -2;
+	}
+
+	strlcpy(dut->event_name, "PUBLISHER", sizeof(dut->event_name));
+	dut->event_name[strlen(dut->event_name)] = '\0';
+	if (wpa_command(ifname, buf)) {
+		send_resp(dut, conn, SIGMA_ERROR, "Unable to USD publish");
+		return -3;
+	}
+	return ret;
+}
+
+int sigma_usd_subscribe(struct sigma_dut *dut, struct sigma_conn *conn,
+			struct sigma_cmd *cmd)
+{
+	const char *program = get_param(cmd, "Prog");
+	const char *ifname = get_param(cmd, "interface");
+	const char *service_name = get_param(cmd, "ServiceName");
+	const char *ssi = get_param(cmd, "ServSpecificInfoPayload");
+	const char *serv_proto_type = get_param(cmd, "ServProtoType");
+	const char *default_chan = get_param(cmd, "DefaultPublishChannel");
+	const char *chan_list = get_param(cmd, "PublishChannelList");
+	const char *bootstrapmethod = get_param(cmd, "PairingBootstrapMethod");
+	char freq_list[200], *token;
+	char buf[3000], buf1[20], ssi_hex[2048];
+	int i, len = 0, ret = 0, chan, freq = 0, ssi_len = 0;
+
+	if (!ifname)
+		ifname = get_station_ifname(dut);
+
+	memset(buf, 0, sizeof(buf));
+	if (bootstrapmethod) {
+		snprintf(buf, sizeof(buf), "P2P_SET pairing_setup 1");
+		if (wpa_command(ifname, buf) < 0) {
+			send_resp(dut, conn, SIGMA_ERROR,
+				  "ErrorCode,Failed to set pairing setup");
+			return -1;
+		}
+
+		memset(buf, 0, sizeof(buf));
+		snprintf(buf, sizeof(buf), "P2P_SET pairing_cache 1");
+		if (wpa_command(ifname, buf) < 0) {
+			send_resp(dut, conn, SIGMA_ERROR,
+				  "ErrorCode,Failed to set pairing cache");
+			return -1;
+		}
+		memset(buf, 0, sizeof(buf));
+		snprintf(buf, sizeof(buf), "P2P_SET pairing_verification 1");
+		if (wpa_command(ifname, buf) < 0) {
+			send_resp(dut, conn, SIGMA_ERROR,
+				  "ErrorCode,Failed to enable pairing verification");
+			return -1;
+		}
+	}
+
+	memset(buf, 0, sizeof(buf));
+	if (service_name) {
+		len = snprintf(buf, sizeof(buf), "NAN_SUBSCRIBE service_name=%s ttl=100",
+			       service_name);
+		if (len < 0 || len > sizeof(buf))
+			return -2;
+	}
+	if (strcasecmp(program, "P2P") == 0) {
+		len += snprintf(buf + len, sizeof(buf) - len, " p2p=1");
+		if (len < 0 || len > sizeof(buf))
+			return -2;
+	}
+	if (ssi) {
+		ssi_len = atoi(ssi);
+		for (i = 0; i < ssi_len; i++)
+			sprintf(ssi_hex + i * 2, "%02x", i);
+		ssi_hex[ssi_len * 2] = '\0';
+		len += snprintf(buf + len, sizeof(buf) - len, " ssi=%s",
+				ssi_hex);
+		if (len < 0 || len > sizeof(buf))
+			return -2;
+	}
+	if (serv_proto_type) {
+		len += snprintf(buf + len, sizeof(buf) - len,
+				" srv_proto_type=%s", serv_proto_type);
+		if (len < 0 || len > sizeof(buf))
+			return -2;
+	}
+	if (default_chan) {
+		chan = atoi(default_chan);
+		freq = channel_to_freq(dut, chan);
+		len += snprintf(buf + len, sizeof(buf) - len, " freq=%d",
+				freq);
+		if (len < 0 || len > sizeof(buf))
+			return -2;
+	} else {
+		len += snprintf(buf + len, sizeof(buf) - len, " freq=%s",
+				"2437");
+		if (len < 0 || len > sizeof(buf))
+			return -2;
+	}
+
+	if (chan_list) {
+		char *ch_list = strdup(chan_list);
+
+		token = strtok(ch_list, " ");
+		freq_list[0] = '\0';
+		while (token) {
+			chan = atoi(token);
+			freq = channel_to_freq(dut, chan);
+			sprintf(buf1, "%d", freq);
+			strcat(freq_list, buf1);
+			strcat(freq_list, ",");
+			token = strtok(NULL, " ");
+		}
+		if (strlen(freq_list) > 0)
+			freq_list[strlen(freq_list) - 1] = '\0';
+
+		len += snprintf(buf + len, sizeof(buf) - len, " freq_list=%s",
+				freq_list);
+		if (len < 0 || len > sizeof(buf))
+			return -2;
+	} else {
+		len += snprintf(buf + len, sizeof(buf) - len, " freq_list=%s",
+				"5180");
+		if (len < 0 || len > sizeof(buf))
+			return -2;
+	}
+
+	strlcpy(dut->event_name, "SUBSCRIBER", sizeof(dut->event_name));
+	dut->event_name[strlen(dut->event_name)] = '\0';
+	if (wpa_command(ifname, buf)) {
+		send_resp(dut, conn, SIGMA_ERROR, "Unable to USD subscribe");
+		return -3;
+	}
+	return ret;
+}
+
+int usd_cmd_sta_exec_action(struct sigma_dut *dut, struct sigma_conn *conn,
+			    struct sigma_cmd *cmd)
+{
+	int ret;
+	u8 len;
+	char buf[200];
+	const char *ifname = get_param(cmd, "interface");
+	const char *method_type = get_param(cmd, "MethodType");
+	const char *pairing_bootstrap_method = get_param(cmd, "PairingBootstrapMethod");
+	int bootstrap = 0;
+
+	if (!ifname)
+		ifname = get_station_ifname(dut);
+
+	if (!dut->generic_service_disc) {
+		send_resp(dut, conn, SIGMA_ERROR,
+			  "errorCode,Generic Service Discovery not enabled");
+		return INVALID_SEND_STATUS;
+	}
+
+	if (!method_type) {
+		send_resp(dut, conn, SIGMA_ERROR,
+			  "errorCode,Missing Generic Service Discovery Type");
+		return STATUS_SENT_ERROR;
+	}
+
+	if (pairing_bootstrap_method) {
+		if (strcasecmp(pairing_bootstrap_method, "NonZero") == 0) {
+			/* opportunistic */
+			bootstrap = BIT(0);
+			/* Display pincode and passphrase */
+			bootstrap |= BIT(1) | BIT(2);
+			/* Keypad pincode and passphrase */
+			bootstrap |= BIT(5) | BIT(6);
+		} else {
+			bootstrap = atoi(pairing_bootstrap_method);
+		}
+		len = snprintf(buf, sizeof(buf), "P2P_SET supported_bootstrapmethods %d",
+			       bootstrap);
+		if (len < 0 || len > sizeof(buf))
+			return ERROR_SEND_STATUS;
+		sigma_dut_print(dut, DUT_MSG_INFO, "P2_SET to wpa_command: %s", buf);
+		if (wpa_command(ifname, buf)) {
+			send_resp(dut, conn, SIGMA_ERROR,
+				  "Unable to set pairing bootstrap method");
+			return STATUS_SENT_ERROR;
+		}
+	}
+
+	if (strcasecmp(method_type, "ADVERTISE") == 0) {
+		ret = sigma_usd_publish(dut, conn, cmd);
+		if (ret < 0)
+			return ret;
+		send_resp(dut, conn, SIGMA_COMPLETE, "NULL");
+		return STATUS_SENT;
+	} else if (strcasecmp(method_type, "SEEK") == 0) {
+		ret = sigma_usd_subscribe(dut, conn, cmd);
+		if (ret < 0)
+			return ret;
+		send_resp(dut, conn, SIGMA_COMPLETE, "NULL");
+		return STATUS_SENT;
+	}
+
+	send_resp(dut, conn, SIGMA_ERROR,
+		  "errorCode,Unsupported USD MethodType");
+	return STATUS_SENT_ERROR;
+}
diff --git a/utils.c b/utils.c
index de813fa..92aec29 100644
--- a/utils.c
+++ b/utils.c
@@ -199,6 +199,8 @@ enum sigma_program sigma_program_to_enum(const char *prog)
 		return PROGRAM_PR;
 	if (strcasecmp(prog, "EHT") == 0)
 		return PROGRAM_EHT;
+	if (strcasecmp(prog, "P2P") == 0)
+		return PROGRAM_P2P;
 
 	return PROGRAM_UNKNOWN;
 }
-- 
2.34.1

