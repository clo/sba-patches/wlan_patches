From 1aa0a37397849b45d99c0da2d6e941b77bebac5f Mon Sep 17 00:00:00 2001
From: Wen Gong <wgong@codeaurora.org>
Date: Thu, 26 Mar 2020 17:33:23 +0800
Subject: [PATCH 72/76] ath11k: [fix conflict in core.h] add mutex to protected
 11d scan

When send more than 1 time of 11d scan start to firmware without 11d
scan stop, firmware will crash. Add mutex to protected ath11k's
vdev_id_11d_scan.

Tested with QCA6390 PCIe with firmware
WLAN.HST.1.0.1-01230-QCAHSTSWPLZ_V2_TO_X86-1.

Change-Id: Ie007e5bcdaf0a19ca1641e4b08f159bbc1faa7d0
Signed-off-by: Wen Gong <wgong@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/core.c |  1 +
 drivers/net/wireless/ath/ath11k/core.h |  3 +++
 drivers/net/wireless/ath/ath11k/mac.c  | 11 ++++++++++-
 3 files changed, 14 insertions(+), 1 deletion(-)

diff --git a/drivers/net/wireless/ath/ath11k/core.c b/drivers/net/wireless/ath/ath11k/core.c
index c542cad..85e6785 100644
--- a/drivers/net/wireless/ath/ath11k/core.c
+++ b/drivers/net/wireless/ath/ath11k/core.c
@@ -929,6 +929,7 @@ struct ath11k_base *ath11k_core_alloc(struct device *dev, size_t priv_size,
 
 	mutex_init(&ab->core_lock);
 	spin_lock_init(&ab->base_lock);
+	mutex_init(&ab->vdev_id_11d_lock);
 
 	INIT_LIST_HEAD(&ab->peers);
 	init_waitqueue_head(&ab->peer_mapping_wq);
diff --git a/drivers/net/wireless/ath/ath11k/core.h b/drivers/net/wireless/ath/ath11k/core.h
index cd0ddd5..60f44d9 100644
--- a/drivers/net/wireless/ath/ath11k/core.h
+++ b/drivers/net/wireless/ath/ath11k/core.h
@@ -708,6 +708,9 @@ struct ath11k_base {
 	bool fixed_mem_region;
 	bool use_register_windowing;
 	const struct ath11k_hw_regs *regs;
+
+	/* To synchronize 11d scan vdev id */
+	struct mutex vdev_id_11d_lock;
 	struct timer_list mon_reap_timer;
 	struct completion fw_mac_restart;
        /* must be last */
diff --git a/drivers/net/wireless/ath/ath11k/mac.c b/drivers/net/wireless/ath/ath11k/mac.c
index 23aff52..3fc645d 100644
--- a/drivers/net/wireless/ath/ath11k/mac.c
+++ b/drivers/net/wireless/ath/ath11k/mac.c
@@ -4346,11 +4346,13 @@ void ath11k_mac_11d_scan_start(struct ath11k *ar, u32 vdev_id)
 {
 	int ret;
 
+	mutex_lock(&ar->ab->vdev_id_11d_lock);
+
 	ath11k_dbg(ar->ab, ATH11K_DBG_MAC, "vdev id for 11d scan %d\n",
 		   ar->vdev_id_11d_scan);
 
 	if (ar->vdev_id_11d_scan != ATH11K_11D_INV_VDEV_ID)
-		return;
+		goto fin;
 
 	if (test_bit(WMI_TLV_SERVICE_11D_OFFLOAD, ar->ab->wmi_ab.svc_map) &&
 	    !(ath11k_mac_has_ap_up(ar->ab))) {
@@ -4369,6 +4371,9 @@ void ath11k_mac_11d_scan_start(struct ath11k *ar, u32 vdev_id)
 		else
 			ar->vdev_id_11d_scan = vdev_id;
 	}
+
+fin:
+	mutex_unlock(&ar->ab->vdev_id_11d_lock);
 }
 
 void ath11k_mac_11d_scan_stop(struct ath11k_base *ab)
@@ -4382,6 +4387,8 @@ void ath11k_mac_11d_scan_stop(struct ath11k_base *ab)
 	if (test_bit(WMI_TLV_SERVICE_11D_OFFLOAD, ab->wmi_ab.svc_map)) {
 		ath11k_dbg(ab, ATH11K_DBG_MAC, "stop 11d scan\n");
 
+		mutex_lock(&ab->vdev_id_11d_lock);
+
 		for (i = 0; i < ab->num_radios; i++) {
 			pdev = &ab->pdevs[i];
 			ar = pdev->ar;
@@ -4401,6 +4408,8 @@ void ath11k_mac_11d_scan_stop(struct ath11k_base *ab)
 					ar->vdev_id_11d_scan = ATH11K_11D_INV_VDEV_ID;
 			}
 		}
+
+		mutex_unlock(&ab->vdev_id_11d_lock);
 	}
 }
 
-- 
2.7.4

