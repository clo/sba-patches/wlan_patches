From b663b5df9dee30f90a7a5d5c9278ae1649e47b6b Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Fri, 20 Mar 2020 11:32:10 +0800
Subject: [PATCH 39/76] ath11k: free qmi allocated memory

Qmi allocated dma memory needs to be freed whenever necessary, such
as rmmod, qmi restart. Otherwise memory leak occurs.

Tested QCA6390 on X86 platform.

Change-Id: I3bf9e642e0e4ad021876ba5fdccd90fcd48c0bf5
Singed-off-by: Carl Huang <cjhuang@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/core.c |  1 +
 drivers/net/wireless/ath/ath11k/qmi.c  | 54 ++++++++++++++++++++++++++++++----
 drivers/net/wireless/ath/ath11k/qmi.h  |  4 +--
 3 files changed, 51 insertions(+), 8 deletions(-)

diff --git a/drivers/net/wireless/ath/ath11k/core.c b/drivers/net/wireless/ath/ath11k/core.c
index aa47358..70613ad 100644
--- a/drivers/net/wireless/ath/ath11k/core.c
+++ b/drivers/net/wireless/ath/ath11k/core.c
@@ -393,6 +393,7 @@ static void ath11k_core_soc_destroy(struct ath11k_base *ab)
 	ath11k_dp_free(ab);
 	ath11k_reg_free(ab);
 	ath11k_qmi_deinit_service(ab);
+	ath11k_qmi_free_resource(ab);
 }
 
 static int ath11k_core_pdev_create(struct ath11k_base *ab)
diff --git a/drivers/net/wireless/ath/ath11k/qmi.c b/drivers/net/wireless/ath/ath11k/qmi.c
index 779732e..6d0cb3c 100644
--- a/drivers/net/wireless/ath/ath11k/qmi.c
+++ b/drivers/net/wireless/ath/ath11k/qmi.c
@@ -1710,16 +1710,40 @@ static int ath11k_qmi_respond_fw_mem_request(struct ath11k_base *ab)
 	return ret;
 }
 
+static void ath11k_qmi_free_target_mem_chunk(struct ath11k_base *ab)
+{
+	int i;
+
+	if (ab->fixed_mem_region)
+		return;
+
+	for (i = 0; i < ab->qmi.mem_seg_count; i++) {
+		if (!ab->qmi.target_mem[i].vaddr)
+			continue;
+
+		dma_free_coherent(ab->dev,
+				  ab->qmi.target_mem[i].size,
+				  ab->qmi.target_mem[i].vaddr,
+				  ab->qmi.target_mem[i].paddr);
+		ab->qmi.target_mem[i].vaddr = NULL;
+	}
+}
+
 static int ath11k_qmi_alloc_target_mem_chunk(struct ath11k_base *ab)
 {
 	int i;
+	struct target_mem_chunk *chunk;
 
 	for (i = 0; i < ab->qmi.mem_seg_count; i++) {
-		ab->qmi.target_mem[i].vaddr = (unsigned long)dma_alloc_coherent(ab->dev, ab->qmi.target_mem[i].size,
-						&ab->qmi.target_mem[i].paddr, GFP_KERNEL);
-		if (!ab->qmi.target_mem[i].vaddr) {
-			ath11k_err(ab, "failed to allocate memory for FW, size: 0x%x, type: %u\n",
-				    ab->qmi.target_mem[i].size, ab->qmi.target_mem[i].type);
+		chunk = &ab->qmi.target_mem[i];
+		chunk->vaddr = dma_alloc_coherent(ab->dev,
+						  chunk->size,
+						  &chunk->paddr,
+						  GFP_KERNEL);
+		if (!chunk->vaddr) {
+			ath11k_err(ab, "failed to alloc memory, size: 0x%x, type: %u\n",
+				   chunk->size,
+				   chunk->type);
 			return -EINVAL;
 		}
 	}
@@ -1735,7 +1759,7 @@ static int ath11k_qmi_assign_target_mem_chunk(struct ath11k_base *ab)
 		switch (ab->qmi.target_mem[i].type) {
 		case BDF_MEM_REGION_TYPE:
 			ab->qmi.target_mem[idx].paddr = ATH11K_QMI_BDF_ADDRESS;
-			ab->qmi.target_mem[idx].vaddr = ATH11K_QMI_BDF_ADDRESS;
+			ab->qmi.target_mem[idx].vaddr = (void *)ATH11K_QMI_BDF_ADDRESS;
 			ab->qmi.target_mem[idx].size = ab->qmi.target_mem[i].size;
 			ab->qmi.target_mem[idx].type = ab->qmi.target_mem[i].type;
 			idx++;
@@ -2075,6 +2099,18 @@ static int ath11k_qmi_load_bdf_target_mem(struct ath11k_base *ab)
 	return ret;
 }
 
+static void ath11k_free_m3_bin(struct ath11k_base *ab)
+{
+	struct m3_mem_region *m3_mem = &ab->qmi.m3_mem;
+
+	if (!ab->m3_fw_support || !m3_mem->vaddr)
+		return;
+
+	dma_free_coherent(ab->dev, m3_mem->size,
+			  m3_mem->vaddr, m3_mem->paddr);
+	m3_mem->vaddr = NULL;
+}
+
 static int ath11k_load_m3_bin(struct ath11k_base *ab)
 {
 	struct m3_mem_region *m3_mem = &ab->qmi.m3_mem;
@@ -2633,6 +2669,12 @@ int ath11k_qmi_init_service(struct ath11k_base *ab)
 	return ret;
 }
 
+void ath11k_qmi_free_resource(struct ath11k_base *ab)
+{
+	ath11k_qmi_free_target_mem_chunk(ab);
+	ath11k_free_m3_bin(ab);
+}
+
 void ath11k_qmi_deinit_service(struct ath11k_base *ab)
 {
 	qmi_handle_release(&ab->qmi.handle);
diff --git a/drivers/net/wireless/ath/ath11k/qmi.h b/drivers/net/wireless/ath/ath11k/qmi.h
index 6d71895..6539249 100644
--- a/drivers/net/wireless/ath/ath11k/qmi.h
+++ b/drivers/net/wireless/ath/ath11k/qmi.h
@@ -87,7 +87,7 @@ struct target_mem_chunk {
 	u32 size;
 	u32 type;
 	dma_addr_t paddr;
-	u32 vaddr;
+	void *vaddr;
 };
 
 struct target_info {
@@ -459,5 +459,5 @@ void ath11k_qmi_event_work(struct work_struct *work);
 void ath11k_qmi_msg_recv_work(struct work_struct *work);
 void ath11k_qmi_deinit_service(struct ath11k_base *ab);
 int ath11k_qmi_init_service(struct ath11k_base *ab);
-
+void ath11k_qmi_free_resource(struct ath11k_base *ab);
 #endif
-- 
2.7.4

