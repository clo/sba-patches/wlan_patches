=================================================Code fetch================================================================================

1)	Clone code base: git clone https://git.kernel.org/pub/scm/linux/kernel/git/kvalo/ath.git -b master
2)	Reset to given tag: git checkout ath-202004230456
3)	Get patches from patchwork.kernel.org as below
	1.	https://patchwork.kernel.org/patch/11511395/
	2.	https://patchwork.kernel.org/patch/11511397/
	3.	https://patchwork.kernel.org/patch/11511399/
	4.	https://patchwork.kernel.org/patch/11530873/
	5.	https://patchwork.kernel.org/patch/11530879/
	6.	https://patchwork.kernel.org/patch/11530885/
	7.	https://patchwork.kernel.org/patch/11536035/
	8.	https://patchwork.kernel.org/patch/11536023/
	9.	https://patchwork.kernel.org/patch/11536031/
	10.	https://patchwork.kernel.org/patch/11536025/
4)	Get patches from codeaurora.org as below
	1.	git clone https://source.codeaurora.org/external/sba/wlan_patches
	2.	get patches from: <workspace>/wlan_patches/QCA6390_ath11k/M1
5)	Apply the patches of step 3) and 4) in below order one by one, total 86 patches. Please note the bottom is the first.
	ath11k: support MAC address randomization in scan
	ath11k: [fix conflict in mac.c] add support for hardware rfkill
	ath11k: enable pkt log default for QCA6390
	ath11k: add regdb.bin download for regdb offload
	ath11k: [fix conflict in core.h] add mutex to protected 11d scan
	ath11k: add 11d scan offload support
	ath11k: add handler for WMI_SET_CURRENT_COUNTRY_CMDID
	ath11k: skip sending vdev down for channel switch
	ath11k: add wmi op version indication for UTF
	ath11k: change check from ATH11K_STATE_ON to ATH11K_STATE_TM for UTF
	ath11k: add support for UTF mode for QCA6390
	ath11k: [fix conflict in mac.c] add hw connection monitor support
	ath11k: [fix conflict in wmi.c core.h ahb.c] factory test mode support
	ath11k: disable OTP write privilege
	ath11k: fix PCI L1ss clock unstable problem
	ath11k: reset registers related to PCI hot reset
	ath11k: reset MHI during power down and power up
	ath11k: enable idle power save mode
	ath11k: enable CE pipe 4 fix for shadow register
	ath11k: enable reo_cmd fix for shadow register
	ath11k: enable shadow register fix for Tx data path
	ath11k: enable shadow register workaround for WMI pipe
	ath11k: enable shadow register configuration and access
	ath11k: read and write registers below unwindowed address
	ath11k: output wow page fault information
	ath11k: purge rx pktlog when entering WoW
	ath11k: implement hw data filter
	ath11k: implement WoW net-detect functionality
	ath11k: add basic WoW functionality
	ath11k: set credit_update flag for flow controlled ep only
	ath11k: check ATH11K_FLAG_CORE_STOPPED in resume and suspend
	ath11k: read select_window register to ensure write is finished
	ath11k: implement hif suspend and resume functions.
	ath11k: hook mhi suspend and resume
	ath11k: enable non-wow suspend and resume
	ath11k: enable ps config for station
	ath11k: enable pktlog
	ath11k: free qmi allocated memory
	ath11k: add shutdown function in ath11k_pci_driver
	ath11k: fix ath11k_pci rmmod crash
	ath11k: use TCL_DATA_RING_0 for QCA6390
	ath11k: process both lmac rings for QCA6390
	ath11k: assign correct search flag and type for QCA6390
	ath11k: delay vdev_start for QCA6390
	ath11k: setup QCA6390 rings for both rxdmas
	ath11k: don't initialize rxdma1 related ring
	ath11k: enable DP interrupt setup for QCA6390
	ath11k: move ring mask to ahb module
	ath11k: redefine peer_map and peer_unmap
	ath11k: put hardware to dbs mode
	ath11k: initialize wmi config based on hw_params
	ath11k: force single pdev only for QCA6390
	ath11k: disable CE interupt before hif start
	ath11k: get msi_addr and msi_data before srng setup
	ath11k: assign msi_addr and msi_data to srng
	ath11k: define ATH11K_IRQ_CE0_OFFSET to 3 for QCA6390
	ath11k: define different ce count
	ath11k: attach register offset dynamically
	ath11k: fix len issue in ath11k_pci_init_qmi_ce_config
	ath11k: enable internal sleep clock
	ath11k: attach hw parameter
	ath11k: fix KASAN warning of ath11k_qmi_wlanfw_wlan_cfg_send
	ath11k: fix memory OOB access in qmi_decode
	ath11k: change bdf to elf type
	ath11k: allocate small chunk memory for fw request
	ath11k: Do not depend on ARCH_QCOM for ath11k
	ath11k: do not return a value in ath11k_get_msi_address
	ath11k: remove execution of qmi.c
	ath11k: setup ce tasklet for control path
	ath11k: configure copy engine msi address in CE srng
	ath11k: Add reg read/write/window select ops
	ath11k: Fill appropriate QMI service instance id for QCA6x90
	ath11k: Modify QMI handshakes in compatible with QCA6x90 chipset
	ath11k: setup resource initialization for QCA6x90
	ath11k: fix compile errors of mhi.c with latest linaro mhi
	ath11k: fix the difference caused by public patches
	ath11k: Register mhi controller device for qca6390
	ath11k: Add msi config init for QCA6390
	ath11k: setup pci resource for QCA6390 target
	ath11k: Add PCI client driver for QCA6390 chipset
	ath11k: Remove bus layer includes from upper layer
	ath11k: Add drv private for bus opaque struct
	ath11k: Add support for multibus support
	net: qrtr: Do not depend on ARCH_QCOM
	net: qrtr: Add MHI transport layer
	bus: mhi: core: Add support for MHI suspend and resume

=================================================Compilation================================================================================

1)	make menuconfig and change config and save
	run cmd: make menuconfig and select the following 
	[M]Device Drivers ---> Bus devices ---> Modem Host Interface[CONFIG_MHI_BUS=m]
	[M]Device Drivers ---> SOC (System On Chip) specific Drivers ---> Qualcomm SoC drivers ---> Qualcomm qmi helpers[CONFIG_QCOM_QMI_HELPERS=m]
	[M]Networking support ---> Networking options ---> Qualcomm IPC Router support[CONFIG_QRTR=m]
	[M]Networking support ---> Networking options ---> MHI IPC Router channels[CONFIG_QRTR_MHI=m]
	[M]Device Drivers ---> Network device support ---> Wireless LAN ---> Qualcomm Technologies 802.11ax chipset support[CONFIG_ATH11K=m]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> Atheros ath11k PCI support[CONFIG_ATH11K_PCI=m]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> QCA ath11k debugging[CONFIG_ATH11K_DEBUG=y]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> QCA ath11k debugfs support[CONFIG_ATH11K_DEBUGFS=y]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> ath11k tracing support[CONFIG_ATH11K_TRACING=y]
	[*]Device Drivers ---> Character devices ---> Serial device bus
	[*]Networking support ---> Bluetooth subsystem support ---> Bluetooth device drivers ---> Qualcomm Atheros protocol support

2)	To build the kernel follow the steps 
	1.	make
	2.	sudo make modules_install
	3.	sudo make install

