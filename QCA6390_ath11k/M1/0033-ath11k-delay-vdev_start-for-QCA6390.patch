From e2429446f77ceabca928f901059bcf96b09ace46 Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Tue, 3 Dec 2019 16:49:35 +0200
Subject: [PATCH 33/76] ath11k: delay vdev_start for QCA6390

For QCA6390 firmware, bss peer must be created before vdev_start,
so deley vdev_start until bss peer is created.

Tested QCA6390 on X86 platform.

Change-Id: I7e1f550f9927a9f0907a5788e7f6dfff5ffdafe0
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
Signed-off-by: Kalle Valo <kvalo@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/core.c |  2 ++
 drivers/net/wireless/ath/ath11k/core.h |  1 +
 drivers/net/wireless/ath/ath11k/hw.h   |  1 +
 drivers/net/wireless/ath/ath11k/mac.c  | 51 ++++++++++++++++++++++++++++++++++
 4 files changed, 55 insertions(+)

diff --git a/drivers/net/wireless/ath/ath11k/core.c b/drivers/net/wireless/ath/ath11k/core.c
index 59a1865..85fb49f 100644
--- a/drivers/net/wireless/ath/ath11k/core.c
+++ b/drivers/net/wireless/ath/ath11k/core.c
@@ -33,6 +33,7 @@ static const struct ath11k_hw_params ath11k_hw_params_list[] = {
 		.misc_caps = MISC_CAPS_BAND_TO_MAC,
 		.rxdma1_enable = true,
 		.num_rxmda_per_pdev = 1,
+		.vdev_start_delay = false,
 	},
 	{
 		.name = "qca6390",
@@ -43,6 +44,7 @@ static const struct ath11k_hw_params ath11k_hw_params_list[] = {
 		.misc_caps = MISC_CAPS_HOST2FW_RXBUF_RING,
 		.rxdma1_enable = false,
 		.num_rxmda_per_pdev = 2,
+		.vdev_start_delay = true,
 	}
 };
 
diff --git a/drivers/net/wireless/ath/ath11k/core.h b/drivers/net/wireless/ath/ath11k/core.h
index 218de08..c4b760a 100644
--- a/drivers/net/wireless/ath/ath11k/core.h
+++ b/drivers/net/wireless/ath/ath11k/core.h
@@ -209,6 +209,7 @@ struct ath11k_vif {
 	int num_legacy_stations;
 	int rtscts_prot_mode;
 	int txpower;
+	struct ieee80211_chanctx_conf chanctx;
 };
 
 struct ath11k_vif_iter {
diff --git a/drivers/net/wireless/ath/ath11k/hw.h b/drivers/net/wireless/ath/ath11k/hw.h
index 8d2217f..402fd0e 100644
--- a/drivers/net/wireless/ath/ath11k/hw.h
+++ b/drivers/net/wireless/ath/ath11k/hw.h
@@ -136,6 +136,7 @@ struct ath11k_hw_params {
 	u32 misc_caps;
 	bool rxdma1_enable;
 	int num_rxmda_per_pdev;
+	bool vdev_start_delay;
 };
 
 struct ath11k_fw_ie {
diff --git a/drivers/net/wireless/ath/ath11k/mac.c b/drivers/net/wireless/ath/ath11k/mac.c
index 99cb05f..520ac2f 100644
--- a/drivers/net/wireless/ath/ath11k/mac.c
+++ b/drivers/net/wireless/ath/ath11k/mac.c
@@ -156,6 +156,10 @@ static const u32 ath11k_smps_map[] = {
 	[WLAN_HT_CAP_SM_PS_DISABLED] = WMI_PEER_SMPS_PS_NONE,
 };
 
+static int
+ath11k_start_vdev_delay(struct ieee80211_hw *hw,
+			struct ieee80211_vif *vif);
+
 u8 ath11k_mac_bw_to_mac80211_bw(u8 bw)
 {
 	u8 ret = 0;
@@ -2850,6 +2854,9 @@ static int ath11k_mac_station_add(struct ath11k *ar,
 		goto free_tx_stats;
 	}
 
+	if (ab->hw_params.vdev_start_delay)
+		ret = ath11k_start_vdev_delay(ar->hw, vif);
+
 	return 0;
 
 free_tx_stats:
@@ -4915,6 +4922,43 @@ static void ath11k_mac_op_change_chanctx(struct ieee80211_hw *hw,
 }
 
 static int
+ath11k_start_vdev_delay(struct ieee80211_hw *hw,
+			struct ieee80211_vif *vif)
+{
+	struct ath11k *ar = hw->priv;
+	struct ath11k_base *ab = ar->ab;
+	struct ath11k_vif *arvif = (void *)vif->drv_priv;
+	int ret;
+
+	if (WARN_ON(arvif->is_started)) {
+		mutex_unlock(&ar->conf_mutex);
+		return -EBUSY;
+	}
+
+	ret = ath11k_mac_vdev_start(arvif, &arvif->chanctx.def);
+	if (ret) {
+		ath11k_warn(ab, "failed to start vdev %i addr %pM on freq %d: %d\n",
+			    arvif->vdev_id, vif->addr,
+			    arvif->chanctx.def.chan->center_freq, ret);
+		goto err;
+	}
+	if (arvif->vdev_type == WMI_VDEV_TYPE_MONITOR) {
+		ret = ath11k_monitor_vdev_up(ar, arvif->vdev_id);
+		if (ret)
+			goto err;
+	}
+
+	arvif->is_started = true;
+
+	/* TODO: Setup ps and cts/rts protection */
+	return 0;
+
+err:
+	mutex_unlock(&ar->conf_mutex);
+	return ret;
+}
+
+static int
 ath11k_mac_op_assign_vif_chanctx(struct ieee80211_hw *hw,
 				 struct ieee80211_vif *vif,
 				 struct ieee80211_chanctx_conf *ctx)
@@ -4930,6 +4974,13 @@ ath11k_mac_op_assign_vif_chanctx(struct ieee80211_hw *hw,
 		   "mac chanctx assign ptr %pK vdev_id %i\n",
 		   ctx, arvif->vdev_id);
 
+	//For QCA6390,bss peer must be created before vdev_start.
+	if (ab->hw_params.vdev_start_delay) {
+		memcpy(&arvif->chanctx, ctx, sizeof(*ctx));
+		mutex_unlock(&ar->conf_mutex);
+		return 0;
+	}
+
 	if (WARN_ON(arvif->is_started)) {
 		mutex_unlock(&ar->conf_mutex);
 		return -EBUSY;
-- 
2.7.4

