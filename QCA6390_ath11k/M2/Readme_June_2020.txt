=================================================Code fetch================================================================================

1)	Clone code base: git clone https://git.kernel.org/pub/scm/linux/kernel/git/kvalo/ath.git -b master

2)	Reset to given tag: git checkout ath-202006090627

3)	Get patches from patchwork.kernel.org as below
	1.	https://patchwork.kernel.org/patch/11511395/
	2.	https://patchwork.kernel.org/patch/11536035/
	3.	https://patchwork.kernel.org/patch/11536023/
	4.	https://patchwork.kernel.org/patch/11536031/
	5.	https://patchwork.kernel.org/patch/11536025/
	
4)	Get patches from git.kernel.org as below
	1.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit?id=cd116318803f5ee222301f3525578241a04822ee
	2.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=44d4e063d18b87c0fef1b19f7883f10e71c544b6
	3.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=020960685041fc09ab6a23cf244477cdcbb75c5f
	4.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=ee75cedf82d832561af8ba8380aeffd00a9eea77
	5.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=560e3a045961ed0c7184ef9f6a93b95bd38c1c48
	6.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=a21eaf592fad132cb60173ed494e54c5b9c82c9a
	7.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=0a895f091ebd943555efce3d7b0e96d667208fdc
	8.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=0ab9fcd0e7b2b45dd407e145f50fd55091405d3c
	9.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=bc7ccce5a5192cf277da0aef05e45cd92c81c79a
	10.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=3c1bd0047124f506294520341cfe03e19ea773e3
	11.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=ada5e1def5da623cbf5709c921fd22cc321cd233
	12.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=30b7892417c50a05bf4a2ad56c2e1355d4d1404a
	13.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=3bc1a5f431618a656bbf674a4627ef4da3a0d893
	14.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=e18d4e9fa79bb27de6447c0c172bb1c428a52bb2

5)	Get patches from codeaurora.org as below
	1.	git clone https://source.codeaurora.org/external/sba/wlan_patches
	2.	get patches from: <workspace>/wlan_patches/QCA6390_ath11k/M2
	
6)	Apply the patches of step 3)/4)/5) in below order one by one, total 150 patches. Please note the bottom is the first.
	ath11k: change 11d scan to stop when assoc and start when disassoc
	ath11k: put ath11k fw files to ath11k related directory
	ath11k: init hw param at earlier phase
	ath11k: change 11d scan to period run in firmware
	ath11k: fix ZERO address in probe request
	ath11k: fix scan fail for the 1st time of load
	ath11k: add wait opeartion for tx management packets for flush from mac8
	ath11k: improve check logic of start reset
	ath11k: remove wakeup before powerdown MHI
	ath11k: fix some memory leak
	ath11k: fix crash caused by memory overflow in arp_ns offload
	ath11k: destroy workqueue when module is unloaded
	ath11k: allocate m3 buffer before m3 downloading
	ath11k: change rfkill config to after qmi firmware ready
	ath11k: support gtk rekey offload
	ath11k: support arp and ns offload
	ath11k: dump sram if firmware bootup fails
	change makefile of mhi
	ath11k: add hw-restart for simulate_fw_crash
	ath11k: report rssi of each chain to mac80211
	ath11k: add support for device recovery for QCA6390
	ath11k: remove some reset operation of ath11k_pci_sw_reset
	ath11k: add support for hardware rfkill
	ath11k: enable pkt log default for QCA6390
	ath11k: add regdb.bin download for regdb offload with match board-2.bin
	ath11k: add mutex to protected 11d scan
	ath11k: add 11d scan offload support
	ath11k: add handler for WMI_SET_CURRENT_COUNTRY_CMDID
	ath11k: skip sending vdev down for channel switch
	ath11k: add wmi op version indication for UTF
	ath11k: change check from ATH11K_STATE_ON to ATH11K_STATE_TM for UTF
	ath11k: add support for UTF mode for QCA6390
	ath11k: remove ATH11K_STATE_TM check for restart
	ath11k: factory test mode support from WIN team v6
	ath11k: add hw connection monitor support
	ath11k: config shadow again whenever power up happens
	ath11k: set dtim policy to stick mode for station interface
	ath11k: fix crash caused by NULL rx_channel
	ath11k: disable ASPM L0sLs before downloading firmware
	ath11k: support MAC address randomization in scan
	ath11k: disable OTP write privilege
	ath11k: fix PCI L1ss clock unstable problem
	ath11k: reset registers related to PCI hot reset
	ath11k: reset MHI during power down and power up
	ath11k: enable idle power save mode
	ath11k: Start a timer to update HP for CE pipe 4
	ath11k: start a timer to update REO cmd ring
	ath11k: Start a timer to update TCL HP
	ath11k: set WMI pipe credit to 1 for QCA6390
	ath11k: enable shadow register configuration and access
	ath11k: read and write registers below unwindowed address
	ath11k: output wow page fault information
	ath11k: purge rx pktlog when entering WoW
	ath11k: implement hw data filter
	ath11k: implement WoW net-detect functionality
	ath11k: add basic WoW functionality
	ath11k: set credit_update flag for flow controlled ep only
	ath11k: check ATH11K_FLAG_CORE_STOPPED in resume and suspend
	ath11k: read select_window register to ensure write is finished
	ath11k: implement hif suspend and resume functions.
	ath11k: hook mhi suspend and resume
	ath11k: enable non-wow suspend and resume
	ath11k: enable ps config for station
	ath11k: enable pktlog
	ath11k: free qmi allocated memory
	ath11k: add shutdown function in ath11k_pci_driver
	ath11k: fix ath11k_pci rmmod crash
	ath11k: use TCL_DATA_RING_0 for QCA6390
	ath11k: process both lmac rings for QCA6390
	ath11k: assign correct search flag and type for QCA6390
	ath11k: delay vdev_start for QCA6390
	ath11k: refine the phy_id check in ath11k_reg_chan_list_event
	ath11k: setup QCA6390 rings for both rxdmas
	ath11k: don't initialize rxdma1 related ring
	ath11k: enable DP interrupt setup for QCA6390
	ath11k: move ring mask to ahb module
	ath11k: redefine peer_map and peer_unmap
	ath11k: put hardware to dbs mode
	ath11k: initialize wmi config based on hw_params
	ath11k: force single pdev only for QCA6390
	ath11k: disable CE interupt before hif start
	ath11k: get msi_addr and msi_data before srng setup
	ath11k: assign msi_addr and msi_data to srng
	ath11k: define ATH11K_IRQ_CE0_OFFSET to 3 for QCA6390
	ath11k: define different ce count
	ath11k: attach register offset dynamically
	ath11k: enable internal sleep clock
	ath11k: attach hw parameter
	ath11k: fix KASAN warning of ath11k_qmi_wlanfw_wlan_cfg_send
	ath11k: fix memory OOB access in qmi_decode
	ath11k: change bdf to elf type
	ath11k: allocate small chunk memory for fw request
	ath11k: Do not depend on ARCH_QCOM for ath11k
	ath11k: setup ce tasklet for control path
	ath11k: configure copy engine msi address in CE srng
	ath11k: Add reg read/write/window select ops
	ath11k: Fill appropriate QMI service instance id for QCA6x90
	ath11k: Add board-2.bin support for QCA6x90 target
	ath11k: Add m3 bin download support
	ath11k: Use remoteproc only for non MHI targets
	ath11k: setup resource initialization for QCA6x90
	ath11k: fix compile errors of mhi.c based on public mhi module
	ath11k: Register mhi controller device for qca6390
	ath11k: Add msi config init for QCA6390
	ath11k: setup pci resource for QCA6390 target
	ath11k: Add PCI client driver for QCA6390 chipset
	bus: mhi: core: Use helper API to trigger a non-blocking host resume
	bus: mhi: core: Do not acquire channel mutex for LPM status callbacks
	bus: mhi: core: Check execution environment for channel before issuing reset
	bus: mhi: core: Resume both host and device before sending commands
	bus: mhi: core: Add support to pause or resume channel transfers
	bus: mhi: clients: Add user space client interface driver
	bus: mhi: core: Move MHI_MAX_MTU to external header file
	bus: mhi: core: Add helper API to return number of free TREs
	bus: mhi: core: Add support for device requested bandwidth scaling
	bus: mhi: core: Add support to read capability id from MISC reg
	bus: mhi: core: Add support for processing of sleepable events
	bus: mhi: core: Add helper API to ring event ring doorbell
	bus: mhi: core: Add support for processing priority of event ring
	bus: mhi: core: Skip device votes if device is in error or shutdown state
	bus: mhi: core: Add low power mode wait for fast/silent suspends
	bus: mhi: core: Add support for fast or silent suspend and resume
	bus: mhi: core: Process execution environment changes serially
	bus: mhi: core: Introduce support for manual AMSS loading
	bus: mhi: core: Use common name for BHI firmware load function
	bus: mhi: core: Check for RDDM support before forcing a device crash
	bus: mhi: core: Mark device inactive soon after host issues a shutdown
	bus: mhi: core: Improve shutdown handling after link down detection
	bus: mhi: core: Introduce sysfs entries for MHI
	bus: mhi: core: Read and save device hardware information from BHI
	bus: mhi: core: Introduce debugfs entries and counters for MHI
	bus: mhi: core: Introduce helper function to check device state
	bus: mhi: core: Use generic name field for an MHI device
	bus: mhi: core: Introduce independent voting mechanism
	bus: mhi: core: Abort suspends due to outgoing pending packets
	bus: mhi: core: Handle syserr during power_up
	bus: mhi: core: Handle write lock properly in mhi_pm_m0_transition
	bus: mhi: core: Do not process SYS_ERROR if RDDM is supported
	bus: mhi: core: Skip handling BHI irq if MHI reg access is not allowed
	bus: mhi: core: Handle disable transitions in state worker
	bus: mhi: core: Remove the system error worker thread
	bus: mhi: core: Ensure non-zero session or sequence ID values are used
	bus: mhi: core: Improve debug logs for loading firmware
	bus: mhi: core: Return appropriate error codes for AMSS load failure
	bus: mhi: core: Handle firmware load using state worker
	bus: mhi: core: Read transfer length from an event properly
	bus: mhi: core: Add range check for channel id received in event ring
	bus: mhi: core: Cache intmod from mhi event to mhi channel
	bus: mhi: core: Refactor mhi queue APIs
	bus: mhi: core: Add support for MHI suspend and resume

=================================================Compilation================================================================================

1)	make menuconfig and change config and save
	run cmd: make menuconfig and select the following 
	[M]Device Drivers ---> Bus devices ---> Modem Host Interface[CONFIG_MHI_BUS=m]
	[M]Device Drivers ---> SOC (System On Chip) specific Drivers ---> Qualcomm SoC drivers ---> Qualcomm qmi helpers[CONFIG_QCOM_QMI_HELPERS=m]
	[M]Networking support ---> Networking options ---> Qualcomm IPC Router support[CONFIG_QRTR=m]
	[M]Networking support ---> Networking options ---> MHI IPC Router channels[CONFIG_QRTR_MHI=m]
	[M]Device Drivers ---> Network device support ---> Wireless LAN ---> Qualcomm Technologies 802.11ax chipset support[CONFIG_ATH11K=m]
	[M]Device Drivers ---> Network device support ---> Wireless LAN ---> Qualcomm Technologies 802.11ax chipset PCI support[CONFIG_ATH11K_PCI=m]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> QCA ath11k debugging[CONFIG_ATH11K_DEBUG=y]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> QCA ath11k debugfs support[CONFIG_ATH11K_DEBUGFS=y]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> ath11k tracing support[CONFIG_ATH11K_TRACING=y]
	[*]Device Drivers ---> Character devices ---> Serial device bus
	[*]Networking support ---> Bluetooth subsystem support ---> Bluetooth device drivers ---> Qualcomm Atheros protocol support
	[*]Networking support ---> Wireless ---> cfg80211 certification onus[CONFIG_CFG80211_CERTIFICATION_ONUS=y]
	[*]Networking support ---> Wireless ---> nl80211 testmode command[CONFIG_NL80211_TESTMODE=y]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> Atheros dynamic user regulatory hints[CONFIG_ATH_REG_DYNAMIC_USER_REG_HINTS=y]

2)	To build the kernel follow the steps 
	1.	make
	2.	sudo make modules_install
	3.	sudo make install


======================================WLAN bring up=======================================================================   
 
1)	Copy firmware files
	1. copy firmware https://github.com/kvalo/ath11k-firmware/tree/master/QCA6390/hw2.0/WLAN.HST.1.0.1-01740-QCAHSTSWPLZ_V2_TO_X86-1
	2. Copy all firmware binary files to /lib/firmware and rename bd file 
	3. sudo cp amss.mbn /lib/firmware/amss.bin 
	4. sudo cp bdwlan.e04 /lib/firmware/bdwlan.bin 
	5. sudo cp m3.bin  /lib/firmware
 
2)	Load modules
	sudo modprobe cfg80211
	sudo modprobe mac80211

	cd /lib/modules/5.7.0-rc7-wt-ath+/kernel/drivers/bus/mhi/core
	sudo insmod mhi.ko

	cd /lib/modules/5.7.0-rc7-wt-ath+/kernel/drivers/soc/qcom
	sudo insmod qmi_helpers.ko

	cd /lib/modules/5.7.0-rc7-wt-ath+/kernel/drivers/net/wireless/ath/ath11k
	sudo insmod ath11k.ko debug_mask=0xffffffff
	sudo insmod ath11k_pci.ko

3)	Use ifconfig to check whether the WiFi interface is up. If yes, try scan and connect AP from Ubuntu Network Manager GUI

4)	If wifi is loaded successfully, try Reboot system see whether wifi driver can be loaded automatically and successfully 


======================================Bluetooth bring up=======================================================================   

1)	Get BT firmware htbtfw20.tlv and htnv20.bin from
	https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git/tree/qca
	
2)	Copy the htbtfw20.tlv and htnv20.bin into /lib/firmware/qca

3)	Reboot the DUT and BT should be workable now, you can check that by hciconfig command
