From 9adaf87808f7736ab7390c54aac3633436f41c71 Mon Sep 17 00:00:00 2001
From: Shi Hong <quic_hongsh@quicinc.com>
Date: Fri, 28 Oct 2022 11:58:40 +0800
Subject: [PATCH] qcacmn: multicast to unicast conversion

Change-Id: Ie71e4df115b79172d824cef3a73906330730ac1e

Signed-off-by: Shi Hong <quic_hongsh@quicinc.com>
---
 dp/wifi3.0/dp_main.c    | 239 +++++++++++++++++++++++++++++++++++++++++++++++-
 dp/wifi3.0/dp_tx.c      |  30 +++++-
 dp/wifi3.0/dp_tx_desc.h |   3 +
 3 files changed, 266 insertions(+), 6 deletions(-)

diff --git a/dp/wifi3.0/dp_main.c b/dp/wifi3.0/dp_main.c
index 204d683..2c0a43a 100644
--- a/dp/wifi3.0/dp_main.c
+++ b/dp/wifi3.0/dp_main.c
@@ -2,6 +2,8 @@
  * Copyright (c) 2016-2021 The Linux Foundation. All rights reserved.
  * Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
  *
+ * Copyright (c) 2009 Atheros Communications Inc.
+ *
  * Permission to use, copy, modify, and/or distribute this software for
  * any purpose with or without fee is hereby granted, provided that the
  * above copyright notice and this permission notice appear in all
@@ -76,7 +78,7 @@ cdp_dump_flow_pool_info(struct cdp_soc_t *soc)
 #include "dp_mesh_latency.h"
 #endif
 #ifdef ATH_SUPPORT_IQUE
-#include "dp_txrx_me.h"
+//#include "dp_txrx_me.h"
 #endif
 #if defined(DP_CON_MON)
 #ifndef REMOVE_PKT_LOG
@@ -4762,7 +4764,7 @@ static void dp_pdev_deinit(struct cdp_pdev *txrx_pdev, int force)
 	if (pdev->pdev_deinit)
 		return;
 
-	dp_tx_me_exit(pdev);
+	//dp_tx_me_exit(pdev);
 	dp_rx_fst_detach(pdev->soc, pdev);
 	dp_rx_pdev_mon_buffers_free(pdev);
 	dp_rx_pdev_buffers_free(pdev);
@@ -11697,11 +11699,240 @@ static struct cdp_ctrl_ops dp_ops_ctrl = {
 #endif /* WLAN_TX_PKT_CAPTURE_ENH || WLAN_RX_PKT_CAPTURE_ENH */
 };
 
+/**
+ * dp_tx_me_send_convert_ucast(): function to convert multicast to unicast
+ * @soc: Datapath soc handle
+ * @vdev_id: vdev id
+ * @nbuf: Multicast nbuf
+ * @newmac: Table of the clients to which packets have to be sent
+ * @new_mac_cnt: No of clients
+ * @tid: desired tid
+ * @is_igmp: flag to indicate if packet is igmp
+ *
+ * return: no of converted packets
+ */
+uint16_t
+dp_tx_me_send_convert_ucast(struct cdp_soc_t *soc_hdl, uint8_t vdev_id,
+			    qdf_nbuf_t nbuf,
+			    uint8_t newmac[][QDF_MAC_ADDR_SIZE],
+			    uint8_t new_mac_cnt, uint8_t tid,
+			    bool is_igmp)
+{
+	struct dp_soc *soc = cdp_soc_t_to_dp_soc(soc_hdl);
+	struct dp_pdev *pdev;
+	qdf_ether_header_t *eh;
+	uint8_t *data;
+	uint16_t len;
+
+	/* reference to frame dst addr */
+	uint8_t *dstmac;
+	/* copy of original frame src addr */
+	uint8_t srcmac[QDF_MAC_ADDR_SIZE];
+
+	/* local index into newmac */
+	uint8_t new_mac_idx = 0;
+	struct dp_tx_me_buf_t *mc_uc_buf;
+	qdf_nbuf_t  nbuf_clone;
+	struct dp_tx_msdu_info_s msdu_info;
+	struct dp_tx_seg_info_s *seg_info_head = NULL;
+	struct dp_tx_seg_info_s *seg_info_tail = NULL;
+	struct dp_tx_seg_info_s *seg_info_new;
+	qdf_dma_addr_t paddr_data;
+	qdf_dma_addr_t paddr_mcbuf = 0;
+	uint8_t empty_entry_mac[QDF_MAC_ADDR_SIZE] = {0};
+	QDF_STATUS status;
+	uint8_t curr_mac_cnt = 0;
+	struct dp_vdev *vdev = dp_vdev_get_ref_by_id(soc, vdev_id,
+						     DP_MOD_ID_MCAST2UCAST);
+
+	if (!vdev)
+		goto free_return;
+
+	pdev = vdev->pdev;
+
+	if (!pdev)
+		goto free_return;
+
+	qdf_mem_zero(&msdu_info, sizeof(msdu_info));
+
+	dp_tx_get_queue(vdev, nbuf, &msdu_info.tx_queue);
+
+	eh = (qdf_ether_header_t *)qdf_nbuf_data(nbuf);
+	qdf_mem_copy(srcmac, eh->ether_shost, QDF_MAC_ADDR_SIZE);
+
+	len = qdf_nbuf_len(nbuf);
+
+	data = qdf_nbuf_data(nbuf);
+
+	status = qdf_nbuf_map(vdev->osdev, nbuf,
+			QDF_DMA_TO_DEVICE);
+
+	if (status) {
+		QDF_TRACE(QDF_MODULE_ID_DP, QDF_TRACE_LEVEL_ERROR,
+				"Mapping failure Error:%d", status);
+		DP_STATS_INC(vdev, tx_i.mcast_en.dropped_map_error, 1);
+		qdf_nbuf_free(nbuf);
+		return 1;
+	}
+
+	paddr_data = qdf_nbuf_mapped_paddr_get(nbuf) + QDF_MAC_ADDR_SIZE;
+
+	for (new_mac_idx = 0; new_mac_idx < new_mac_cnt; new_mac_idx++) {
+		dstmac = newmac[new_mac_idx];
+		QDF_TRACE(QDF_MODULE_ID_DP, QDF_TRACE_LEVEL_DEBUG,
+				"added mac addr (%pM)", dstmac);
+
+		/* Check for NULL Mac Address */
+		if (!qdf_mem_cmp(dstmac, empty_entry_mac, QDF_MAC_ADDR_SIZE))
+			continue;
+
+		/* frame to self mac. skip */
+		if (!qdf_mem_cmp(dstmac, srcmac, QDF_MAC_ADDR_SIZE))
+			continue;
+
+		/*
+		 * optimize to avoid malloc in per-packet path
+		 * For eg. seg_pool can be made part of vdev structure
+		 */
+		seg_info_new = qdf_mem_malloc(sizeof(*seg_info_new));
+
+		if (!seg_info_new) {
+			QDF_TRACE(QDF_MODULE_ID_DP, QDF_TRACE_LEVEL_ERROR,
+					"alloc failed");
+			DP_STATS_INC(vdev, tx_i.mcast_en.fail_seg_alloc, 1);
+			goto fail_seg_alloc;
+		}
+
+		//mc_uc_buf = dp_tx_me_alloc_buf(pdev);
+		mc_uc_buf = qdf_mem_malloc(sizeof(*mc_uc_buf));
+		if (!mc_uc_buf)
+			goto fail_buf_alloc;
+
+		/*
+		 * Check if we need to clone the nbuf
+		 * Or can we just use the reference for all cases
+		 */
+		//if (new_mac_idx < (new_mac_cnt - 1)) {
+		//	nbuf_clone = qdf_nbuf_clone((qdf_nbuf_t)nbuf);
+		//	if (!nbuf_clone) {
+		//		DP_STATS_INC(vdev, tx_i.mcast_en.clone_fail, 1);
+		//		goto fail_clone;
+		//	}
+		//} else {
+			/*
+			 * Update the ref
+			 * to account for frame sent without cloning
+			 */
+			qdf_nbuf_ref(nbuf);
+			nbuf_clone = nbuf;
+		//}
+
+		qdf_mem_copy(mc_uc_buf->data, dstmac, QDF_MAC_ADDR_SIZE);
+
+		status = qdf_mem_map_nbytes_single(vdev->osdev, mc_uc_buf->data,
+				QDF_DMA_TO_DEVICE, QDF_MAC_ADDR_SIZE,
+				&paddr_mcbuf);
+
+		if (status) {
+			QDF_TRACE(QDF_MODULE_ID_DP, QDF_TRACE_LEVEL_ERROR,
+					"Mapping failure Error:%d", status);
+			DP_STATS_INC(vdev, tx_i.mcast_en.dropped_map_error, 1);
+			mc_uc_buf->paddr_macbuf = 0;
+			goto fail_map;
+		}
+		mc_uc_buf->paddr_macbuf = paddr_mcbuf;
+		seg_info_new->frags[0].vaddr =  (uint8_t *)mc_uc_buf;
+		seg_info_new->frags[0].paddr_lo = (uint32_t) paddr_mcbuf;
+		seg_info_new->frags[0].paddr_hi =
+			(uint16_t)((uint64_t)paddr_mcbuf >> 32);
+		seg_info_new->frags[0].len = QDF_MAC_ADDR_SIZE;
+
+		/*preparing data fragment*/
+		seg_info_new->frags[1].vaddr =
+			qdf_nbuf_data(nbuf) + QDF_MAC_ADDR_SIZE;
+		seg_info_new->frags[1].paddr_lo = (uint32_t)paddr_data;
+		seg_info_new->frags[1].paddr_hi =
+			(uint16_t)(((uint64_t)paddr_data) >> 32);
+		seg_info_new->frags[1].len = len - QDF_MAC_ADDR_SIZE;
+
+		seg_info_new->nbuf = nbuf_clone;
+		seg_info_new->frag_cnt = 2;
+		seg_info_new->total_len = len;
+
+		seg_info_new->next = NULL;
+		curr_mac_cnt++;
+
+		if (!seg_info_head)
+			seg_info_head = seg_info_new;
+		else
+			seg_info_tail->next = seg_info_new;
+
+		seg_info_tail = seg_info_new;
+	}
+
+	if (!seg_info_head) {
+		goto unmap_free_return;
+	}
+
+	msdu_info.u.sg_info.curr_seg = seg_info_head;
+	msdu_info.num_seg = curr_mac_cnt;
+	msdu_info.frm_type = dp_tx_frm_me;
+
+	if (tid == HTT_INVALID_TID) {
+		msdu_info.tid = HTT_INVALID_TID;
+		if (qdf_unlikely(vdev->mcast_enhancement_en > 0) &&
+		    qdf_unlikely(pdev->hmmc_tid_override_en))
+			msdu_info.tid = pdev->hmmc_tid;
+	} else {
+		msdu_info.tid = tid;
+	}
+
+	if (is_igmp) {
+		DP_STATS_INC(vdev, tx_i.igmp_mcast_en.igmp_ucast_converted,
+			     curr_mac_cnt);
+	} else {
+		DP_STATS_INC(vdev, tx_i.mcast_en.ucast, curr_mac_cnt);
+	}
+
+	dp_tx_send_msdu_multiple(vdev, nbuf, &msdu_info);
+
+	while (seg_info_head->next) {
+		seg_info_new = seg_info_head;
+		seg_info_head = seg_info_head->next;
+		qdf_mem_free(seg_info_new);
+	}
+	qdf_mem_free(seg_info_head);
+
+	qdf_nbuf_free(nbuf);
+	dp_vdev_unref_delete(soc, vdev, DP_MOD_ID_MCAST2UCAST);
+	return new_mac_cnt;
+
+fail_map:
+	qdf_nbuf_free(nbuf_clone);
+
+//fail_clone:
+	dp_tx_me_free_buf(pdev, mc_uc_buf);
+
+fail_buf_alloc:
+	qdf_mem_free(seg_info_new);
+
+fail_seg_alloc:
+	//dp_tx_me_mem_free(pdev, seg_info_head);
+
+unmap_free_return:
+	qdf_nbuf_unmap(pdev->soc->osdev, nbuf, QDF_DMA_TO_DEVICE);
+free_return:
+	if (vdev)
+		dp_vdev_unref_delete(soc, vdev, DP_MOD_ID_MCAST2UCAST);
+	qdf_nbuf_free(nbuf);
+	return 1;
+}
+
 static struct cdp_me_ops dp_ops_me = {
 #ifndef QCA_HOST_MODE_WIFI_DISABLED
 #ifdef ATH_SUPPORT_IQUE
-	.tx_me_alloc_descriptor = dp_tx_me_alloc_descriptor,
-	.tx_me_free_descriptor = dp_tx_me_free_descriptor,
+	//.tx_me_alloc_descriptor = dp_tx_me_alloc_descriptor,
+	//.tx_me_free_descriptor = dp_tx_me_free_descriptor,
 	.tx_me_convert_ucast = dp_tx_me_send_convert_ucast,
 #endif
 #endif
diff --git a/dp/wifi3.0/dp_tx.c b/dp/wifi3.0/dp_tx.c
index c229963..b8e1135 100644
--- a/dp/wifi3.0/dp_tx.c
+++ b/dp/wifi3.0/dp_tx.c
@@ -42,12 +42,13 @@
 #include "dp_txrx_wds.h"
 #endif
 #ifdef ATH_SUPPORT_IQUE
-#include "dp_txrx_me.h"
+//#include "dp_txrx_me.h"
 #endif
 #include "dp_hist.h"
 #ifdef WLAN_DP_FEATURE_SW_LATENCY_MGR
 #include <dp_swlm.h>
 #endif
+#include "cdp_txrx_me.h"
 
 /* Flag to skip CCE classify when mesh or tid override enabled */
 #define DP_TX_SKIP_CCE_CLASSIFY \
@@ -2119,7 +2120,7 @@ static inline void dp_tx_comp_free_buf(struct dp_soc *soc,
 		}
 	}
 	/* If it's ME frame, dont unmap the cloned nbuf's */
-	if ((desc->flags & DP_TX_DESC_FLAG_ME) && qdf_nbuf_is_cloned(nbuf))
+	if ((desc->flags & DP_TX_DESC_FLAG_ME) && qdf_nbuf_shared(nbuf))
 		goto nbuf_free;
 
 	qdf_nbuf_unmap_nbytes_single(soc->osdev, nbuf,
@@ -2610,6 +2611,29 @@ static bool dp_check_exc_metadata(struct cdp_tx_exception_metadata *tx_exc)
 }
 
 #ifdef ATH_SUPPORT_IQUE
+#define MAX_PEER_MAC_CNT 10
+static int dp_me_mcast_convert(struct cdp_soc_t *soc,
+			uint8_t vdev_id,
+			uint8_t pdev_id,
+			qdf_nbuf_t wbuf)
+{
+	uint8_t peer_mac[MAX_PEER_MAC_CNT][QDF_MAC_ADDR_SIZE];
+	uint8_t peer_mac_cnt;
+
+	peer_mac_cnt = cdp_vdev_get_peer_mac_list(soc, vdev_id, peer_mac, MAX_PEER_MAC_CNT, false);
+
+	return cdp_tx_me_convert_ucast(soc, vdev_id, wbuf, peer_mac, peer_mac_cnt, HTT_INVALID_TID, false);
+}
+QDF_STATUS
+dp_tx_prepare_send_me(struct dp_vdev *vdev, qdf_nbuf_t nbuf)
+{
+	if (dp_me_mcast_convert((struct cdp_soc_t *)(vdev->pdev->soc),
+				vdev->vdev_id, vdev->pdev->pdev_id,
+				nbuf) > 0)
+		return QDF_STATUS_SUCCESS;
+
+	return QDF_STATUS_E_FAILURE;
+}
 /**
  * dp_tx_mcast_enhance() - Multicast enhancement on TX
  * @vdev: vdev handle
@@ -2639,12 +2663,14 @@ static inline bool dp_tx_mcast_enhance(struct dp_vdev *vdev, qdf_nbuf_t nbuf)
 			return false;
 		}
 
+#if 0
 		if (qdf_unlikely(vdev->igmp_mcast_enhanc_en > 0)) {
 			if (dp_tx_prepare_send_igmp_me(vdev, nbuf) ==
 					QDF_STATUS_SUCCESS) {
 				return false;
 			}
 		}
+#endif
 	}
 
 	return true;
diff --git a/dp/wifi3.0/dp_tx_desc.h b/dp/wifi3.0/dp_tx_desc.h
index cdc1f4c..78dd962 100644
--- a/dp/wifi3.0/dp_tx_desc.h
+++ b/dp/wifi3.0/dp_tx_desc.h
@@ -1013,9 +1013,12 @@ dp_tx_me_free_buf(struct dp_pdev *pdev, struct dp_tx_me_buf_t *buf)
 		buf->paddr_macbuf = 0;
 	}
 	qdf_spin_lock_bh(&pdev->tx_mutex);
+#if 0
 	buf->next = pdev->me_buf.freelist;
 	pdev->me_buf.freelist = buf;
 	pdev->me_buf.buf_in_use--;
+#endif
+	qdf_mem_free(buf);
 	qdf_spin_unlock_bh(&pdev->tx_mutex);
 }
 #endif /* DP_TX_DESC_H */
-- 
2.7.4

