From 9c3428c01aeccb76707a466d4b22bf790e53736e Mon Sep 17 00:00:00 2001
From: Zijun Hu <zijuhu@codeaurora.org>
Date: Wed, 13 Mar 2019 20:51:06 +0800
Subject: [PATCH 3/6] bt_usb_driver: Fix read ACL/EVENT error

Fix read ACL/EVENT error.

Change-Id: Ida2f392c83f24e7f00de785e3207461c09ace852
Signed-off-by: Zijun Hu <zijuhu@codeaurora.org>
---
 bt_usb_main.c           | 58 ++++++++++++++++++++++++++++++++++++++++++-------
 include/linux_usb_com.h |  2 ++
 2 files changed, 52 insertions(+), 8 deletions(-)

diff --git a/bt_usb_main.c b/bt_usb_main.c
index 3021386..31c6264 100644
--- a/bt_usb_main.c
+++ b/bt_usb_main.c
@@ -112,7 +112,9 @@ typedef struct
     uint8_t minor;
     struct cdev cdev;
     bool in_use;
-    
+    uint8_t *c_buff;
+    int c_idx;
+    int c_count;
 } bt_usb_instance_t;
 
 
@@ -287,6 +289,7 @@ static ssize_t bt_usb_read(struct file *file, char __user *buf, size_t size, lof
     int ret = -1;
     unsigned char val = 0;
     read_q_elm_t *ptr = NULL;
+    int t;
 
     inst = (bt_usb_instance_t *)file->private_data;
     if (inst == NULL){
@@ -304,6 +307,19 @@ static ssize_t bt_usb_read(struct file *file, char __user *buf, size_t size, lof
         return(-ERESTARTSYS);
     }
 
+     t = inst->c_count - inst->c_idx;
+     if (t > 0) {
+          n = min(t, size);
+          ret = copy_to_user(buf, inst->c_buff + inst->c_idx, n);
+          if (0 == ret) {
+               inst->c_idx += n;
+          } else {
+               n = -1;
+               pr_err("%s: %d copy_to_user() error\n", __func__, __LINE__);
+          }
+          goto out;
+     }
+
     while (inst->first == NULL)
     {
 #ifdef BT_USB_DEBUG
@@ -323,7 +339,7 @@ static ssize_t bt_usb_read(struct file *file, char __user *buf, size_t size, lof
         }
     }
 
-    if (size > 1)
+    if (size >= 1)
     {
         unsigned char channel;
         ptr = inst->first;
@@ -356,12 +372,23 @@ static ssize_t bt_usb_read(struct file *file, char __user *buf, size_t size, lof
                 return -ENODEV;
         }
         ret = copy_to_user(buf, &channel, 1);
-        if (ret == 0)
-        {
-            ret = copy_to_user(buf + 1, ptr->msg->buf,
-                min(size -1, ptr->msg->buflen));
+        if (0 == ret) {
+             t = min(size - 1, ptr->msg->buflen);
+             if (t > 0)
+                  ret = copy_to_user(buf + 1, ptr->msg->buf, t);
+             if (0 == ret) {
+                  n = t + 1;
+                  if (t < ptr->msg->buflen) {
+                       inst->c_idx = 0;
+                       inst->c_count = ptr->msg->buflen - t;
+                       memcpy(inst->c_buff, ptr->msg->buf + t, inst->c_count);
+                  }
+             } else {
+                  pr_err("%s: %d copy_to_user() error\n", __func__, __LINE__);
+             }
+        } else {
+             pr_err("%s: %d copy_to_user() error\n", __func__, __LINE__);
         }
-        n = ret == 0 ? min(size - 1, ptr->msg->buflen) + 1 : -1;
 
         pfree(ptr->msg->buf);
         pfree(ptr->msg);
@@ -369,6 +396,7 @@ static ssize_t bt_usb_read(struct file *file, char __user *buf, size_t size, lof
         ptr = NULL;
     }
 
+out:
     up(&inst->access_sem);
 
 #ifdef BT_USB_DEBUG
@@ -486,7 +514,7 @@ static ssize_t bt_usb_write(struct file *file, const char __user *buf, size_t co
     }
     up(&inst_sem);
 
-    if (count > 1)
+    if (count >= 1)
     {
         unsigned char value;
         ret = copy_from_user(&value, buf, 1);
@@ -593,6 +621,13 @@ static int bt_usb_open(struct inode *inode, struct file *filp)
     init_waitqueue_head(&inst->read_q);
     sema_init(&inst->access_sem, 1);
 
+    inst->c_buff = kmalloc(C_BUFF_SIZE, GFP_KERNEL);
+    if (NULL == inst->c_buff)
+         return -ENOMEM;
+    inst->c_idx = 0;
+    inst->c_count = 0;
+    pr_info("%s: kmalloc(%d) okay\n", __func__, C_BUFF_SIZE);
+
     filp->private_data = inst;
      printk("bt_usb%u: open complete\n",minor);
 #ifdef CONFIG_USB_SUSPEND
@@ -635,6 +670,13 @@ static int bt_usb_release(struct inode *inode, struct file *filp)
 
         ptr = next;
     }
+
+    if (inst->c_buff) {
+         kfree(inst->c_buff);
+         inst->c_buff = NULL;
+         pr_info("%s: kfree(inst->c_buff) Done\n");
+    }
+
     printk("wake up interrupt on release %p %p\n",&inst, &(inst->read_q));
     inst->first = NULL;
     inst->last = NULL;
diff --git a/include/linux_usb_com.h b/include/linux_usb_com.h
index 4efec19..caced7c 100644
--- a/include/linux_usb_com.h
+++ b/include/linux_usb_com.h
@@ -62,6 +62,8 @@ extern "C" {
 #define MAX_HCI_EVENT_SIZE      (HCI_EVENT_HDR_SIZE + HCI_EVENT_DATA_SIZE)
 #define MAX_HCI_ACL_SIZE        (HCI_ACL_HDR_SIZE + HCI_ACL_DATA_SIZE)
 
+#define C_BUFF_SIZE (MAX_HCI_ACL_SIZE + 1)
+
 /* Macros to avoid too much versioning inside code */
 #define URB_ALLOC(no, mf) usb_alloc_urb((no), (mf))
 #define URB_SUBMIT(urb, mf) usb_submit_urb((urb), (mf))
-- 
2.7.4

