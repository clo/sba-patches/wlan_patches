From 4479898c5abad963fdf45db7644e45c52446e166 Mon Sep 17 00:00:00 2001
From: Zijun Hu <quic_zijuhu@quicinc.com>
Date: Tue, 4 Mar 2025 11:18:52 +0800
Subject: [PATCH] bt_usb_driver: Support F/W memdump collection

Support FW memdump collection.

Signed-off-by: Zijun Hu <quic_zijuhu@quicinc.com>
---
 usb_intf.c | 293 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 1 file changed, 293 insertions(+)

diff --git a/usb_intf.c b/usb_intf.c
index 0bb2f50febf3..3f61d3ee4031 100644
--- a/usb_intf.c
+++ b/usb_intf.c
@@ -39,6 +39,7 @@
 #include <linux/wait.h>
 #include <linux/usb.h>
 #include <linux/kthread.h>
+#include <linux/vmalloc.h>
 #include <asm/bitops.h>
 
 #include "include/linux_usb_com.h"
@@ -480,12 +481,304 @@ static inline int hevt_recv_match(int rx_ind, uint8_t *rx_data, int rx_size,
 	}
 	return 1;
 }
+
+struct acl_hdr_desc {
+	uint16_t handle;
+	uint16_t alen;
+} __packed;
+
+struct memdump_pkg_desc {
+	uint8_t evt;
+	uint8_t elen;
+
+	uint8_t ve_class;
+	uint8_t msg_type;
+	uint16_t seq_no;
+	uint8_t ss_id;
+
+	union {
+		struct {
+			uint32_t ram_dump_size;
+			uint8_t  first_data[0];
+
+		} __packed;
+		uint8_t  data[0];
+	} __packed;
+}__packed;
+
+struct md_desc {
+	uint32_t ram_dump_size;
+	uint16_t ram_dump_seqno;
+
+	uint8_t *dump_buff;
+	uint32_t dump_count;
+
+	int state;
+};
+
+struct fw_dump_grab {
+	struct work_struct grab_work;
+	void *ptr;
+	int size;
+	char name[96];
+};
+
+static void btfw_grab_work(struct work_struct *work)
+{
+	struct fw_dump_grab *grab = container_of(work, struct fw_dump_grab, grab_work);
+	struct file *f;
+	loff_t pos = 0;
+	ssize_t res;
+
+	pr_info("Enter %s: ptr=%p, size=%d, name='%s'\n", __func__, grab->ptr, grab->size, grab->name);
+	if(!grab->ptr || !grab->size) {
+		pr_err("%s: Invalid parameters\n", __func__);
+		return;
+	}
+
+	f = filp_open(grab->name, O_RDWR|O_CREAT|O_TRUNC, S_IRWXU|S_IRGRP|S_IROTH);
+	if (IS_ERR(f)) {
+		pr_err("%s: filp_open(%s, ...) error %ld\n", __func__, grab->name, PTR_ERR(f));
+		return;
+	}
+
+	res = kernel_write(f, grab->ptr, grab->size, &pos);
+	if (res < 0)
+		pr_err("%s: kernel_write() failed %ld\n", __func__, res);
+	else
+		pr_info("%s: Store %ld bytes into %s\n", __func__, res, grab->name);
+
+	filp_close(f, NULL);
+
+	vfree(grab->ptr);
+	grab->ptr=NULL;
+	grab->size = 0;
+	memset(grab->name, 0x00, sizeof(grab->name));
+}
+
+unsigned int btfw_dump_seq;
+static struct md_desc md_instsb[1];
+struct fw_dump_grab fw_grab = {
+	.grab_work = __WORK_INITIALIZER(fw_grab.grab_work, btfw_grab_work),
+};
+
+static int memdump_init(int inst, uint32_t ram_dump_size)
+{
+	struct md_desc *md_ptr = &md_instsb[0];
+	int ret = 0;
+
+	md_ptr->ram_dump_size = ram_dump_size;
+	md_ptr->state = 1;
+	md_ptr->ram_dump_seqno = 0;
+	md_ptr->dump_count = 0;
+
+	if (!md_ptr->dump_buff)
+		md_ptr->dump_buff = vmalloc(0x200000);
+	if (md_ptr->dump_buff)
+		goto out;
+
+	ret = -1;
+	md_ptr->ram_dump_size = 0;
+	md_ptr->state = 0;
+
+out:
+	pr_info("%s: %d ram_dump_size(%d) %s\n", __func__, inst, ram_dump_size, ret ? "failed" : "okay");
+	return ret;
+}
+
+static int memdump_write(int inst, uint16_t seq_no, unsigned char *buff, unsigned int size)
+{
+	struct md_desc *md_ptr = &md_instsb[0];
+
+	if (!size) {
+		pr_err("%s: %d None date to write\n", __func__, inst);
+		return 0;
+	}
+
+	if (md_ptr->state < 0)
+		return -1;
+
+	if (md_ptr->state != 1) {
+		pr_err("%s: %d MEMDUMP at Invalid state(%d)\n", __func__, inst, md_ptr->state);
+		md_ptr->state = -1;
+		return -1;
+	}
+
+	if (seq_no != 0xffff && seq_no != md_ptr->ram_dump_seqno) {
+		md_ptr->state = -2;
+		pr_err("%s: %d INvalid seq_no(%d) VS expected(%d)\n", __func__, inst, seq_no, md_ptr->ram_dump_seqno);
+		return -1;
+	}
+
+	memcpy(&md_ptr->dump_buff[md_ptr->dump_count], buff, size);
+	md_ptr->dump_count += size;
+
+	if (seq_no != 0xffff)
+		++md_ptr->ram_dump_seqno;
+	else
+		md_ptr->state = 2;
+
+	return 0;
+}
+
+static void memdump_exit(int inst)
+{
+	struct md_desc *md_ptr = &md_instsb[0];
+	int is_okay = 0;
+
+	if (md_ptr->state == 0 || md_ptr->state == 1)
+		return;
+
+	if ((md_ptr->ram_dump_size > 0) && (md_ptr->dump_count > 0) && (md_ptr->state == 2)) {
+		snprintf(fw_grab.name, sizeof(fw_grab.name),
+			 "/data/misc/bluetooth/btusb_%s_%d_fwdump.bin", inst & 0x02 ? "acl" : "evt",
+			 btfw_dump_seq++);
+		fw_grab.ptr = md_ptr->dump_buff;
+		fw_grab.size = md_ptr->dump_count;
+		pr_info("%s: %d Memdump %d/%d bytes\n", __func__, inst, md_ptr->dump_count, md_ptr->ram_dump_size);
+		is_okay = 1;
+		schedule_work(&fw_grab.grab_work);
+	}
+	pr_info("%s : md_ptr->state(%d)\n", __func__, md_ptr->state);
+
+	md_ptr->ram_dump_size = 0;
+	md_ptr->ram_dump_seqno = 0;
+
+	if (!is_okay)
+		vfree(md_ptr->dump_buff);
+	md_ptr->dump_buff = NULL;
+	md_ptr->dump_count = 0;
+
+	md_ptr->state = 0;
+}
+
+/* 0: not a memdump pkg
+ * 1: a valid memdump pkg
+ * <0: a error memdump pkg
+ */
+static int btfw_dump_hdl_pkt(int ind, unsigned char *buff, unsigned int size)
+{
+	struct memdump_pkg_desc *md_pkg_ptr = NULL;
+	struct acl_hdr_desc *acl_hdr_ptr = NULL;
+	struct md_desc *inst_ptr = NULL;
+	uint8_t *p = buff;
+	unsigned int s = size;
+	unsigned int d_s = 0;
+	uint8_t *d_p;
+	uint16_t handle;
+	uint16_t alen;
+	uint16_t elen;
+	uint16_t seq_no;
+	uint32_t ram_dump_size;
+	int res;
+	int ret = 0;
+
+	int md_inst = -1;
+	uint32_t md_size_max = 0x180000;
+
+	switch (ind) {
+	case 0x04:
+		p += 1;
+		s -= 1;
+		break;
+	case 0x02:
+		p += 1;
+		s -= 1;
+
+		acl_hdr_ptr = (struct acl_hdr_desc *)p;
+		handle = le16_to_cpu(acl_hdr_ptr->handle) & 0xfff;
+		if (handle != 0xedd)
+			return 0;
+		alen = le16_to_cpu(acl_hdr_ptr->alen);
+		if (alen + sizeof(*acl_hdr_ptr) != s) {
+			pr_err("%s: Invalid BT ACL Memdump frame alen(%d), s(%d)\n", __func__, alen, s);
+			return -1;
+		}
+		p += sizeof(*acl_hdr_ptr);
+		s -= sizeof(*acl_hdr_ptr);
+		md_inst = 3;
+		break;
+	default:
+		return 0;
+	}
+
+	if (s < offsetof(struct memdump_pkg_desc, data)) {
+		ret = -2;
+		goto out_error;
+	}
+	md_pkg_ptr = (struct memdump_pkg_desc *)p;
+
+	if (md_pkg_ptr->evt != 0xff) {
+		ret = -3;
+		goto out_error;
+	}
+	elen = md_pkg_ptr->elen;
+	if ((uint16_t)elen + offsetof(struct memdump_pkg_desc, ve_class) != s) {
+		ret = -4;
+		goto out_error;
+	}
+
+	if (md_pkg_ptr->ve_class == 0x01 && md_pkg_ptr->msg_type == 0x08) {
+		if (md_inst < 0) {
+			md_inst = 0x01;
+		} else if (!(md_inst & 0x01)){
+			ret = -6;
+			goto out_error;
+		}
+	} else {
+		ret = -7;
+		goto out_error;
+	}
+
+	if (md_inst & 0x01)
+		md_size_max = 0x200000;
+
+	seq_no = le16_to_cpu(md_pkg_ptr->seq_no);
+	inst_ptr = &md_instsb[0];
+
+	if(seq_no == 0) {
+		if (s < offsetof(struct memdump_pkg_desc, first_data)) {
+			ret = -8;
+			goto out_error;
+		}
+
+		ram_dump_size = le32_to_cpu(md_pkg_ptr->ram_dump_size);
+		if (ram_dump_size == 0 || ram_dump_size > md_size_max) {
+			pr_err("%s: invalid ram_dump_size/md_size_max: (%u/%u)\n", __func__, ram_dump_size, md_size_max);
+			ret = -9;
+			goto out_error;
+		}
+
+		memdump_init(md_inst, ram_dump_size);
+		d_s = s - offsetof(struct memdump_pkg_desc, first_data);
+		d_p = md_pkg_ptr->first_data;
+	}else {
+		d_s = s - offsetof(struct memdump_pkg_desc, data);
+		d_p = md_pkg_ptr->data;
+	}
+
+	res = memdump_write(md_inst, seq_no, d_p, d_s);
+	ret = res ? -10 : 1;
+
+out_error:
+	if (md_inst < 0)
+		return 0;
+	memdump_exit(md_inst);
+	return ret;
+}
+
+
 static int hrx_filter(csr_dev_t *dv, struct usb_qe *qe)
 {
 	int res;
 	uint8_t ind, *rx_data = qe->msg->buf;
 	int cpy_len, rx_len = qe->msg->buflen;
 	if (dv->probe_done) {
+
+		if (qe->chan == BCSP_CHANNEL_HCI || qe->chan == BCSP_CHANNEL_ACL) {
+			btfw_dump_hdl_pkt(qe->chan == BCSP_CHANNEL_HCI ? 0x04 : 0x02, rx_data - 1, rx_len + 1);
+		}
+
 		if (qe->chan == BCSP_CHANNEL_PERI_HCI ||
 		    qe->chan == BCSP_CHANNEL_PERI_ACL)
 			goto qe_free;
-- 
2.7.4

