From d1527dcc3fb1ba5e9ff1a2c645405c76f8f7c092 Mon Sep 17 00:00:00 2001
From: Zijun Hu <quic_zijuhu@quicinc.com>
Date: Wed, 31 Jan 2024 15:17:16 +0800
Subject: [PATCH 45/46] SDP: pass the bounds to process _service_*_rsp

pass the bounds to process _service_*_rsp.
---
 stack/sdp/sdp_discovery.c | 29 ++++++++++++++++++-----------
 1 file changed, 18 insertions(+), 11 deletions(-)

diff --git a/stack/sdp/sdp_discovery.c b/stack/sdp/sdp_discovery.c
index 9f96a172467f..9e3e73fb8909 100644
--- a/stack/sdp/sdp_discovery.c
+++ b/stack/sdp/sdp_discovery.c
@@ -45,9 +45,12 @@
 /*              L O C A L    F U N C T I O N     P R O T O T Y P E S            */
 /********************************************************************************/
 #if SDP_CLIENT_ENABLED == TRUE
-static void          process_service_search_rsp (tCONN_CB *p_ccb, UINT8 *p_reply);
-static void          process_service_attr_rsp (tCONN_CB *p_ccb, UINT8 *p_reply);
-static void          process_service_search_attr_rsp (tCONN_CB *p_ccb, UINT8 *p_reply);
+static void          process_service_search_rsp (tCONN_CB *p_ccb, UINT8 *p_reply,
+                                                  UINT8 *p_reply_end);
+static void          process_service_attr_rsp (tCONN_CB *p_ccb, UINT8 *p_reply,
+                                                  UINT8 *p_reply_end);
+static void          process_service_search_attr_rsp (tCONN_CB *p_ccb, UINT8 *p_reply,
+                                                  UINT8 *p_reply_end);
 static UINT8         *save_attr_seq (tCONN_CB *p_ccb, UINT8 *p, UINT8 *p_msg_end);
 static tSDP_DISC_REC *add_record (tSDP_DISCOVERY_DB *p_db, BD_ADDR p_bda);
 static UINT8         *add_attr (UINT8 *p, tSDP_DISCOVERY_DB *p_db, tSDP_DISC_REC *p_rec,
@@ -200,7 +203,7 @@ void sdp_disc_connected (tCONN_CB *p_ccb)
     {
         p_ccb->disc_state = SDP_DISC_WAIT_SEARCH_ATTR;
 
-        process_service_search_attr_rsp (p_ccb, NULL);
+        process_service_search_attr_rsp (p_ccb, NULL, NULL);
     }
     else
     {
@@ -238,6 +241,7 @@ void sdp_disc_server_rsp (tCONN_CB *p_ccb, BT_HDR *p_msg)
 
     /* Got a reply!! Check what we got back */
     p = (UINT8 *)(p_msg + 1) + p_msg->offset;
+    UINT8 *p_end = p + p_msg->len;
 
     BE_STREAM_TO_UINT8 (rsp_pdu, p);
 
@@ -248,7 +252,7 @@ void sdp_disc_server_rsp (tCONN_CB *p_ccb, BT_HDR *p_msg)
     case SDP_PDU_SERVICE_SEARCH_RSP:
         if (p_ccb->disc_state == SDP_DISC_WAIT_HANDLES)
         {
-            process_service_search_rsp (p_ccb, p);
+            process_service_search_rsp (p_ccb, p, p_end);
             invalid_pdu = FALSE;
         }
         break;
@@ -256,7 +260,7 @@ void sdp_disc_server_rsp (tCONN_CB *p_ccb, BT_HDR *p_msg)
     case SDP_PDU_SERVICE_ATTR_RSP:
         if (p_ccb->disc_state == SDP_DISC_WAIT_ATTR)
         {
-            process_service_attr_rsp (p_ccb, p);
+            process_service_attr_rsp (p_ccb, p, p_end);
             invalid_pdu = FALSE;
         }
         break;
@@ -264,7 +268,7 @@ void sdp_disc_server_rsp (tCONN_CB *p_ccb, BT_HDR *p_msg)
     case SDP_PDU_SERVICE_SEARCH_ATTR_RSP:
         if (p_ccb->disc_state == SDP_DISC_WAIT_SEARCH_ATTR)
         {
-            process_service_search_attr_rsp (p_ccb, p);
+            process_service_search_attr_rsp (p_ccb, p, p_end);
             invalid_pdu = FALSE;
         }
         break;
@@ -287,7 +291,8 @@ void sdp_disc_server_rsp (tCONN_CB *p_ccb, BT_HDR *p_msg)
 ** Returns          void
 **
 *******************************************************************************/
-static void process_service_search_rsp (tCONN_CB *p_ccb, UINT8 *p_reply)
+static void process_service_search_rsp (tCONN_CB *p_ccb, UINT8 *p_reply,
+                                        UINT8 *p_reply_end)
 {
     UINT16      xx;
     UINT16      total, cur_handles, orig;
@@ -333,7 +338,7 @@ static void process_service_search_rsp (tCONN_CB *p_ccb, UINT8 *p_reply)
         p_ccb->disc_state = SDP_DISC_WAIT_ATTR;
 
         /* Kick off the first attribute request */
-        process_service_attr_rsp (p_ccb, NULL);
+        process_service_attr_rsp (p_ccb, NULL, NULL);
     }
 }
 
@@ -408,7 +413,8 @@ static void sdp_copy_raw_data (tCONN_CB *p_ccb, BOOLEAN offset)
 ** Returns          void
 **
 *******************************************************************************/
-static void process_service_attr_rsp (tCONN_CB *p_ccb, UINT8 *p_reply)
+static void process_service_attr_rsp (tCONN_CB *p_ccb, UINT8 *p_reply,
+                                      UINT8 *p_reply_end)
 {
     UINT8           *p_start, *p_param_len;
     UINT16          param_len, list_byte_count;
@@ -554,7 +560,8 @@ static void process_service_attr_rsp (tCONN_CB *p_ccb, UINT8 *p_reply)
 ** Returns          void
 **
 *******************************************************************************/
-static void process_service_search_attr_rsp (tCONN_CB *p_ccb, UINT8 *p_reply)
+static void process_service_search_attr_rsp (tCONN_CB *p_ccb, UINT8 *p_reply,
+                                             UINT8 *p_reply_end)
 {
     UINT8           *p, *p_start, *p_end, *p_param_len;
     UINT8           type;
-- 
2.7.4

