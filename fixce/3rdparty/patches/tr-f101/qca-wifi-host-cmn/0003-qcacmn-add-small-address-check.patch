From c7d8ae073532e7fa6a8b58562a3b804a9b7a2776 Mon Sep 17 00:00:00 2001
From: Shi Hong <quic_hongsh@quicinc.com>
Date: Tue, 16 Apr 2024 14:15:30 +0800
Subject: [PATCH] qcacmn: add small address check

HW doesn't not allow dma address<0x2000. Add physical address check for
skb and dma memory.

Signed-off-by: Hong Shi <quic_hongsh@quicinc.com>
---
 qdf/inc/qdf_nbuf.h       |  4 +++
 qdf/linux/src/qdf_mem.c  | 64 +++++++++++++++++++++++++++++++++++++++-
 qdf/linux/src/qdf_nbuf.c | 37 +++++++++++++++++++++++
 3 files changed, 104 insertions(+), 1 deletion(-)

diff --git a/qdf/inc/qdf_nbuf.h b/qdf/inc/qdf_nbuf.h
index e12f6ba0d..534d5b19f 100644
--- a/qdf/inc/qdf_nbuf.h
+++ b/qdf/inc/qdf_nbuf.h
@@ -1163,6 +1163,10 @@ qdf_nbuf_set_send_complete_flag(qdf_nbuf_t buf, bool flag)
 	__qdf_nbuf_set_send_complete_flag(buf, flag);
 }
 
+void qdf_nbuf_invalid_nbuf_queue_init(void);
+void qdf_nbuf_invalid_nbuf_queue_deinit(void);
+void qdf_nbuf_invalid_nbuf_queue_add(qdf_nbuf_t buf);
+
 #define QDF_NBUF_QUEUE_WALK_SAFE(queue, var, tvar)	\
 		__qdf_nbuf_queue_walk_safe(queue, var, tvar)
 
diff --git a/qdf/linux/src/qdf_mem.c b/qdf/linux/src/qdf_mem.c
index bd091ee5d..795413da5 100644
--- a/qdf/linux/src/qdf_mem.c
+++ b/qdf/linux/src/qdf_mem.c
@@ -122,6 +122,37 @@ static struct __qdf_mem_stat {
 	int32_t tx_descs_max;
 } qdf_mem_stat;
 
+struct qdf_mem_invalid_dma_buf {
+	void *dev;
+	qdf_size_t size;
+	void *vaddr;
+	qdf_dma_addr_t paddr;
+};
+
+static struct qdf_mem_invalid_dma_buf qdf_mem_invalid_dma_buf_list[1000];
+static uint32_t qdf_mem_invalid_dma_buf_list_index=0;
+static qdf_spinlock_t qdf_mem_invalid_dma_buf_list_lock;
+
+static void qdf_mem_invalid_dma_init(void) {
+       qdf_spinlock_create(&qdf_mem_invalid_dma_buf_list_lock);
+}
+
+static void qdf_mem_invalid_dma_exit(void) {
+       int i = 0;
+
+       for (i = 0; i < qdf_mem_invalid_dma_buf_list_index; i++) {
+               if (qdf_mem_invalid_dma_buf_list[i].size > 0) {
+                       qdf_err("free invalid dma buf: 0x0x%llx",
+                               (uint64_t)(qdf_mem_invalid_dma_buf_list[i].paddr));
+                       dma_free_coherent(qdf_mem_invalid_dma_buf_list[i].dev,
+                                        qdf_mem_invalid_dma_buf_list[i].size,
+                                        qdf_mem_invalid_dma_buf_list[i].vaddr,
+                                        qdf_mem_invalid_dma_buf_list[i].paddr);
+               }
+       }
+       qdf_spinlock_destroy(&qdf_mem_invalid_dma_buf_list_lock);
+}
+
 #ifdef MEMORY_DEBUG
 #include "qdf_debug_domain.h"
 
@@ -2659,7 +2690,36 @@ void *qdf_mem_dma_alloc(qdf_device_t osdev, void *dev, qdf_size_t size,
 static inline void *qdf_mem_dma_alloc(qdf_device_t osdev, void *dev,
 				      qdf_size_t size, qdf_dma_addr_t *paddr)
 {
-	return dma_alloc_coherent(dev, size, paddr, qdf_mem_malloc_flags());
+       uint32_t lowmem_alloc_tries = 0;
+       void *vaddr = NULL;
+
+realloc:
+       vaddr = dma_alloc_coherent(dev, size, paddr, qdf_mem_malloc_flags());
+
+       if ((uint64_t)(*paddr) < 0x2000) {
+               lowmem_alloc_tries++;
+               qdf_err("invalid phy addr 0x%llx, trying again", (uint64_t)(*paddr));
+               qdf_spin_lock_irqsave(&qdf_mem_invalid_dma_buf_list_lock);
+               if (qdf_mem_invalid_dma_buf_list_index >= 1000) {
+                       qdf_spin_unlock_irqrestore(&qdf_mem_invalid_dma_buf_list_lock);
+                       qdf_err("exceed maximum size of invalid dma buf list");
+                       dma_free_coherent(dev, size, vaddr, *paddr);
+                       return NULL;
+               }
+               qdf_mem_invalid_dma_buf_list[qdf_mem_invalid_dma_buf_list_index].dev = dev;
+               qdf_mem_invalid_dma_buf_list[qdf_mem_invalid_dma_buf_list_index].vaddr = vaddr;
+               qdf_mem_invalid_dma_buf_list[qdf_mem_invalid_dma_buf_list_index].paddr = *paddr;
+               qdf_mem_invalid_dma_buf_list[qdf_mem_invalid_dma_buf_list_index++].size = size;
+               qdf_spin_unlock_irqrestore(&qdf_mem_invalid_dma_buf_list_lock);
+               if (lowmem_alloc_tries > 100) {
+                       qdf_err("invalid phy addr, MAX trying");
+                       return NULL;
+               } else {
+                       goto realloc;
+               }
+       }
+
+       return vaddr;
 }
 #endif
 
@@ -2906,6 +2966,7 @@ qdf_export_symbol(qdf_mem_dma_sync_single_for_cpu);
 
 void qdf_mem_init(void)
 {
+	qdf_mem_invalid_dma_init();
 	qdf_mem_debug_init();
 	qdf_net_buf_debug_init();
 	qdf_frag_debug_init();
@@ -2921,6 +2982,7 @@ void qdf_mem_exit(void)
 	qdf_frag_debug_exit();
 	qdf_net_buf_debug_exit();
 	qdf_mem_debug_exit();
+	qdf_mem_invalid_dma_exit();
 }
 qdf_export_symbol(qdf_mem_exit);
 
diff --git a/qdf/linux/src/qdf_nbuf.c b/qdf/linux/src/qdf_nbuf.c
index 04b4bdcad..be73ccbde 100644
--- a/qdf/linux/src/qdf_nbuf.c
+++ b/qdf/linux/src/qdf_nbuf.c
@@ -111,6 +111,7 @@ struct qdf_track_timer {
 };
 
 static struct qdf_track_timer alloc_track_timer;
+static qdf_nbuf_queue_t invalid_nbuf_queue;
 
 #define QDF_NBUF_ALLOC_EXPIRE_TIMER_MS  5000
 #define QDF_NBUF_ALLOC_EXPIRE_CNT_THRESHOLD  50
@@ -296,6 +297,27 @@ void qdf_nbuf_set_state(qdf_nbuf_t nbuf, uint8_t current_state)
 }
 qdf_export_symbol(qdf_nbuf_set_state);
 
+void qdf_nbuf_invalid_nbuf_queue_init(void)
+{
+       qdf_nbuf_queue_init(&invalid_nbuf_queue);
+}
+
+void qdf_nbuf_invalid_nbuf_queue_deinit(void)
+{
+       qdf_nbuf_t buf = NULL;
+
+       qdf_err("invalid nbuf to free:%u", qdf_nbuf_queue_len(&invalid_nbuf_queue));
+       while ((buf = qdf_nbuf_queue_remove(&invalid_nbuf_queue)) != NULL) {
+               if (qdf_likely(buf))
+                       dev_kfree_skb_any(buf);
+       }
+}
+
+void qdf_nbuf_invalid_nbuf_queue_add(struct sk_buff *skb)
+{
+       qdf_nbuf_queue_add(&invalid_nbuf_queue, skb);
+}
+
 #ifdef FEATURE_NBUFF_REPLENISH_TIMER
 /**
  * __qdf_nbuf_start_replenish_timer - Start alloc fail replenish timer
@@ -620,6 +642,7 @@ struct sk_buff *__qdf_nbuf_alloc(qdf_device_t osdev, size_t size, int reserve,
 {
 	struct sk_buff *skb;
 	int flags = GFP_KERNEL;
+	uint32_t lowmem_alloc_tries = 0;
 
 	if (align)
 		size += (align - 1);
@@ -637,6 +660,7 @@ struct sk_buff *__qdf_nbuf_alloc(qdf_device_t osdev, size_t size, int reserve,
 #endif
 	}
 
+realloc:
 	skb = __netdev_alloc_skb(NULL, size, flags);
 
 	if (skb)
@@ -654,6 +678,19 @@ struct sk_buff *__qdf_nbuf_alloc(qdf_device_t osdev, size_t size, int reserve,
 	}
 
 skb_alloc:
+        if (virt_to_phys(qdf_nbuf_data(skb)) < 0x2000) {
+                lowmem_alloc_tries++;
+               qdf_nofl_err("small address allocated, realloc");
+               qdf_nbuf_invalid_nbuf_queue_add(skb);
+                if (lowmem_alloc_tries > 100) {
+                        qdf_nofl_err("NBUF alloc failed %zuB @ %s:%d",
+                                     size, func, line);
+                        return NULL;
+                } else {
+                        goto realloc;
+                }
+        }
+
 	qdf_nbuf_set_defaults(skb, align, reserve);
 
 	return skb;
-- 
2.25.1

