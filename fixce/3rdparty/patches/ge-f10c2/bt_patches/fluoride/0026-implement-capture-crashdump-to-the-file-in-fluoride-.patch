From 60a4f0c97d6a5b386626ea877caa8c781a6783bc Mon Sep 17 00:00:00 2001
From: Tim Jiang <tjiang@codeaurora.org>
Date: Wed, 26 Feb 2020 17:43:34 +0800
Subject: [PATCH 1/2] implement capture crashdump to the file in fluoride
 stack.

Change-Id: I0e5af83267310491ddf661228cea54422c860503
Signed-off-by: Tim Jiang <tjiang@codeaurora.org>
---
 hci/src/hci_layer.cc    |   2 +
 stack/btm/btm_devctl.cc | 169 ++++++++++++++++++++++++++++++++++++++++++++++++
 2 files changed, 171 insertions(+)

diff --git a/hci/src/hci_layer.cc b/hci/src/hci_layer.cc
index 9b0a463..ae2b305 100644
--- a/hci/src/hci_layer.cc
+++ b/hci/src/hci_layer.cc
@@ -591,6 +591,8 @@ int commands_pending_response_check(waiting_command_t *wait_entry)
   opcode = (uint16_t)(*stream) + (((uint16_t)(*(stream + 1))) << 8);
   len = stream[2];
 /* Check if command need to wait response */
+  if(opcode == 0xfc0c) //crash dump command
+      ret = 0;
   return ret;
 }
 
diff --git a/stack/btm/btm_devctl.cc b/stack/btm/btm_devctl.cc
index 31a955f..0fe743b 100644
--- a/stack/btm/btm_devctl.cc
+++ b/stack/btm/btm_devctl.cc
@@ -1,4 +1,6 @@
 /******************************************************************************
+ *  Copyright (c) 2020, The Linux Foundation. All rights reserved.
+ *  Not a contribution.
  *
  *  Copyright (C) 1999-2012 Broadcom Corporation
  *
@@ -28,6 +30,11 @@
 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
+#ifdef LG_WEBOS
+#include <sys/stat.h>
+#include <fcntl.h>
+#include <errno.h>
+#endif
 
 #include "bt_types.h"
 #include "bt_utils.h"
@@ -60,6 +67,16 @@ extern thread_t* bt_workqueue_thread;
                 */
 
 #define BTM_INFO_TIMEOUT 5 /* 5 seconds for info response */
+#ifdef LG_WEBOS
+/* Macros used in  Crash dump handler */
+#define BTM_CRASH_DUMP_LAST_SEQUENCE_NUM           	0xFFFF    /*  Last sequnce received from controller for crash dump data collection */
+
+#define BTM_CRASH_DUMP_PATH_BUF_SIZE           0xFF    /* Buffer size of the crash dump file path */
+
+#define BTM_GENOA_CRASH_RAMDUMP_PATH           "/var/log"    /* Directory where crash dump file needs to be stored */
+
+#define BTM_MAX_CRASH_DUMP_COUNT           0xFF    /* Maximum number to which crash dump file name can be appended with starting from 0   */
+#endif
 
 /******************************************************************************/
 /*            L O C A L    F U N C T I O N     P R O T O T Y P E S            */
@@ -766,7 +783,152 @@ tBTM_STATUS BTM_RegisterForVSEvents(tBTM_VS_EVT_CB* p_cb, bool is_register) {
 
   return (retval);
 }
+#ifdef LG_WEBOS
+
+/*******************************************************************************
+**
+** Function         btm_crash_dump_handler
+**
+** Description      Handler for handling crash dump event and dumping data to file
+**
+** Returns          void
+**
+*******************************************************************************/
 
+void btm_crash_dump_handler(uint8_t *eventBuf,uint8_t len)
+{
+	static int dumpFd = -1;
+	unsigned char inx_scan;
+	static char path[BTM_CRASH_DUMP_PATH_BUF_SIZE];
+	char nullBuff[255] = {0,};
+	static unsigned int dump_size =0, total_size =0;
+	unsigned short seq_num =0;
+	static unsigned short seq_num_cnt =0;
+	unsigned char *dump_ptr = NULL, data_len=0, packet_len =0;
+	static unsigned char *temp_buf, *p;
+
+
+	data_len = len;
+	dump_ptr = &eventBuf[5];
+	seq_num = eventBuf[2] | (eventBuf[3] << 8);
+
+	BTM_TRACE_EVENT("%s: Seq num: (%d) ", __func__, seq_num);
+	if((seq_num != seq_num_cnt) && seq_num !=BTM_CRASH_DUMP_LAST_SEQUENCE_NUM)
+	{
+		BTM_TRACE_EVENT("%s: current sequence number: %d, expected seq num: %d ", __func__,seq_num, seq_num_cnt);
+	}
+
+
+	if (seq_num == 0x0000)
+	{
+		dump_size = (unsigned int) (eventBuf[5]|(eventBuf[6]<<8)|(eventBuf[7]<<16)|(eventBuf[8]<<24));
+		dump_ptr =  &eventBuf[9];
+		total_size = seq_num_cnt = 0;
+
+		memset(path, 0, BTM_CRASH_DUMP_PATH_BUF_SIZE);
+		/* first pack has total ram dump size (4 bytes) */
+		if(data_len > 4)
+		{
+			data_len -= 4;
+		}
+		else
+		{
+			BTM_TRACE_ERROR("Insufficient crash dump data. Expected atleast four bytes.");
+			return;
+		}
+	
+		BTM_TRACE_EVENT("%s: Crash Dump Start - total Size: %d ", __func__, dump_size);
+
+		p = temp_buf = (unsigned char *) malloc(dump_size);
+		if (p != NULL)
+		{
+			memset(p, 0, dump_size);
+		}
+		else
+		{
+			BTM_TRACE_ERROR("Failed to allocate memory for the crash dump size: %d", dump_size);
+			return;
+		}
+
+		/* Scan for crash dump file */
+		for (inx_scan= 0; inx_scan<BTM_MAX_CRASH_DUMP_COUNT;inx_scan++)
+		{
+			snprintf(path, BTM_CRASH_DUMP_PATH_BUF_SIZE, "%s/bt_fw_crashdump-%.02d.bin",BTM_GENOA_CRASH_RAMDUMP_PATH,inx_scan);
+			BTM_TRACE_EVENT("%s: try to open file : %s", __func__, path);
+			dumpFd = open(path, O_RDONLY);
+			if(dumpFd < 0 && (errno == ENOENT) )
+			{
+				BTM_TRACE_EVENT("%s: file( %s) can be used", __func__, path);
+				break;
+			}
+			close(dumpFd);
+		}
+
+		if(inx_scan >= BTM_MAX_CRASH_DUMP_COUNT)
+		{
+			BTM_TRACE_EVENT("%s: Bluetooth Firmware Crash dump file reaches max count (%d)!\n"
+			              "\t\t\tPlease delete file in %s\n"
+			              "\t\t\tbt_fw_crashdump-last.bin file created",
+			              __func__, inx_scan, BTM_GENOA_CRASH_RAMDUMP_PATH);
+			snprintf(path, BTM_CRASH_DUMP_PATH_BUF_SIZE, "%s/bt_fw_crashdump-last.bin",BTM_GENOA_CRASH_RAMDUMP_PATH );
+		}
+
+		dumpFd = open(path, O_CREAT| O_SYNC | O_WRONLY,  S_IRUSR |S_IWUSR |S_IRGRP );
+		if(dumpFd < 0)
+		{
+			BTM_TRACE_ERROR("%s: File open (%s) failed: errno: %d", __func__, path, errno);
+			seq_num_cnt++;
+			return;
+		}
+		BTM_TRACE_EVENT("%s: File Open (%s) successfull ", __func__, path);
+	}
+
+	if(data_len > 5)
+	{
+		packet_len = data_len - 5;
+	}
+	else
+	{
+		BTM_TRACE_ERROR("Insufficient crash dump data. Expected atleast five bytes.");
+		return;
+	}
+	if(dumpFd >= 0)
+	{
+		//If some packet is missed/dropped fill with null ptr
+		for ( ; (seq_num > seq_num_cnt) && (seq_num !=BTM_CRASH_DUMP_LAST_SEQUENCE_NUM) ; seq_num_cnt++)
+		{
+			BTM_TRACE_EVENT("%s: controller missed packet : %d, write null (%d) into file ",__func__, seq_num_cnt, packet_len);
+
+			if (p != NULL)
+			{
+				 memcpy(temp_buf, nullBuff, packet_len);
+				 temp_buf = temp_buf + packet_len;
+			}
+		}
+
+		if (p != NULL) 
+		{
+			 memcpy(temp_buf, dump_ptr, packet_len);
+			 temp_buf = temp_buf + packet_len;
+		}
+		total_size += packet_len;
+		BTM_TRACE_EVENT("%s: File Write : total_size:%d,seq_num:%d (seq_num_cnt:%d),"
+		  "packet_len: %d", __func__, total_size, seq_num, seq_num_cnt, packet_len);
+	}
+
+	seq_num_cnt++;
+	if (seq_num == BTM_CRASH_DUMP_LAST_SEQUENCE_NUM && p != NULL)
+	{
+		BTM_TRACE_EVENT("Writing crash dump of size %d bytes", total_size);
+		if(write(dumpFd, p, total_size) <= 0)
+		    BTM_TRACE_ERROR("Failed to Write crash dump,errno= %d",errno);
+		free(p);
+		close(dumpFd);
+		dumpFd = -1;
+	}
+
+}
+#endif
 /*******************************************************************************
  *
  * Function         btm_vendor_specific_evt
@@ -783,6 +945,13 @@ void btm_vendor_specific_evt(uint8_t* p, uint8_t evt_len) {
   uint8_t i;
 
   BTM_TRACE_DEBUG("BTM Event: Vendor Specific event from controller");
+#ifdef LG_WEBOS
+    //There are no callback for crash dump event, so handle it by comparing first two bytes(It should be 0x01 and 0x08)
+    if((evt_len > 5) && (p !=NULL) && (p[0] == 0x01 && p[1] ==0x08))
+    {//It is crash dump event, call handler
+        btm_crash_dump_handler(p,evt_len);
+    }
+#endif		
 
   for (i = 0; i < BTM_MAX_VSE_CALLBACKS; i++) {
     if (btm_cb.devcb.p_vend_spec_cb[i])
-- 
1.9.1

