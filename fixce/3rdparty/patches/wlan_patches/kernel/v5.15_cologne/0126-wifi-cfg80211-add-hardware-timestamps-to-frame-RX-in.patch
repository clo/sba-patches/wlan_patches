From 5d6aa1576749729dce080660ccad4a98e3f4dca3 Mon Sep 17 00:00:00 2001
From: Avraham Stern <avraham.stern@intel.com>
Date: Thu, 27 Jan 2022 13:56:29 +0200
Subject: [PATCH 126/179] wifi: cfg80211: add hardware timestamps to frame RX
 info

Add hardware timestamps to management frame RX info.
This shall be used by drivers that support hardware timestamping for
Timing measurement and Fine timing measurement action frames RX.

Signed-off-by: Avraham Stern <avraham.stern@intel.com>
Signed-off-by: Johannes Berg <johannes.berg@intel.com>
---
 include/net/cfg80211.h |  4 ++++
 net/wireless/nl80211.c | 10 +++++++++-
 2 files changed, 13 insertions(+), 1 deletion(-)

diff --git a/include/net/cfg80211.h b/include/net/cfg80211.h
index 0666115..608c4f0 100644
--- a/include/net/cfg80211.h
+++ b/include/net/cfg80211.h
@@ -7800,6 +7800,8 @@ void cfg80211_conn_failed(struct net_device *dev, const u8 *mac_addr,
  * @buf: Management frame (header + body)
  * @len: length of the frame data
  * @flags: flags, as defined in enum nl80211_rxmgmt_flags
+ * @rx_tstamp: Hardware timestamp of frame RX in nanoseconds
+ * @ack_tstamp: Hardware timestamp of ack TX in nanoseconds
  */
 struct cfg80211_rx_info {
 	int freq;
@@ -7807,6 +7809,8 @@ struct cfg80211_rx_info {
 	const u8 *buf;
 	size_t len;
 	u32 flags;
+	u64 rx_tstamp;
+	u64 ack_tstamp;
 };
 
 /**
diff --git a/net/wireless/nl80211.c b/net/wireless/nl80211.c
index 80d3471..e5ed095 100644
--- a/net/wireless/nl80211.c
+++ b/net/wireless/nl80211.c
@@ -18383,7 +18383,15 @@ int nl80211_send_mgmt(struct cfg80211_registered_device *rdev,
 	     nla_put_u32(msg, NL80211_ATTR_RX_SIGNAL_DBM, info->sig_dbm)) ||
 	    nla_put(msg, NL80211_ATTR_FRAME, info->len, info->buf) ||
 	    (info->flags &&
-	     nla_put_u32(msg, NL80211_ATTR_RXMGMT_FLAGS, info->flags)))
+	     nla_put_u32(msg, NL80211_ATTR_RXMGMT_FLAGS, info->flags)) ||
+	    (info->rx_tstamp && nla_put_u64_64bit(msg,
+						  NL80211_ATTR_RX_HW_TIMESTAMP,
+						  info->rx_tstamp,
+						  NL80211_ATTR_PAD)) ||
+	    (info->ack_tstamp && nla_put_u64_64bit(msg,
+						   NL80211_ATTR_TX_HW_TIMESTAMP,
+						   info->ack_tstamp,
+						   NL80211_ATTR_PAD)))
 		goto nla_put_failure;
 
 	genlmsg_end(msg, hdr);
-- 
2.7.4

