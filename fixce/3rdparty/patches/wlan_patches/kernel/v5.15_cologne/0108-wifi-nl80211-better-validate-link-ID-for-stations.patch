From e5cc073569f4118f38fa4d3f38861ad2b05661be Mon Sep 17 00:00:00 2001
From: Johannes Berg <johannes.berg@intel.com>
Date: Wed, 6 Jul 2022 15:31:55 +0200
Subject: [PATCH 108/179] wifi: nl80211: better validate link ID for stations

If we add a station on an MLD, we need a link ID to see
where it lives (by default). Validate the link ID against
the valid_links.

Signed-off-by: Johannes Berg <johannes.berg@intel.com>
---
 net/wireless/nl80211.c | 26 +++++++++++++++++---------
 1 file changed, 17 insertions(+), 9 deletions(-)

diff --git a/net/wireless/nl80211.c b/net/wireless/nl80211.c
index d774e9a..37ec8b3 100644
--- a/net/wireless/nl80211.c
+++ b/net/wireless/nl80211.c
@@ -6991,6 +6991,7 @@ static int nl80211_new_station(struct sk_buff *skb, struct genl_info *info)
 	struct cfg80211_registered_device *rdev = info->user_ptr[0];
 	int err;
 	struct net_device *dev = info->user_ptr[1];
+	struct wireless_dev *wdev = dev->ieee80211_ptr;
 	struct station_parameters params;
 	u8 *mac_addr = NULL;
 	u32 auth_assoc = BIT(NL80211_STA_FLAG_AUTHENTICATED) |
@@ -7018,14 +7019,6 @@ static int nl80211_new_station(struct sk_buff *skb, struct genl_info *info)
 		nl80211_link_id_or_invalid(info->attrs);
 
 	if (info->attrs[NL80211_ATTR_MLD_ADDR]) {
-		/* If MLD_ADDR attribute is set then this is an MLD station
-		 * and the MLD_ADDR attribute holds the MLD address and the
-		 * MAC attribute holds for the LINK address.
-		 * In that case, the link_id is also expected to be valid.
-		 */
-		if (params.link_sta_params.link_id < 0)
-			return -EINVAL;
-
 		mac_addr = nla_data(info->attrs[NL80211_ATTR_MLD_ADDR]);
 		params.link_sta_params.mld_mac = mac_addr;
 		params.link_sta_params.link_mac =
@@ -7253,9 +7246,24 @@ static int nl80211_new_station(struct sk_buff *skb, struct genl_info *info)
 	/* be aware of params.vlan when changing code here */
 
 	wdev_lock(dev->ieee80211_ptr);
+	if (wdev->valid_links) {
+		if (params.link_sta_params.link_id < 0) {
+			err = -EINVAL;
+			goto out;
+		}
+		if (!(wdev->valid_links & BIT(params.link_sta_params.link_id))) {
+			err = -ENOLINK;
+			goto out;
+		}
+	} else {
+		if (params.link_sta_params.link_id >= 0) {
+			err = -EINVAL;
+			goto out;
+		}
+	}
 	err = rdev_add_station(rdev, dev, mac_addr, &params);
+out:
 	wdev_unlock(dev->ieee80211_ptr);
-
 	dev_put(params.vlan);
 	return err;
 }
-- 
2.7.4

