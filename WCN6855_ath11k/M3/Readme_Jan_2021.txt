﻿=================================================Code fetch================================================================================

1)	Clone code base: git clone https://git.kernel.org/pub/scm/linux/kernel/git/kvalo/ath.git -b master

2)	Reset to given tag: git checkout ath-202012180905

3)	Get patches from patchwork.kernel.org as below
	1.	https://patchwork.kernel.org/project/linux-wireless/patch/1609816120-9411-2-git-send-email-wgong@codeaurora.org/
	2.	https://patchwork.kernel.org/project/linux-wireless/patch/1609816120-9411-3-git-send-email-wgong@codeaurora.org/
	
4)	Get patches from git.kernel.org as below
	1.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=556bbb442bbb44f429dbaa9f8b48e0b4cda6e088
	2.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=a03c7a86e12721da9f6bb509dddda19fd9ae8c6c
	3.	https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git/commit/?id=0c76b3fa580da8f65333b8774205f0a72ffe844c
	4.  https://git.kernel.org/pub/scm/linux/kernel/git/bluetooth/bluetooth-next.git/commit/?id=3b0d5250be30e76727284bb9e4c26b662ede7378

5)	Get patches from codeaurora.org as below
	1.	git clone https://source.codeaurora.org/external/sba/wlan_patches
	2.	get patches from: <workspace>/wlan_patches/WCN6855_ath11k/M3
	
6)	Apply the patches of step 3)/4)/5) in below order one by one, total 77 patches. Please note the bottom is the first.
	Bluetooth: btusb: add shutdown function for wcn6855
	ath11k: add sram start and end addr to hw params
	ath11k: advertise PLATFORM_CAP_PCIE_GLOBAL_RESET in qmi msg
	ath11k: don't call ath11k_pci_set_l1ss for WCN6850
	ath11k: add support for WCN6850 hw2.0
	ath11k: add support to get peer id for WCN6850
	ath11k: setup WBM_IDLE_LINK ring once again
	ath11k: setup REO for WCN6850
	ath11k: add dp support for WCN6850
	ath11k: add hw reg support for WCN6850 target
	ath11k: add PCI vendor and device id for WCN6850
	ath11k: add 5 seconds for timeout of scan started if both 6G and 11d scan offload enable
	ath11k: add trace log support from vnaralas(Venkateswara Naralasetty)
	ath11k: calucate the correct nss of peer for HE capabilities
	mac80211: do intersection with he mcs and nss set of peer and own
	mac80211: remove NSS number of 160MHz if not support 160MHz for HE
	ath11k: fix the value of msecs_to_jiffies in ath11k_debugfs_fw_stats_request
	ath11k: set correct NL80211_FEATURE_DYNAMIC_SMPS for WCN6855
	ath11k: enable HE-160MHz and VHT-160MHz bandwidth for WCN6855
	ath11k: treat scan dequeued as aborting and scan complete
	ath11k: report tx bitrate for iw wlan station dump
	ath11k: remove IEEE80211_HW_USES_RSS flag for QCA6390
	ath11k: fix read fail for htt_stats and htt_peer_stats for single pdev
	ath11k: move peer delete after vdev stop of STATION for QCA6390
	ath11k: add ieee80211_unregister_hw to avoid kernel crash caused by NULL pointer
	ath11k: fix blocked for more than 120 seconds caused by reg update
	bus: mhi: core: Prevent sending multiple RDDM entry callbacks
	bus: mhi: core: Mark and maintain device states early on after power down
	bus: mhi: core: Separate system error and power down handling
	ath11k: change to copy cap info of 6G band under WMI_HOST_WLAN_5G_CAP for WCN685X
	ath11k: enable 6G channels for WCN685X
	ath11k: re-enable ht_cap/vht_cap for 5G band for WCN685X
	ath11k: [fix conflict in nl80211.c]add 6ghz params in peer assoc command
	ath11k: remove scan wmi for "ath11k: fix for accepting bcast presp in GHz scan "
	ath11k: fix for accepting bcast presp in GHz scan
	ath11k: change to indicate scan complete for scan canceled
	ath11k: Add signal report to mac80211 for QCA6390 and WCN685X
	ath11k: set scan state to abort if scan not started while restart
	ath11k: change to treat alpha code na as world regdomain
	ath11k: add hw-restart for simulate_fw_crash
	ath11k: report rssi of each chain to mac80211
	ath11k: remove return for empty tx bitrate in mac_op_sta_statistics
	ath11k: add wait opeartion for tx management packets for flush from mac80211
	ath11k: add support for device recovery for QCA6390
	ath11k: add support for hardware rfkill
	ath11k: enable pkt log default for QCA6390
	ath11k: add regdb.bin download for regdb offload
	ath11k: add 11d scan offload support
	ath11k: add handler for WMI_SET_CURRENT_COUNTRY_CMDID
	ath11k: skip sending vdev down for channel switch
	ath11k: add hw connection monitor support
	ath11k: add wmi op version indication for UTF
	ath11k: change check from ATH11K_STATE_ON to ATH11K_STATE_TM for UTF
	ath11k: add support for UTF mode for QCA6390
	ath11k: remove ATH11K_STATE_TM check for restart
	ath11k: factory test mode support from WIN team v6
	ath11k: change some dp ring parameters
	ath11k: enable idle power save in ath11k_core_qmi_firmware_ready
	ath11k: force wake target before mhi stop
	ath11k: Adjust the dest entries of some Copy Engines
	ath11k: decrease clients to 16 from 64 for QCA6390 series
	ath11k: decrease MHI IPC inbound entries and buffer length
	ath11k: fix deadloop in ath11k_dp_tx
	ath11k: don't send msi info to firmware for lmac rings
	ath11k: debug M3 download issue
	ath11k: define different tx comp ring size for QCA6390
	ath11k: fix memory leak of qmi event
	ath11k: fix invalid m3 buffer address
	ath11k: destroy workqueue when module is unloaded
	ath11k: dump sram if firmware bootup fails
	ath11k: set dtim policy to stick mode for station interface
	ath11k: support MAC address randomization in scan
	ath11k: support gtk rekey offload
	ath11k: support arp and ns offload
	ath11k: purge rx pktlog when entering WoW
	ath11k: implement hw data filter
	ath11k: add basic WoW functionality

=================================================Compilation================================================================================

1)	make menuconfig and change config and save
	run cmd: make menuconfig and select the following 
	[M]Device Drivers ---> Bus devices ---> Modem Host Interface[CONFIG_MHI_BUS=m]
	[M]Device Drivers ---> SOC (System On Chip) specific Drivers ---> Qualcomm SoC drivers ---> Qualcomm qmi helpers[CONFIG_QCOM_QMI_HELPERS=m]
	[M]Networking support ---> Networking options ---> Qualcomm IPC Router support[CONFIG_QRTR=m]
	[M]Networking support ---> Networking options ---> MHI IPC Router channels[CONFIG_QRTR_MHI=m]
	[M]Device Drivers ---> Network device support ---> Wireless LAN ---> Qualcomm Technologies 802.11ax chipset support[CONFIG_ATH11K=m]
	[M]Device Drivers ---> Network device support ---> Wireless LAN ---> Qualcomm Technologies 802.11ax chipset PCI support[CONFIG_ATH11K_PCI=m]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> QCA ath11k debugging[CONFIG_ATH11K_DEBUG=y]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> QCA ath11k debugfs support[CONFIG_ATH11K_DEBUGFS=y]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> ath11k tracing support[CONFIG_ATH11K_TRACING=y]
	[*]Device Drivers ---> Character devices ---> Serial device bus
	[*]Networking support ---> Bluetooth subsystem support ---> Bluetooth device drivers ---> Qualcomm Atheros protocol support
	[*]Networking support ---> Wireless ---> cfg80211 certification onus[CONFIG_CFG80211_CERTIFICATION_ONUS=y]
	[*]Networking support ---> Wireless ---> nl80211 testmode command[CONFIG_NL80211_TESTMODE=y]
	[*]Device Drivers ---> Network device support ---> Wireless LAN ---> Atheros dynamic user regulatory hints[CONFIG_ATH_REG_DYNAMIC_USER_REG_HINTS=y]

2)	To build the kernel follow the steps 
	1.	make
	2.	sudo make modules_install
	3.	sudo make install


======================================WLAN bring up=======================================================================   
 
1)	Copy firmware files
	1. find firmware from CE team
	2. Copy all firmware binary files to /lib/firmware/ath11k/WCN6850/hw2.0 and rename bd file 
	3. sudo cp amss.mbn /lib/firmware/ath11k/WCN6850/hw2.0/amss.bin 
	4. sudo cp bdwlan01.e06 /lib/firmware/ath11k/WCN6850/hw2.0/board.bin 
	5. sudo cp m3.bin  /lib/firmware/ath11k/WCN6850/hw2.0
    	6. sudo cp regdb.bin  /lib/firmware/ath11k/WCN6850/hw2.0

 
2)	Load modules
	sudo modprobe cfg80211
	sudo modprobe mac80211

	cd /lib/modules/5.10.0-wt-ath+/kernel/drivers/bus/mhi/core
	sudo insmod mhi.ko
	
	cd /lib/modules/5.10.0-wt-ath+/kernel/net/qrtr
	sudo insmod ns.ko
	sudo insmod qrtr.ko
	sudo insmod qrtr-mhi.ko

	cd /lib/modules/5.10.0-wt-ath+/kernel/drivers/soc/qcom
	sudo insmod qmi_helpers.ko

	cd /lib/modules/5.10.0-wt-ath+/kernel/drivers/net/wireless/ath/ath11k
	sudo insmod ath11k.ko debug_mask=0xffffffff
	sudo insmod ath11k_pci.ko

3)	Use ifconfig to check whether the WiFi interface is up. If yes, try scan and connect AP from Ubuntu Network Manager GUI

4)	If wifi is loaded successfully, try Reboot system see whether wifi driver can be loaded automatically and successfully 

======================================Bluetooth bring up=======================================================================   
1)	Get BT firmware files
	
2)	Copy AthrBT_0x00130200.dfu, ramps_0x00130200.dfu, ramps_0x00130200_0104.dfu, ramps_0x00130200_0105.dfu, ramps_0x00130200_0106.dfu and ramps_0x00130200_0107.dfu into /lib/firmware/qca

3)	Reboot the DUT and BT should be workable now
