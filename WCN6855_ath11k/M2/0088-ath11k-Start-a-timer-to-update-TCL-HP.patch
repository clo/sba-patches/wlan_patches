From fd5def23122fab96d7f5b4625a5bd533390ef916 Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Thu, 26 Mar 2020 14:47:55 +0800
Subject: [PATCH 088/124] ath11k: Start a timer to update TCL HP

The timer is to check if TCL HP isn't updated to target.
The timer will postpone itself if there are TX operations
during the interval, otherwise the timer handler updates
the HP again so the index value in HP register will be
forwarded to target register, and the timer stops afterwards.

Tested-on: QCA6390 WLAN.HST.1.0.1-01230-QCAHSTSWPLZ_V2_TO_X86-1

Change-Id: I372b21d88e815f88b8acb1be6cf6761385893a5f
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/dp.c    | 79 +++++++++++++++++++++++++++++++++
 drivers/net/wireless/ath/ath11k/dp.h    | 22 +++++++++
 drivers/net/wireless/ath/ath11k/dp_tx.c |  3 ++
 drivers/net/wireless/ath/ath11k/hal.c   | 13 ++++++
 drivers/net/wireless/ath/ath11k/hal.h   |  2 +
 drivers/net/wireless/ath/ath11k/wow.c   |  3 ++
 6 files changed, 122 insertions(+)

diff --git a/drivers/net/wireless/ath/ath11k/dp.c b/drivers/net/wireless/ath/ath11k/dp.c
index 647772d..ffbac86 100644
--- a/drivers/net/wireless/ath/ath11k/dp.c
+++ b/drivers/net/wireless/ath/ath11k/dp.c
@@ -322,11 +322,23 @@ int ath11k_dp_srng_setup(struct ath11k_base *ab, struct dp_srng *ring,
 	return 0;
 }
 
+void ath11k_dp_stop_shadow_timers(struct ath11k_base *ab)
+{
+	int i;
+
+	if (!ab->hw_params.shadow_support_fix)
+		return;
+
+	for (i = 0; i < DP_TCL_NUM_RING_MAX; i++)
+		ath11k_shadow_stop_timer(&ab->dp.tx_ring_timer[i]);
+}
+
 static void ath11k_dp_srng_common_cleanup(struct ath11k_base *ab)
 {
 	struct ath11k_dp *dp = &ab->dp;
 	int i;
 
+	ath11k_dp_stop_shadow_timers(ab);
 	ath11k_dp_srng_cleanup(ab, &dp->wbm_desc_rel_ring);
 	ath11k_dp_srng_cleanup(ab, &dp->tcl_cmd_ring);
 	ath11k_dp_srng_cleanup(ab, &dp->tcl_status_ring);
@@ -392,6 +404,12 @@ static int ath11k_dp_srng_common_setup(struct ath11k_base *ab)
 
 		srng = &ab->hal.srng_list[dp->tx_ring[i].tcl_data_ring.ring_id];
 		ath11k_hal_tx_init_data_ring(ab, srng);
+
+		if (ab->hw_params.shadow_support_fix) {
+			ath11k_shadow_init_timer(ab, &dp->tx_ring_timer[i],
+						 ATH11K_SHADOW_DP_TIMER_INTERVAL,
+						 dp->tx_ring[i].tcl_data_ring.ring_id);
+		}
 	}
 
 	ret = ath11k_dp_srng_setup(ab, &dp->reo_reinject_ring, HAL_REO_REINJECT,
@@ -1085,3 +1103,64 @@ int ath11k_dp_alloc(struct ath11k_base *ab)
 
 	return ret;
 }
+
+static void ath11k_shadow_timer_handler(struct timer_list *t)
+{
+	struct ath11k_hp_update_timer *update_timer = from_timer(update_timer,
+		t, timer);
+	struct ath11k_base *ab = update_timer->ab;
+	struct hal_srng	*srng = &ab->hal.srng_list[update_timer->ring_id];
+
+	spin_lock_bh(&srng->lock);
+
+	/* when the timer is fired, the handler checks whether there
+	 * are new TX happened. The handler updates HP only when there
+	 * are no TX operations during the timeout interval, and stop
+	 * the timer. Timer will be started again when TX happens again.
+	 */
+	if (update_timer->timer_tx_num != update_timer->tx_num) {
+		update_timer->timer_tx_num = update_timer->tx_num;
+		mod_timer(&update_timer->timer, jiffies +
+		  msecs_to_jiffies(update_timer->interval));
+	} else {
+		update_timer->started = false;
+		ath11k_hal_srng_shadow_update_hp_tp(ab, srng);
+	}
+
+	spin_unlock_bh(&srng->lock);
+}
+
+/* it must be called with the hal_ring->lock been hold */
+void ath11k_shadow_start_timer(struct ath11k_base *ab,
+			       struct hal_srng	*srng,
+			       struct ath11k_hp_update_timer *update_timer)
+{
+	update_timer->tx_num++;
+	if (!update_timer->started) {
+		update_timer->started = true;
+		update_timer->timer_tx_num = update_timer->tx_num;
+		mod_timer(&update_timer->timer, jiffies +
+		  msecs_to_jiffies(update_timer->interval));
+	}
+}
+
+void ath11k_shadow_stop_timer(struct ath11k_hp_update_timer *update_timer)
+{
+	if (update_timer->init)
+		del_timer_sync(&update_timer->timer);
+}
+
+void ath11k_shadow_init_timer(struct ath11k_base *ab,
+			      struct ath11k_hp_update_timer *update_timer,
+			      u32 interval, u32 ring_id)
+{
+	update_timer->tx_num = 0;
+	update_timer->timer_tx_num = 0;
+	update_timer->ab = ab;
+	update_timer->ring_id = ring_id;
+	update_timer->interval = interval;
+	update_timer->init = true;
+	timer_setup(&update_timer->timer,
+		    ath11k_shadow_timer_handler, 0);
+}
+
diff --git a/drivers/net/wireless/ath/ath11k/dp.h b/drivers/net/wireless/ath/ath11k/dp.h
index 74ba1d6..967aeb2 100644
--- a/drivers/net/wireless/ath/ath11k/dp.h
+++ b/drivers/net/wireless/ath/ath11k/dp.h
@@ -210,6 +210,19 @@ struct ath11k_pdev_dp {
 #define DP_TX_DESC_ID_MSDU_ID GENMASK(18, 2)
 #define DP_TX_DESC_ID_POOL_ID GENMASK(20, 19)
 
+#define ATH11K_SHADOW_DP_TIMER_INTERVAL 20
+
+struct ath11k_hp_update_timer {
+	struct timer_list timer;
+	bool   started;
+	bool   init;
+	u32    tx_num;
+	u32    timer_tx_num;
+	u32    ring_id;
+	u32	   interval;
+	struct ath11k_base *ab;
+};
+
 struct ath11k_dp {
 	struct ath11k_base *ab;
 	enum ath11k_htc_ep_id eid;
@@ -239,6 +252,7 @@ struct ath11k_dp {
 	 * - reo_cmd_cache_flush_count
 	 */
 	spinlock_t reo_cmd_lock;
+	struct ath11k_hp_update_timer tx_ring_timer[DP_TCL_NUM_RING_MAX];
 };
 
 /* HTT definitions */
@@ -1578,5 +1592,13 @@ int ath11k_dp_link_desc_setup(struct ath11k_base *ab,
 			      struct dp_link_desc_bank *link_desc_banks,
 			      u32 ring_type, struct hal_srng *srng,
 			      u32 n_link_desc);
+void ath11k_dp_stop_shadow_timers(struct ath11k_base *ab);
+void ath11k_shadow_start_timer(struct ath11k_base *ab,
+			       struct hal_srng	*srng,
+			       struct ath11k_hp_update_timer *update_timer);
+void ath11k_shadow_stop_timer(struct ath11k_hp_update_timer *update_timer);
+void ath11k_shadow_init_timer(struct ath11k_base *ab,
+			      struct ath11k_hp_update_timer *update_timer,
+			      u32 interval, u32 ring_id);
 
 #endif
diff --git a/drivers/net/wireless/ath/ath11k/dp_tx.c b/drivers/net/wireless/ath/ath11k/dp_tx.c
index 5958a50..43300b5 100644
--- a/drivers/net/wireless/ath/ath11k/dp_tx.c
+++ b/drivers/net/wireless/ath/ath11k/dp_tx.c
@@ -236,6 +236,9 @@ int ath11k_dp_tx(struct ath11k *ar, struct ath11k_vif *arvif,
 
 	ath11k_hal_srng_access_end(ab, tcl_ring);
 
+	if (ab->hw_params.shadow_support_fix)
+		ath11k_shadow_start_timer(ab, tcl_ring, &dp->tx_ring_timer[ti.ring_id]);
+
 	spin_unlock_bh(&tcl_ring->lock);
 
 	atomic_inc(&ar->dp.num_tx_pending);
diff --git a/drivers/net/wireless/ath/ath11k/hal.c b/drivers/net/wireless/ath/ath11k/hal.c
index 17584b3..468862d 100644
--- a/drivers/net/wireless/ath/ath11k/hal.c
+++ b/drivers/net/wireless/ath/ath11k/hal.c
@@ -1174,6 +1174,19 @@ void ath11k_hal_srng_get_shadow_config(struct ath11k_base *ab,
 	*cfg = hal->shadow_reg_addr;
 }
 
+/* it must be called with srng->lock being hold */
+void ath11k_hal_srng_shadow_update_hp_tp(struct ath11k_base *ab,
+					 struct hal_srng *srng)
+{
+	/* check whether the ring is emptry. Update the shadow
+	 * HP only when then ring isn't' empty.
+	 */
+	if (srng->ring_dir == HAL_SRNG_DIR_SRC &&
+	    *(volatile u32 *)srng->u.src_ring.tp_addr !=
+			srng->u.src_ring.hp)
+		ath11k_hal_srng_access_end(ab, srng);
+}
+
 static void ath11k_hal_srng_init_config(struct ath11k_base *ab)
 {
 	struct ath11k_hal *hal = &ab->hal;
diff --git a/drivers/net/wireless/ath/ath11k/hal.h b/drivers/net/wireless/ath/ath11k/hal.h
index 16e5e69..f84d27b 100644
--- a/drivers/net/wireless/ath/ath11k/hal.h
+++ b/drivers/net/wireless/ath/ath11k/hal.h
@@ -925,4 +925,6 @@ int ath11k_hal_srng_update_shadow_config(struct ath11k_base *ab,
 					 enum hal_ring_type ring_type,
 					int ring_num);
 void ath11k_hal_srng_shadow_config(struct ath11k_base *ab);
+void ath11k_hal_srng_shadow_update_hp_tp(struct ath11k_base *ab,
+					 struct hal_srng *srng);
 #endif
diff --git a/drivers/net/wireless/ath/ath11k/wow.c b/drivers/net/wireless/ath/ath11k/wow.c
index 856a964..482a335 100644
--- a/drivers/net/wireless/ath/ath11k/wow.c
+++ b/drivers/net/wireless/ath/ath11k/wow.c
@@ -605,6 +605,9 @@ int ath11k_wow_op_suspend(struct ieee80211_hw *hw,
 		goto wakeup;
 	}
 
+	/* stop shadow hp update related timers */
+	ath11k_dp_stop_shadow_timers(ar->ab);
+
 	goto exit;
 
 wakeup:
-- 
2.7.4

