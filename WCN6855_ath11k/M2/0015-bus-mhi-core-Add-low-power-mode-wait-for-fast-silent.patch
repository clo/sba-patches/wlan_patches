From e555568d6b636e5dcf27df068893671053a13adf Mon Sep 17 00:00:00 2001
From: Bhaumik Bhatt <bbhatt@codeaurora.org>
Date: Wed, 6 May 2020 18:10:47 -0700
Subject: [PATCH 015/124] bus: mhi: core: Add low power mode wait for
 fast/silent suspends

The device should be given a chance to attempt an entry to MHI M2 state
and enable its own low power mode before the host attempts its fast or
silent suspend. This can result in better power savings for the device
if it occurs. Introduce the m2_timeout_ms configuration option to allow
for this.

Change-Id: I9385b2ba9a2df55dad35921d6d28b207c846bfb6
Signed-off-by: Bhaumik Bhatt <bbhatt@codeaurora.org>
---
 drivers/bus/mhi/core/init.c | 1 +
 drivers/bus/mhi/core/pm.c   | 5 +++++
 include/linux/mhi.h         | 4 ++++
 3 files changed, 10 insertions(+)

diff --git a/drivers/bus/mhi/core/init.c b/drivers/bus/mhi/core/init.c
index 0ae4c34..cbfce89 100644
--- a/drivers/bus/mhi/core/init.c
+++ b/drivers/bus/mhi/core/init.c
@@ -835,6 +835,7 @@ static int parse_config(struct mhi_controller *mhi_cntrl,
 	mhi_cntrl->timeout_ms = config->timeout_ms;
 	if (!mhi_cntrl->timeout_ms)
 		mhi_cntrl->timeout_ms = MHI_TIMEOUT_MS;
+	mhi_cntrl->m2_timeout_ms = config->m2_timeout_ms;
 
 	mhi_cntrl->bounce_buf = config->use_bounce_buf;
 	mhi_cntrl->buffer_len = config->buf_len;
diff --git a/drivers/bus/mhi/core/pm.c b/drivers/bus/mhi/core/pm.c
index 6633360..7a0fc94 100644
--- a/drivers/bus/mhi/core/pm.c
+++ b/drivers/bus/mhi/core/pm.c
@@ -810,6 +810,11 @@ int mhi_pm_fast_suspend(struct mhi_controller *mhi_cntrl, bool notify_clients)
 	    atomic_read(&mhi_dev->bus_vote))
 		return -EBUSY;
 
+	/* wait for the device to attempt a low power mode (M2 entry) */
+	wait_event_timeout(mhi_cntrl->state_event,
+			   mhi_cntrl->dev_state == MHI_STATE_M2,
+			   msecs_to_jiffies(mhi_cntrl->m2_timeout_ms));
+
 	/* disable primary event ring processing to prevent interference */
 	tasklet_disable(&mhi_cntrl->mhi_event->task);
 
diff --git a/include/linux/mhi.h b/include/linux/mhi.h
index 2790c51..3507909 100644
--- a/include/linux/mhi.h
+++ b/include/linux/mhi.h
@@ -270,6 +270,7 @@ struct mhi_event_config {
  * struct mhi_controller_config - Root MHI controller configuration
  * @max_channels: Maximum number of channels supported
  * @timeout_ms: Timeout value for operations. 0 means use default
+ * @m2_timeout_ms: Timeout value for fast suspend operations. 0 ms if not set.
  * @buf_len: Size of automatically allocated buffers. 0 means use default
  * @num_channels: Number of channels defined in @ch_cfg
  * @ch_cfg: Array of defined channels
@@ -281,6 +282,7 @@ struct mhi_event_config {
 struct mhi_controller_config {
 	u32 max_channels;
 	u32 timeout_ms;
+	u32 m2_timeout_ms;
 	u32 buf_len;
 	u32 num_channels;
 	struct mhi_channel_config *ch_cfg;
@@ -330,6 +332,7 @@ struct mhi_controller_config {
  * @pm_mutex: Mutex for suspend/resume operation
  * @pm_lock: Lock for protecting MHI power management state
  * @timeout_ms: Timeout in ms for state transitions
+ * @m2_timeout_ms: Timeout in ms to wait for the device to attempt an M2
  * @pm_state: MHI power management state
  * @saved_pm_state: MHI power management state backup used in fast/silent modes
  * @db_access: DB access states
@@ -416,6 +419,7 @@ struct mhi_controller {
 	struct mutex pm_mutex;
 	rwlock_t pm_lock;
 	u32 timeout_ms;
+	u32 m2_timeout_ms;
 	u32 pm_state;
 	u32 saved_pm_state;
 	u32 db_access;
-- 
2.7.4

