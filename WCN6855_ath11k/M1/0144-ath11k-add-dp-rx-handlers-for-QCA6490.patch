From d29c63babdef0e6972b173af184caa044f986a0d Mon Sep 17 00:00:00 2001
From: Baochen qiang <bqiang@codeaurora.org>
Date: Fri, 17 Jul 2020 17:59:17 +0800
Subject: [PATCH 160/173] ath11k: add dp rx handlers for QCA6490

Since there are two versions of hal_rx_desc, any code related to this structure
needs to pay attention to the target being used.

Tested-on: QCA6490 hw1.1 PCI WLAN.HSP.1.1-00687-QCAHSPSWPL_V1_SILICONZ-1

Signed-off-by: Baochen qiang <bqiang@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/dp.c    |   3 +
 drivers/net/wireless/ath/ath11k/dp.h    |   2 +
 drivers/net/wireless/ath/ath11k/dp_rx.c | 588 +++++++++++++++++++++++++++++---
 drivers/net/wireless/ath/ath11k/dp_rx.h |  38 +++
 4 files changed, 587 insertions(+), 44 deletions(-)

diff --git a/drivers/net/wireless/ath/ath11k/dp.c b/drivers/net/wireless/ath/ath11k/dp.c
index 605df29..1f4c8e6 100644
--- a/drivers/net/wireless/ath/ath11k/dp.c
+++ b/drivers/net/wireless/ath/ath11k/dp.c
@@ -1097,6 +1097,9 @@ int ath11k_dp_alloc(struct ath11k_base *ab)
 		ath11k_hal_tx_set_dscp_tid_map(ab, i);
 
 	/* Init any SOC level resource for DP */
+	ret = ath11k_dp_rx_ops_init(ab);
+	if(ret)
+		goto fail_cmn_srng_cleanup;
 
 	return 0;
 
diff --git a/drivers/net/wireless/ath/ath11k/dp.h b/drivers/net/wireless/ath/ath11k/dp.h
index 0fc4cf9..711c92e 100644
--- a/drivers/net/wireless/ath/ath11k/dp.h
+++ b/drivers/net/wireless/ath/ath11k/dp.h
@@ -255,6 +255,8 @@ struct ath11k_dp {
 	spinlock_t reo_cmd_lock;
 	struct ath11k_hp_update_timer reo_cmd_timer;
 	struct ath11k_hp_update_timer tx_ring_timer[DP_TCL_NUM_RING_MAX];
+
+	struct dp_rx_ops *rx_ops;
 };
 
 /* HTT definitions */
diff --git a/drivers/net/wireless/ath/ath11k/dp_rx.c b/drivers/net/wireless/ath/ath11k/dp_rx.c
index f4c38e1..ec05e0b 100644
--- a/drivers/net/wireless/ath/ath11k/dp_rx.c
+++ b/drivers/net/wireless/ath/ath11k/dp_rx.c
@@ -18,13 +18,36 @@
 
 #define ATH11K_DP_RX_FRAGMENT_TIMEOUT_MS (2 * HZ)
 
-static u8 *ath11k_dp_rx_h_80211_hdr(struct hal_rx_desc *desc)
+static u8 *ath11k_dp_rx_h_80211_hdr(void *buf)
 {
+	struct hal_rx_desc *desc = 
+		(struct hal_rx_desc*)buf;
 	return desc->hdr_status;
 }
+static u8 *ath11k_dp_rx_h_80211_hdr_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = 
+		(struct hal_rx_desc_qca6490*)buf;
+	return desc->hdr_status;
+}
+static enum hal_encrypt_type ath11k_dp_rx_h_mpdu_start_enctype(void *buf)
+{
+	struct hal_rx_desc *desc = 
+		(struct hal_rx_desc*)buf;
+
+	if (!(__le32_to_cpu(desc->mpdu_start.info1) &
+	    RX_MPDU_START_INFO1_ENCRYPT_INFO_VALID))
+		return HAL_ENCRYPT_TYPE_OPEN;
 
-static enum hal_encrypt_type ath11k_dp_rx_h_mpdu_start_enctype(struct hal_rx_desc *desc)
+	return FIELD_GET(RX_MPDU_START_INFO2_ENC_TYPE,
+			 __le32_to_cpu(desc->mpdu_start.info2));
+}
+
+static enum hal_encrypt_type ath11k_dp_rx_h_mpdu_start_enctype_qca6490(void *buf)
 {
+	struct hal_rx_desc_qca6490 *desc = 
+		(struct hal_rx_desc_qca6490*)buf;
+
 	if (!(__le32_to_cpu(desc->mpdu_start.info1) &
 	    RX_MPDU_START_INFO1_ENCRYPT_INFO_VALID))
 		return HAL_ENCRYPT_TYPE_OPEN;
@@ -33,26 +56,71 @@ static enum hal_encrypt_type ath11k_dp_rx_h_mpdu_start_enctype(struct hal_rx_des
 			 __le32_to_cpu(desc->mpdu_start.info2));
 }
 
-static u8 ath11k_dp_rx_h_msdu_start_decap_type(struct hal_rx_desc *desc)
+
+static u8 ath11k_dp_rx_h_msdu_start_decap_type(void *buf)
 {
+	struct hal_rx_desc *desc = 
+		(struct hal_rx_desc*)buf;
+
 	return FIELD_GET(RX_MSDU_START_INFO2_DECAP_FORMAT,
 			 __le32_to_cpu(desc->msdu_start.info2));
 }
+static u8 ath11k_dp_rx_h_msdu_start_decap_type_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = 
+		(struct hal_rx_desc_qca6490*)buf;
+
+	return FIELD_GET(RX_MSDU_START_INFO2_DECAP_FORMAT,
+			 __le32_to_cpu(desc->msdu_start.info2));
+}
+
+static u8 ath11k_dp_rx_h_msdu_start_mesh_ctl_present(void *buf)
+{
+	struct hal_rx_desc *desc = 
+		(struct hal_rx_desc*)buf;
 
-static u8 ath11k_dp_rx_h_msdu_start_mesh_ctl_present(struct hal_rx_desc *desc)
+	return FIELD_GET(RX_MSDU_START_INFO2_MESH_CTRL_PRESENT,
+			 __le32_to_cpu(desc->msdu_start.info2));
+}
+static u8 ath11k_dp_rx_h_msdu_start_mesh_ctl_present_qca6490(void *buf)
 {
+	struct hal_rx_desc_qca6490 *desc = 
+		(struct hal_rx_desc_qca6490*)buf;
+
 	return FIELD_GET(RX_MSDU_START_INFO2_MESH_CTRL_PRESENT,
 			 __le32_to_cpu(desc->msdu_start.info2));
 }
 
-static bool ath11k_dp_rx_h_mpdu_start_seq_ctrl_valid(struct hal_rx_desc *desc)
+static bool ath11k_dp_rx_h_mpdu_start_seq_ctrl_valid(void *buf)
 {
+	struct hal_rx_desc *desc = 
+		(struct hal_rx_desc*)buf;
+
 	return !!FIELD_GET(RX_MPDU_START_INFO1_MPDU_SEQ_CTRL_VALID,
 			   __le32_to_cpu(desc->mpdu_start.info1));
 }
+static bool ath11k_dp_rx_h_mpdu_start_seq_ctrl_valid_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = 
+		(struct hal_rx_desc_qca6490*)buf;
 
-static bool ath11k_dp_rx_h_mpdu_start_fc_valid(struct hal_rx_desc *desc)
+	return !!FIELD_GET(RX_MPDU_START_INFO1_MPDU_SEQ_CTRL_VALID,
+			   __le32_to_cpu(desc->mpdu_start.info1));
+}
+
+static bool ath11k_dp_rx_h_mpdu_start_fc_valid(void *buf)
+{
+	struct hal_rx_desc *desc = 
+		(struct hal_rx_desc*)buf;
+
+	return !!FIELD_GET(RX_MPDU_START_INFO1_MPDU_FCTRL_VALID,
+			   __le32_to_cpu(desc->mpdu_start.info1));
+}
+static bool ath11k_dp_rx_h_mpdu_start_fc_valid_qca6490(void *buf)
 {
+	struct hal_rx_desc_qca6490 *desc = 
+		(struct hal_rx_desc_qca6490*)buf;
+
 	return !!FIELD_GET(RX_MPDU_START_INFO1_MPDU_FCTRL_VALID,
 			   __le32_to_cpu(desc->mpdu_start.info1));
 }
@@ -61,7 +129,14 @@ static bool ath11k_dp_rx_h_mpdu_start_more_frags(struct sk_buff *skb)
 {
 	struct ieee80211_hdr *hdr;
 
-	hdr = (struct ieee80211_hdr *)(skb->data + HAL_RX_DESC_SIZE);
+	hdr = (struct ieee80211_hdr *)(skb->data + sizeof(struct hal_rx_desc));
+	return ieee80211_has_morefrags(hdr->frame_control);
+}
+static bool ath11k_dp_rx_h_mpdu_start_more_frags_qca6490(struct sk_buff *skb)
+{
+	struct ieee80211_hdr *hdr;
+
+	hdr = (struct ieee80211_hdr *)(skb->data + sizeof(struct hal_rx_desc_qca6490));
 	return ieee80211_has_morefrags(hdr->frame_control);
 }
 
@@ -69,43 +144,140 @@ static u16 ath11k_dp_rx_h_mpdu_start_frag_no(struct sk_buff *skb)
 {
 	struct ieee80211_hdr *hdr;
 
-	hdr = (struct ieee80211_hdr *)(skb->data + HAL_RX_DESC_SIZE);
+	hdr = (struct ieee80211_hdr *)(skb->data + sizeof(struct hal_rx_desc));
 	return le16_to_cpu(hdr->seq_ctrl) & IEEE80211_SCTL_FRAG;
 }
+static u16 ath11k_dp_rx_h_mpdu_start_frag_no_qca6490(struct sk_buff *skb)
+{
+	struct ieee80211_hdr *hdr;
 
-static u16 ath11k_dp_rx_h_mpdu_start_seq_no(struct hal_rx_desc *desc)
+	hdr = (struct ieee80211_hdr *)(skb->data + sizeof(struct hal_rx_desc_qca6490));
+	return le16_to_cpu(hdr->seq_ctrl) & IEEE80211_SCTL_FRAG;
+}
+
+static u16 ath11k_dp_rx_h_mpdu_start_seq_no(void *buf)
+{
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
+
+	return FIELD_GET(RX_MPDU_START_INFO1_MPDU_SEQ_NUM,
+			 __le32_to_cpu(desc->mpdu_start.info1));
+}
+static u16 ath11k_dp_rx_h_mpdu_start_seq_no_qca6490(void *buf)
 {
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
+
 	return FIELD_GET(RX_MPDU_START_INFO1_MPDU_SEQ_NUM,
 			 __le32_to_cpu(desc->mpdu_start.info1));
 }
 
-static bool ath11k_dp_rx_h_attn_msdu_done(struct hal_rx_desc *desc)
+static bool ath11k_dp_rx_h_attn_msdu_done(void *buf)
 {
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
+
+	return !!FIELD_GET(RX_ATTENTION_INFO2_MSDU_DONE,
+			   __le32_to_cpu(desc->attention.info2));
+}
+static bool ath11k_dp_rx_h_attn_msdu_done_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
+
 	return !!FIELD_GET(RX_ATTENTION_INFO2_MSDU_DONE,
 			   __le32_to_cpu(desc->attention.info2));
 }
 
-static bool ath11k_dp_rx_h_attn_l4_cksum_fail(struct hal_rx_desc *desc)
+static bool ath11k_dp_rx_h_attn_l4_cksum_fail(void *buf)
 {
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
+
 	return !!FIELD_GET(RX_ATTENTION_INFO1_TCP_UDP_CKSUM_FAIL,
 			   __le32_to_cpu(desc->attention.info1));
 }
+static bool ath11k_dp_rx_h_attn_l4_cksum_fail_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
+
+	return !!FIELD_GET(RX_ATTENTION_INFO1_TCP_UDP_CKSUM_FAIL,
+			   __le32_to_cpu(desc->attention.info1));
+}
+
+static bool ath11k_dp_rx_h_attn_ip_cksum_fail(void *buf)
+{
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
 
-static bool ath11k_dp_rx_h_attn_ip_cksum_fail(struct hal_rx_desc *desc)
+	return !!FIELD_GET(RX_ATTENTION_INFO1_IP_CKSUM_FAIL,
+			   __le32_to_cpu(desc->attention.info1));
+}
+static bool ath11k_dp_rx_h_attn_ip_cksum_fail_qca6490(void *buf)
 {
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
+
 	return !!FIELD_GET(RX_ATTENTION_INFO1_IP_CKSUM_FAIL,
 			   __le32_to_cpu(desc->attention.info1));
 }
 
-static bool ath11k_dp_rx_h_attn_is_decrypted(struct hal_rx_desc *desc)
+static bool ath11k_dp_rx_h_attn_is_decrypted(void *buf)
+{
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
+
+	return (FIELD_GET(RX_ATTENTION_INFO2_DCRYPT_STATUS_CODE,
+			  __le32_to_cpu(desc->attention.info2)) ==
+		RX_DESC_DECRYPT_STATUS_CODE_OK);
+}
+static bool ath11k_dp_rx_h_attn_is_decrypted_qca6490(void *buf)
 {
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
+
 	return (FIELD_GET(RX_ATTENTION_INFO2_DCRYPT_STATUS_CODE,
 			  __le32_to_cpu(desc->attention.info2)) ==
 		RX_DESC_DECRYPT_STATUS_CODE_OK);
 }
 
-static u32 ath11k_dp_rx_h_attn_mpdu_err(struct hal_rx_desc *desc)
+static u32 ath11k_dp_rx_h_attn_mpdu_err(void *buf)
 {
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
+
+	u32 info = __le32_to_cpu(desc->attention.info1);
+	u32 errmap = 0;
+
+	if (info & RX_ATTENTION_INFO1_FCS_ERR)
+		errmap |= DP_RX_MPDU_ERR_FCS;
+
+	if (info & RX_ATTENTION_INFO1_DECRYPT_ERR)
+		errmap |= DP_RX_MPDU_ERR_DECRYPT;
+
+	if (info & RX_ATTENTION_INFO1_TKIP_MIC_ERR)
+		errmap |= DP_RX_MPDU_ERR_TKIP_MIC;
+
+	if (info & RX_ATTENTION_INFO1_A_MSDU_ERROR)
+		errmap |= DP_RX_MPDU_ERR_AMSDU_ERR;
+
+	if (info & RX_ATTENTION_INFO1_OVERFLOW_ERR)
+		errmap |= DP_RX_MPDU_ERR_OVERFLOW;
+
+	if (info & RX_ATTENTION_INFO1_MSDU_LEN_ERR)
+		errmap |= DP_RX_MPDU_ERR_MSDU_LEN;
+
+	if (info & RX_ATTENTION_INFO1_MPDU_LEN_ERR)
+		errmap |= DP_RX_MPDU_ERR_MPDU_LEN;
+
+	return errmap;
+}
+static u32 ath11k_dp_rx_h_attn_mpdu_err_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
+
 	u32 info = __le32_to_cpu(desc->attention.info1);
 	u32 errmap = 0;
 
@@ -133,81 +305,238 @@ static u32 ath11k_dp_rx_h_attn_mpdu_err(struct hal_rx_desc *desc)
 	return errmap;
 }
 
-static u16 ath11k_dp_rx_h_msdu_start_msdu_len(struct hal_rx_desc *desc)
+static u16 ath11k_dp_rx_h_msdu_start_msdu_len(void *buf)
 {
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
+
 	return FIELD_GET(RX_MSDU_START_INFO1_MSDU_LENGTH,
 			 __le32_to_cpu(desc->msdu_start.info1));
 }
+static u16 ath11k_dp_rx_h_msdu_start_msdu_len_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
+
+	return FIELD_GET(RX_MSDU_START_INFO1_MSDU_LENGTH,
+			 __le32_to_cpu(desc->msdu_start.info1));
+}
+
+static u8 ath11k_dp_rx_h_msdu_start_sgi(void *buf)
+{
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
 
-static u8 ath11k_dp_rx_h_msdu_start_sgi(struct hal_rx_desc *desc)
+	return FIELD_GET(RX_MSDU_START_INFO3_SGI,
+			 __le32_to_cpu(desc->msdu_start.info3));
+}
+static u8 ath11k_dp_rx_h_msdu_start_sgi_qca6490(void *buf)
 {
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
+
 	return FIELD_GET(RX_MSDU_START_INFO3_SGI,
 			 __le32_to_cpu(desc->msdu_start.info3));
 }
 
-static u8 ath11k_dp_rx_h_msdu_start_rate_mcs(struct hal_rx_desc *desc)
+static u8 ath11k_dp_rx_h_msdu_start_rate_mcs(void *buf)
+{
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
+
+	return FIELD_GET(RX_MSDU_START_INFO3_RATE_MCS,
+			 __le32_to_cpu(desc->msdu_start.info3));
+}
+static u8 ath11k_dp_rx_h_msdu_start_rate_mcs_qca6490(void *buf)
 {
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
+
 	return FIELD_GET(RX_MSDU_START_INFO3_RATE_MCS,
 			 __le32_to_cpu(desc->msdu_start.info3));
 }
 
-static u8 ath11k_dp_rx_h_msdu_start_rx_bw(struct hal_rx_desc *desc)
+static u8 ath11k_dp_rx_h_msdu_start_rx_bw(void *buf)
+{
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
+
+	return FIELD_GET(RX_MSDU_START_INFO3_RECV_BW,
+			 __le32_to_cpu(desc->msdu_start.info3));
+}
+static u8 ath11k_dp_rx_h_msdu_start_rx_bw_qca6490(void *buf)
 {
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
+
 	return FIELD_GET(RX_MSDU_START_INFO3_RECV_BW,
 			 __le32_to_cpu(desc->msdu_start.info3));
 }
 
-static u32 ath11k_dp_rx_h_msdu_start_freq(struct hal_rx_desc *desc)
+static u32 ath11k_dp_rx_h_msdu_start_freq(void *buf)
 {
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
+
 	return __le32_to_cpu(desc->msdu_start.phy_meta_data);
 }
+static u32 ath11k_dp_rx_h_msdu_start_freq_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
 
-static u8 ath11k_dp_rx_h_msdu_start_pkt_type(struct hal_rx_desc *desc)
+	return __le32_to_cpu(desc->msdu_start.phy_meta_data);
+}
+
+static u8 ath11k_dp_rx_h_msdu_start_pkt_type(void *buf)
 {
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
+
+	return FIELD_GET(RX_MSDU_START_INFO3_PKT_TYPE,
+			 __le32_to_cpu(desc->msdu_start.info3));
+}
+static u8 ath11k_dp_rx_h_msdu_start_pkt_type_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
+
 	return FIELD_GET(RX_MSDU_START_INFO3_PKT_TYPE,
 			 __le32_to_cpu(desc->msdu_start.info3));
 }
 
-static u8 ath11k_dp_rx_h_msdu_start_nss(struct hal_rx_desc *desc)
+static u8 ath11k_dp_rx_h_msdu_start_nss(void *buf)
 {
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
+
 	u8 mimo_ss_bitmap = FIELD_GET(RX_MSDU_START_INFO3_MIMO_SS_BITMAP,
 				      __le32_to_cpu(desc->msdu_start.info3));
 
 	return hweight8(mimo_ss_bitmap);
 }
+static u8 ath11k_dp_rx_h_msdu_start_nss_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
 
-static u8 ath11k_dp_rx_h_mpdu_start_tid(struct hal_rx_desc *desc)
+	u8 mimo_ss_bitmap = FIELD_GET(RX_MSDU_START_INFO3_MIMO_SS_BITMAP,
+				      __le32_to_cpu(desc->msdu_start.info3));
+
+	return hweight8(mimo_ss_bitmap);
+}
+
+static u8 ath11k_dp_rx_h_mpdu_start_tid(void *buf)
 {
+#ifdef RX_MPDU_START_INFO2_TID
+#undef RX_MPDU_START_INFO2_TID
+#endif
+#define RX_MPDU_START_INFO2_TID	GENMASK(17, 14)
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
+
 	return FIELD_GET(RX_MPDU_START_INFO2_TID,
 			 __le32_to_cpu(desc->mpdu_start.info2));
 }
+static u8 ath11k_dp_rx_h_mpdu_start_tid_qca6490(void *buf)
+{
+#ifdef RX_MPDU_START_INFO2_TID
+#undef RX_MPDU_START_INFO2_TID
+#endif
+#define RX_MPDU_START_INFO2_TID	GENMASK(18, 15)
+
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
 
-static u16 ath11k_dp_rx_h_mpdu_start_peer_id(struct hal_rx_desc *desc)
+	return FIELD_GET(RX_MPDU_START_INFO2_TID,
+			 __le32_to_cpu(desc->mpdu_start.info2));
+}
+
+static u16 ath11k_dp_rx_h_mpdu_start_peer_id(void *buf)
+{
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
+
+	return __le16_to_cpu(desc->mpdu_start.sw_peer_id);
+}
+static u16 ath11k_dp_rx_h_mpdu_start_peer_id_qca6490(void *buf)
 {
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
+
 	return __le16_to_cpu(desc->mpdu_start.sw_peer_id);
 }
 
-static u8 ath11k_dp_rx_h_msdu_end_l3pad(struct hal_rx_desc *desc)
+static u8 ath11k_dp_rx_h_msdu_end_l3pad(void *buf)
 {
+	struct hal_rx_desc *desc = 
+			(struct hal_rx_desc*)buf;
+
 	return FIELD_GET(RX_MSDU_END_INFO2_L3_HDR_PADDING,
 			 __le32_to_cpu(desc->msdu_end.info2));
 }
+static u8 ath11k_dp_rx_h_msdu_end_l3pad_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = 
+			(struct hal_rx_desc_qca6490*)buf;
+
+	return FIELD_GET(RX_MSDU_END_INFO2_L3_HDR_PADDING,
+			 __le32_to_cpu(desc->msdu_end.info2));
+}
+
+static bool ath11k_dp_rx_h_msdu_end_first_msdu(void *buf)
+{
+#ifdef RX_MSDU_END_INFO2_FIRST_MSDU
+#undef RX_MSDU_END_INFO2_FIRST_MSDU
+#endif	
+#define RX_MSDU_END_INFO2_FIRST_MSDU BIT(14)
+	struct hal_rx_desc *desc = 
+		(struct hal_rx_desc *)buf;
+	return !!FIELD_GET(RX_MSDU_END_INFO2_FIRST_MSDU,
+			__le32_to_cpu(desc->msdu_end.info2));
+}
 
-static bool ath11k_dp_rx_h_msdu_end_first_msdu(struct hal_rx_desc *desc)
+static bool ath11k_dp_rx_h_msdu_end_first_msdu_qca6490(void *buf)
 {
+#ifdef RX_MSDU_END_INFO2_FIRST_MSDU
+#undef RX_MSDU_END_INFO2_FIRST_MSDU
+#endif	
+#define RX_MSDU_END_INFO2_FIRST_MSDU BIT(28)
+	struct hal_rx_desc_qca6490 *desc = 
+		(struct hal_rx_desc_qca6490 *)buf;
 	return !!FIELD_GET(RX_MSDU_END_INFO2_FIRST_MSDU,
-			   __le32_to_cpu(desc->msdu_end.info2));
+			__le32_to_cpu(desc->msdu_end.info2));
+}
+
+static bool ath11k_dp_rx_h_msdu_end_last_msdu(void *buf)
+{
+#ifdef RX_MSDU_END_INFO2_LAST_MSDU
+#undef RX_MSDU_END_INFO2_LAST_MSDU
+#endif	
+#define RX_MSDU_END_INFO2_LAST_MSDU BIT(15)
+	struct hal_rx_desc *desc= 
+		(struct hal_rx_desc *)buf;
+	return !!FIELD_GET(RX_MSDU_END_INFO2_LAST_MSDU,
+			__le32_to_cpu(desc->msdu_end.info2));
 }
 
-static bool ath11k_dp_rx_h_msdu_end_last_msdu(struct hal_rx_desc *desc)
+static bool ath11k_dp_rx_h_msdu_end_last_msdu_qca6490(void *buf)
 {
+#ifdef RX_MSDU_END_INFO2_LAST_MSDU
+#undef RX_MSDU_END_INFO2_LAST_MSDU
+#endif	
+#define RX_MSDU_END_INFO2_LAST_MSDU BIT(29)
+	struct hal_rx_desc_qca6490 *desc = 
+		(struct hal_rx_desc_qca6490 *)buf;
 	return !!FIELD_GET(RX_MSDU_END_INFO2_LAST_MSDU,
-			   __le32_to_cpu(desc->msdu_end.info2));
+			__le32_to_cpu(desc->msdu_end.info2));
 }
 
-static void ath11k_dp_rx_desc_end_tlv_copy(struct hal_rx_desc *fdesc,
-					   struct hal_rx_desc *ldesc)
+static void ath11k_dp_rxdesc_end_tlv_copy(void *fbuf,
+					   void *lbuf)
 {
+	struct hal_rx_desc *fdesc = (struct hal_rx_desc *)fbuf;
+	struct hal_rx_desc *ldesc = (struct hal_rx_desc *)lbuf;
 	memcpy((u8 *)&fdesc->msdu_end, (u8 *)&ldesc->msdu_end,
 	       sizeof(struct rx_msdu_end));
 	memcpy((u8 *)&fdesc->attention, (u8 *)&ldesc->attention,
@@ -215,49 +544,110 @@ static void ath11k_dp_rx_desc_end_tlv_copy(struct hal_rx_desc *fdesc,
 	memcpy((u8 *)&fdesc->mpdu_end, (u8 *)&ldesc->mpdu_end,
 	       sizeof(struct rx_mpdu_end));
 }
+static void ath11k_dp_rxdesc_end_tlv_copy_qca6490(void *fbuf,
+					  void *lbuf)
+{
+	struct hal_rx_desc_qca6490 *fdesc = (struct hal_rx_desc_qca6490 *)fbuf;
+	struct hal_rx_desc_qca6490 *ldesc = (struct hal_rx_desc_qca6490 *)lbuf;
+	memcpy((u8 *)&fdesc->msdu_end, (u8 *)&ldesc->msdu_end,
+		  sizeof(struct rx_msdu_end_qca6490));
+	memcpy((u8 *)&fdesc->attention, (u8 *)&ldesc->attention,
+		  sizeof(struct rx_attention));
+	memcpy((u8 *)&fdesc->mpdu_end, (u8 *)&ldesc->mpdu_end,
+		  sizeof(struct rx_mpdu_end));
+}
 
-static u32 ath11k_dp_rxdesc_get_mpdulen_err(struct hal_rx_desc *rx_desc)
+static u32 ath11k_dp_rxdesc_get_mpdulen_err(void *buf)
 {
-	struct rx_attention *rx_attn;
+	struct hal_rx_desc *desc = 
+		  (struct hal_rx_desc*)buf;
 
-	rx_attn = &rx_desc->attention;
+	return FIELD_GET(RX_ATTENTION_INFO1_MPDU_LEN_ERR,
+			 __le32_to_cpu(desc->attention.info1));
+}
+static u32 ath11k_dp_rxdesc_get_mpdulen_err_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = 
+		  (struct hal_rx_desc_qca6490*)buf;
 
 	return FIELD_GET(RX_ATTENTION_INFO1_MPDU_LEN_ERR,
-			 __le32_to_cpu(rx_attn->info1));
+			 __le32_to_cpu(desc->attention.info1));
 }
 
-static u32 ath11k_dp_rxdesc_get_decap_format(struct hal_rx_desc *rx_desc)
+static u32 ath11k_dp_rxdesc_get_decap_format(void *buf)
 {
-	struct rx_msdu_start *rx_msdu_start;
+	struct hal_rx_desc *desc = 
+		  (struct hal_rx_desc*)buf;
 
-	rx_msdu_start = &rx_desc->msdu_start;
+	return FIELD_GET(RX_MSDU_START_INFO2_DECAP_FORMAT,
+			 __le32_to_cpu(desc->msdu_start.info2));
+}
+static u32 ath11k_dp_rxdesc_get_decap_format_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = 
+		  (struct hal_rx_desc_qca6490*)buf;
 
 	return FIELD_GET(RX_MSDU_START_INFO2_DECAP_FORMAT,
-			 __le32_to_cpu(rx_msdu_start->info2));
+			 __le32_to_cpu(desc->msdu_start.info2));
 }
 
-static u8 *ath11k_dp_rxdesc_get_80211hdr(struct hal_rx_desc *rx_desc)
+static u8 *ath11k_dp_rxdesc_get_80211hdr(void *buf)
 {
 	u8 *rx_pkt_hdr;
+	struct hal_rx_desc *desc = 
+		  (struct hal_rx_desc*)buf;
 
-	rx_pkt_hdr = &rx_desc->msdu_payload[0];
+	rx_pkt_hdr = &desc->msdu_payload[0];
 
 	return rx_pkt_hdr;
 }
+static u8 *ath11k_dp_rxdesc_get_80211hdr_qca6490(void *buf)
+{
+	u8 *rx_pkt_hdr;
+	struct hal_rx_desc_qca6490 *desc = 
+		  (struct hal_rx_desc_qca6490*)buf;
+
+	rx_pkt_hdr = &desc->msdu_payload[0];
+
+	return rx_pkt_hdr;
+}
+
+static bool ath11k_dp_rxdesc_mpdu_valid(void *buf)
+{
+	u32 tlv_tag;
+	struct hal_rx_desc *desc = 
+		  (struct hal_rx_desc*)buf;
+
+	tlv_tag = FIELD_GET(HAL_TLV_HDR_TAG,
+			    __le32_to_cpu(desc->mpdu_start_tag));
 
-static bool ath11k_dp_rxdesc_mpdu_valid(struct hal_rx_desc *rx_desc)
+	return tlv_tag == HAL_RX_MPDU_START;
+}
+static bool ath11k_dp_rxdesc_mpdu_valid_qca6490(void *buf)
 {
 	u32 tlv_tag;
+	struct hal_rx_desc_qca6490 *desc = 
+		  (struct hal_rx_desc_qca6490*)buf;
 
 	tlv_tag = FIELD_GET(HAL_TLV_HDR_TAG,
-			    __le32_to_cpu(rx_desc->mpdu_start_tag));
+			    __le32_to_cpu(desc->mpdu_start_tag));
 
 	return tlv_tag == HAL_RX_MPDU_START;
 }
 
-static u32 ath11k_dp_rxdesc_get_ppduid(struct hal_rx_desc *rx_desc)
+static u32 ath11k_dp_rxdesc_get_ppduid(void *buf)
 {
-	return __le16_to_cpu(rx_desc->mpdu_start.phy_ppdu_id);
+	struct hal_rx_desc *desc = 
+		  (struct hal_rx_desc*)buf;
+
+	return __le16_to_cpu(desc->mpdu_start.phy_ppdu_id);
+}
+static u32 ath11k_dp_rxdesc_get_ppduid_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = 
+		  (struct hal_rx_desc_qca6490*)buf;
+
+	return __le16_to_cpu(desc->mpdu_start.phy_ppdu_id);
 }
 
 void ath11k_dp_service_mon_ring(struct timer_list *t)
@@ -3194,6 +3584,16 @@ static int ath11k_dp_rx_h_defrag(struct ath11k *ar,
 	*defrag_skb = first_frag;
 	return 0;
 }
+static u32* ath11k_dp_rx_h_msdu_start_msdu_len_offset(void *buf)
+{
+	struct hal_rx_desc *desc = (struct hal_rx_desc *)buf;
+	return (u32 *)&desc->msdu_start;
+}
+static u32* ath11k_dp_rx_h_msdu_start_msdu_len_offset_qca6490(void *buf)
+{
+	struct hal_rx_desc_qca6490 *desc = (struct hal_rx_desc_qca6490 *)buf;
+	return (u32 *)&desc->msdu_start;
+}
 
 static int ath11k_dp_rx_h_defrag_reo_reinject(struct ath11k *ar, struct dp_rx_tid *rx_tid,
 					      struct sk_buff *defrag_skb)
@@ -5017,3 +5417,103 @@ int ath11k_dp_rx_pdev_mon_detach(struct ath11k *ar)
 	ath11k_dp_mon_link_free(ar);
 	return 0;
 }
+
+static size_t ath11k_dp_rxdesc_get_desc_size(void)
+{
+	return sizeof(struct hal_rx_desc);
+}
+static size_t ath11k_dp_rxdesc_get_desc_size_qca6490(void)
+{
+	return sizeof(struct hal_rx_desc_qca6490);
+}
+static struct dp_rx_ops ath11k_dp_rx_ops = {
+	.h_80211_hdr = ath11k_dp_rx_h_80211_hdr,
+	.h_mpdu_start_enctype = ath11k_dp_rx_h_mpdu_start_enctype,
+	.h_msdu_start_decap_type = ath11k_dp_rx_h_msdu_start_decap_type,
+	.h_msdu_start_mesh_ctl_present = ath11k_dp_rx_h_msdu_start_mesh_ctl_present,
+	.h_mpdu_start_seq_ctrl_valid = ath11k_dp_rx_h_mpdu_start_seq_ctrl_valid,
+	.h_mpdu_start_fc_valid = ath11k_dp_rx_h_mpdu_start_fc_valid,
+	.h_mpdu_start_more_frags = ath11k_dp_rx_h_mpdu_start_more_frags,
+	.h_mpdu_start_frag_no = ath11k_dp_rx_h_mpdu_start_frag_no,
+	.h_mpdu_start_seq_no = ath11k_dp_rx_h_mpdu_start_seq_no,
+	.h_attn_msdu_done = ath11k_dp_rx_h_attn_msdu_done,
+	.h_attn_l4_cksum_fail = ath11k_dp_rx_h_attn_l4_cksum_fail,
+	.h_attn_ip_cksum_fail = ath11k_dp_rx_h_attn_ip_cksum_fail,
+	.h_attn_is_decrypted = ath11k_dp_rx_h_attn_is_decrypted,
+	.h_attn_mpdu_err = ath11k_dp_rx_h_attn_mpdu_err,
+	.h_msdu_start_msdu_len = ath11k_dp_rx_h_msdu_start_msdu_len,
+	.h_msdu_start_sgi = ath11k_dp_rx_h_msdu_start_sgi,
+	.h_msdu_start_rate_mcs = ath11k_dp_rx_h_msdu_start_rate_mcs,
+	.h_msdu_start_rx_bw = ath11k_dp_rx_h_msdu_start_rx_bw,
+	.h_msdu_start_freq = ath11k_dp_rx_h_msdu_start_freq,
+	.h_msdu_start_pkt_type = ath11k_dp_rx_h_msdu_start_pkt_type,
+	.h_msdu_start_nss = ath11k_dp_rx_h_msdu_start_nss,
+	.h_mpdu_start_tid = ath11k_dp_rx_h_mpdu_start_tid,
+	.h_mpdu_start_peer_id = ath11k_dp_rx_h_mpdu_start_peer_id,
+	.h_msdu_end_l3pad = ath11k_dp_rx_h_msdu_end_l3pad,
+	.h_msdu_end_first_msdu = ath11k_dp_rx_h_msdu_end_first_msdu,
+	.h_msdu_end_last_msdu = ath11k_dp_rx_h_msdu_end_last_msdu,
+	.rxdesc_end_tlv_copy = ath11k_dp_rxdesc_end_tlv_copy,
+	.rxdesc_get_mpdulen_err = ath11k_dp_rxdesc_get_mpdulen_err,
+	.rxdesc_get_decap_format = ath11k_dp_rxdesc_get_decap_format,
+	.rxdesc_get_80211hdr = ath11k_dp_rxdesc_get_80211hdr,
+	.rxdesc_mpdu_valid = ath11k_dp_rxdesc_mpdu_valid,
+	.rxdesc_get_ppduid = ath11k_dp_rxdesc_get_ppduid,
+	.h_msdu_start_msdu_len_offset = ath11k_dp_rx_h_msdu_start_msdu_len_offset,
+	.rxdesc_get_desc_size = ath11k_dp_rxdesc_get_desc_size,
+};
+	
+static struct dp_rx_ops ath11k_dp_rx_ops_qca6490 = {
+	.h_80211_hdr = ath11k_dp_rx_h_80211_hdr_qca6490,
+	.h_mpdu_start_enctype = ath11k_dp_rx_h_mpdu_start_enctype_qca6490,
+	.h_msdu_start_decap_type = ath11k_dp_rx_h_msdu_start_decap_type_qca6490,
+	.h_msdu_start_mesh_ctl_present = ath11k_dp_rx_h_msdu_start_mesh_ctl_present_qca6490,
+	.h_mpdu_start_seq_ctrl_valid = ath11k_dp_rx_h_mpdu_start_seq_ctrl_valid_qca6490,
+	.h_mpdu_start_fc_valid = ath11k_dp_rx_h_mpdu_start_fc_valid_qca6490,
+	.h_mpdu_start_more_frags = ath11k_dp_rx_h_mpdu_start_more_frags_qca6490,
+	.h_mpdu_start_frag_no = ath11k_dp_rx_h_mpdu_start_frag_no_qca6490,
+	.h_mpdu_start_seq_no = ath11k_dp_rx_h_mpdu_start_seq_no_qca6490,
+	.h_attn_msdu_done = ath11k_dp_rx_h_attn_msdu_done_qca6490,
+	.h_attn_l4_cksum_fail = ath11k_dp_rx_h_attn_l4_cksum_fail_qca6490,
+	.h_attn_ip_cksum_fail = ath11k_dp_rx_h_attn_ip_cksum_fail_qca6490,
+	.h_attn_is_decrypted = ath11k_dp_rx_h_attn_is_decrypted_qca6490,
+	.h_attn_mpdu_err = ath11k_dp_rx_h_attn_mpdu_err_qca6490,
+	.h_msdu_start_msdu_len = ath11k_dp_rx_h_msdu_start_msdu_len_qca6490,
+	.h_msdu_start_sgi = ath11k_dp_rx_h_msdu_start_sgi_qca6490,
+	.h_msdu_start_rate_mcs = ath11k_dp_rx_h_msdu_start_rate_mcs_qca6490,
+	.h_msdu_start_rx_bw = ath11k_dp_rx_h_msdu_start_rx_bw_qca6490,
+	.h_msdu_start_freq = ath11k_dp_rx_h_msdu_start_freq_qca6490,
+	.h_msdu_start_pkt_type = ath11k_dp_rx_h_msdu_start_pkt_type_qca6490,
+	.h_msdu_start_nss = ath11k_dp_rx_h_msdu_start_nss_qca6490,
+	.h_mpdu_start_tid = ath11k_dp_rx_h_mpdu_start_tid_qca6490,
+	.h_mpdu_start_peer_id = ath11k_dp_rx_h_mpdu_start_peer_id_qca6490,
+	.h_msdu_end_l3pad = ath11k_dp_rx_h_msdu_end_l3pad_qca6490,
+	.h_msdu_end_first_msdu = ath11k_dp_rx_h_msdu_end_first_msdu_qca6490,
+	.h_msdu_end_last_msdu = ath11k_dp_rx_h_msdu_end_last_msdu_qca6490,
+	.rxdesc_end_tlv_copy = ath11k_dp_rxdesc_end_tlv_copy_qca6490,
+	.rxdesc_get_mpdulen_err = ath11k_dp_rxdesc_get_mpdulen_err_qca6490,
+	.rxdesc_get_decap_format = ath11k_dp_rxdesc_get_decap_format_qca6490,
+	.rxdesc_get_80211hdr = ath11k_dp_rxdesc_get_80211hdr_qca6490,
+	.rxdesc_mpdu_valid = ath11k_dp_rxdesc_mpdu_valid_qca6490,
+	.rxdesc_get_ppduid = ath11k_dp_rxdesc_get_ppduid_qca6490,
+	.h_msdu_start_msdu_len_offset = ath11k_dp_rx_h_msdu_start_msdu_len_offset_qca6490,
+	.rxdesc_get_desc_size = ath11k_dp_rxdesc_get_desc_size_qca6490,
+};
+	
+int ath11k_dp_rx_ops_init(struct ath11k_base *ab){
+	switch(ab->hw_rev){
+	case ATH11K_HW_IPQ8074:
+	case ATH11K_HW_QCA6290:
+	case ATH11K_HW_QCA6390:
+		ab->dp.rx_ops = &ath11k_dp_rx_ops;
+		break;
+	case ATH11K_HW_QCA6490:
+		ab->dp.rx_ops = &ath11k_dp_rx_ops_qca6490
+		;break;
+	default:
+		ath11k_err(ab, "Unknown hw rev: 0x%x\n", ab->hw_rev);
+		return -ENOTSUPP;
+	}
+	return 0;
+}
+
diff --git a/drivers/net/wireless/ath/ath11k/dp_rx.h b/drivers/net/wireless/ath/ath11k/dp_rx.h
index 1f46d0a..835c7a0 100644
--- a/drivers/net/wireless/ath/ath11k/dp_rx.h
+++ b/drivers/net/wireless/ath/ath11k/dp_rx.h
@@ -41,6 +41,43 @@ struct ath11k_dp_rfc1042_hdr {
 	__be16 snap_type;
 } __packed;
 
+struct dp_rx_ops {
+	u8* (*h_80211_hdr)(void *);
+	enum hal_encrypt_type (*h_mpdu_start_enctype)(void *);
+	u8 (*h_msdu_start_decap_type)(void *);
+	u8 (*h_msdu_start_mesh_ctl_present)(void *);
+	bool (*h_mpdu_start_seq_ctrl_valid)(void *);
+	bool (*h_mpdu_start_fc_valid)(void *);
+	bool (*h_mpdu_start_more_frags)(struct sk_buff *);
+	u16 (*h_mpdu_start_frag_no)(struct sk_buff *);
+	u16 (*h_mpdu_start_seq_no)(void *);
+	bool (*h_attn_msdu_done)(void *);
+	bool (*h_attn_l4_cksum_fail)(void *);
+	bool (*h_attn_ip_cksum_fail)(void *);
+	bool (*h_attn_is_decrypted)(void *);
+	u32 (*h_attn_mpdu_err)(void *);
+	u16 (*h_msdu_start_msdu_len)(void *);
+	u8 (*h_msdu_start_sgi)(void *);
+	u8 (*h_msdu_start_rate_mcs)(void *);
+	u8 (*h_msdu_start_rx_bw)(void *);
+	u32 (*h_msdu_start_freq)(void *);
+	u8 (*h_msdu_start_pkt_type)(void *);
+	u8 (*h_msdu_start_nss)(void *);
+	u8 (*h_mpdu_start_tid)(void *);
+	u16 (*h_mpdu_start_peer_id)(void *);
+	u8 (*h_msdu_end_l3pad)(void *);
+	bool (*h_msdu_end_first_msdu)(void*);
+	bool (*h_msdu_end_last_msdu)(void*);
+	void (*rxdesc_end_tlv_copy)(void *fbuf, void *lbuf);
+	u32 (*rxdesc_get_mpdulen_err)(void *);
+	u32 (*rxdesc_get_decap_format)(void *);
+	u8* (*rxdesc_get_80211hdr)(void *);
+	bool (*rxdesc_mpdu_valid)(void *);
+	u32 (*rxdesc_get_ppduid)(void *);
+	u32* (*h_msdu_start_msdu_len_offset)(void *);
+	size_t (*rxdesc_get_desc_size)(void);
+}__packed;
+
 int ath11k_dp_rx_ampdu_start(struct ath11k *ar,
 			     struct ieee80211_ampdu_params *params);
 int ath11k_dp_rx_ampdu_stop(struct ath11k *ar,
@@ -93,4 +130,5 @@ int ath11k_dp_rx_mon_status_bufs_replenish(struct ath11k_base *ab, int mac_id,
 int ath11k_dp_rx_pdev_mon_attach(struct ath11k *ar);
 int ath11k_peer_rx_frag_setup(struct ath11k *ar, const u8 *peer_mac, int vdev_id);
 int ath11k_dp_purge_mon_ring(struct ath11k_base *ab);
+int ath11k_dp_rx_ops_init(struct ath11k_base *ab);
 #endif /* ATH11K_DP_RX_H */
-- 
1.9.1

