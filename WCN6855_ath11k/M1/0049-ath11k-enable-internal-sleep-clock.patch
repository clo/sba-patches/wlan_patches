From 1547cfbede4110e344589773edcf27cdaefbad22 Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Tue, 3 Dec 2019 16:23:08 +0800
Subject: [PATCH 049/124] ath11k: enable internal sleep clock

On x86 and third-party platform, host need explicitly tell firmware to use
internal sleep clock. Some QCA6390 modules have OTP burnt with external
sleep clock selected, and these moduesl can't work expectedly unless
firmware selects internal sleep clock.

Add a field to hw_params to support this difference.

Tested-on: QCA6390 WLAN.HST.1.0.1-01230-QCAHSTSWPLZ_V2_TO_X86-1

Change-Id: Idda12cf262a00e2e63ab26612fc87747c2ce1d89
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/core.c |  2 ++
 drivers/net/wireless/ath/ath11k/hw.h   |  1 +
 drivers/net/wireless/ath/ath11k/qmi.c  | 13 +++++++++++++
 3 files changed, 16 insertions(+)

diff --git a/drivers/net/wireless/ath/ath11k/core.c b/drivers/net/wireless/ath/ath11k/core.c
index c6e8015..008ec0c 100644
--- a/drivers/net/wireless/ath/ath11k/core.c
+++ b/drivers/net/wireless/ath/ath11k/core.c
@@ -27,10 +27,12 @@ static const struct ath11k_hw_params ath11k_hw_params_list[] = {
 			.board_size = IPQ8074_MAX_BOARD_DATA_SZ,
 			.cal_size =  IPQ8074_MAX_CAL_DATA_SZ,
 		},
+		.internal_sleep_clock = false,
 	},
 	{
 		.name = "qca6390",
 		.dev_id = ATH11K_HW_QCA6390,
+		.internal_sleep_clock = true,
 	}
 };
 
diff --git a/drivers/net/wireless/ath/ath11k/hw.h b/drivers/net/wireless/ath/ath11k/hw.h
index aee8c4b..9e85579 100644
--- a/drivers/net/wireless/ath/ath11k/hw.h
+++ b/drivers/net/wireless/ath/ath11k/hw.h
@@ -112,6 +112,7 @@ struct ath11k_hw_params {
 		size_t board_size;
 		size_t cal_size;
 	} fw;
+	bool internal_sleep_clock;
 };
 
 struct ath11k_fw_ie {
diff --git a/drivers/net/wireless/ath/ath11k/qmi.c b/drivers/net/wireless/ath/ath11k/qmi.c
index d358a51..0098e3d 100644
--- a/drivers/net/wireless/ath/ath11k/qmi.c
+++ b/drivers/net/wireless/ath/ath11k/qmi.c
@@ -10,6 +10,8 @@
 #include <linux/firmware.h>
 
 #define ATH11K_DEFAULT_M3_FILE_NAME	"m3.bin"
+#define SLEEP_CLOCK_SELECT_INTERNAL_BIT 0x02
+#define HOST_CSTATE_BIT 0x04
 
 static struct qmi_elem_info qmi_wlanfw_host_cap_req_msg_v01_ei[] = {
 	{
@@ -1533,6 +1535,17 @@ static int ath11k_qmi_host_cap_send(struct ath11k_base *ab)
 	req.cal_done_valid = 1;
 	req.cal_done = ab->qmi.cal_done;
 
+	if (ab->hw_params.internal_sleep_clock) {
+		req.nm_modem_valid = 1;
+		req.nm_modem |= HOST_CSTATE_BIT;
+		/* notify firmware about the sleep clock selection,
+		 * discussed with firmware, nm_modem_bit[1] used
+		 * for this purpose. Driver on third-party platform
+		 * should select internal sleep clock.
+		 */
+		req.nm_modem |= SLEEP_CLOCK_SELECT_INTERNAL_BIT;
+	}
+
 	ret = qmi_txn_init(&ab->qmi.handle, &txn,
 			   qmi_wlanfw_host_cap_resp_msg_v01_ei, &resp);
 	if (ret < 0)
-- 
2.7.4

