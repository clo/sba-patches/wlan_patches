From eda5060b3dcdde63ad2537ad34ca5ff125fd00cd Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Thu, 26 Mar 2020 15:22:34 +0800
Subject: [PATCH 090/124] ath11k: Start a timer to update HP for CE pipe 4

For QCA6390, Start a timer to update CE pipe 4 ring HP when shadow
register is enabled. Its' to avoid that HP isn't updated to target
register.

Tested-on: QCA6390 WLAN.HST.1.0.1-01230-QCAHSTSWPLZ_V2_TO_X86-1

Change-Id: Ida358cb839450b85e227c89220828bb1edc5d731
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/ce.c  | 39 ++++++++++++++++++++++++++++++++++-
 drivers/net/wireless/ath/ath11k/ce.h  |  4 ++++
 drivers/net/wireless/ath/ath11k/wow.c |  1 +
 3 files changed, 43 insertions(+), 1 deletion(-)

diff --git a/drivers/net/wireless/ath/ath11k/ce.c b/drivers/net/wireless/ath/ath11k/ce.c
index add023e..37dfe18 100644
--- a/drivers/net/wireless/ath/ath11k/ce.c
+++ b/drivers/net/wireless/ath/ath11k/ce.c
@@ -189,6 +189,27 @@ const struct ce_attr host_ce_config_wlan_qca6x90[] = {
 
 };
 
+static bool ath11k_ce_need_shadow_fix(int ce_id)
+{
+	/* only ce4 needs shadow workaroud*/
+	if (ce_id == 4)
+		return true;
+	return false;
+}
+
+void ath11k_ce_stop_shadow_timers(struct ath11k_base *ab)
+{
+	int i;
+
+	if (!ab->hw_params.shadow_support_fix)
+		return;
+
+	for (i = 0; i < CE_COUNT; i++) {
+		if (ath11k_ce_need_shadow_fix(i))
+			ath11k_shadow_stop_timer(&ab->ce.hp_timer[i]);
+	}
+}
+
 static int ath11k_ce_rx_buf_enqueue_pipe(struct ath11k_ce_pipe *pipe,
 					 struct sk_buff *skb, dma_addr_t paddr)
 {
@@ -504,9 +525,14 @@ static int ath11k_ce_init_ring(struct ath11k_base *ab,
 			    ret, ce_id);
 		return ret;
 	}
-
 	ce_ring->hal_ring_id = ret;
 
+	if (ab->hw_params.shadow_support_fix &&
+	    ath11k_ce_need_shadow_fix(ce_id))
+		ath11k_shadow_init_timer(ab, &ab->ce.hp_timer[ce_id],
+					 ATH11K_SHADOW_CTRL_TIMER_INTERVAL,
+					 ce_ring->hal_ring_id);
+
 	return 0;
 }
 
@@ -679,6 +705,10 @@ int ath11k_ce_send(struct ath11k_base *ab, struct sk_buff *skb, u8 pipe_id,
 
 	ath11k_hal_srng_access_end(ab, srng);
 
+	if (ab->hw_params.shadow_support_fix &&
+	    ath11k_ce_need_shadow_fix(pipe_id))
+		ath11k_shadow_start_timer(ab, srng, &ab->ce.hp_timer[pipe_id]);
+
 	spin_unlock_bh(&srng->lock);
 
 	spin_unlock_bh(&ab->ce.ce_lock);
@@ -779,6 +809,8 @@ void ath11k_ce_cleanup_pipes(struct ath11k_base *ab)
 	struct ath11k_ce_pipe *pipe;
 	int pipe_num;
 
+	ath11k_ce_stop_shadow_timers(ab);
+
 	for (pipe_num = 0; pipe_num < CE_COUNT; pipe_num++) {
 		pipe = &ab->ce.ce_pipe[pipe_num];
 		ath11k_ce_rx_pipe_cleanup(pipe);
@@ -889,6 +921,11 @@ void ath11k_ce_free_pipes(struct ath11k_base *ab)
 	for (i = 0; i < CE_COUNT; i++) {
 		pipe = &ab->ce.ce_pipe[i];
 
+		if (ab->hw_params.shadow_support_fix &&
+		    ath11k_ce_need_shadow_fix(i)) {
+			ath11k_shadow_stop_timer(&ab->ce.hp_timer[i]);
+		}
+
 		if (pipe->src_ring) {
 			desc_sz = ath11k_hal_ce_get_desc_size(HAL_CE_DESC_SRC);
 			dma_free_coherent(ab->dev,
diff --git a/drivers/net/wireless/ath/ath11k/ce.h b/drivers/net/wireless/ath/ath11k/ce.h
index d50cc40..85da182 100644
--- a/drivers/net/wireless/ath/ath11k/ce.h
+++ b/drivers/net/wireless/ath/ath11k/ce.h
@@ -6,6 +6,8 @@
 #ifndef ATH11K_CE_H
 #define ATH11K_CE_H
 
+#include "core.h"
+
 #define CE_COUNT (ab->ce.ce_count)
 #define MAX_CE_COUNT 12
 
@@ -171,6 +173,7 @@ struct ath11k_ce {
 	spinlock_t ce_lock;
 	const struct ce_attr *host_ce_config;
 	u32	ce_count;
+	struct ath11k_hp_update_timer hp_timer[MAX_CE_COUNT];
 };
 
 void ath11k_ce_cleanup_pipes(struct ath11k_base *ab);
@@ -189,4 +192,5 @@ int ath11k_ce_map_service_to_pipe(struct ath11k_base *ab, u16 service_id,
 int ath11k_ce_attr_attach(struct ath11k_base *ab);
 void ath11k_ce_get_shadow_config(struct ath11k_base *ab,
 				 u32 **shadow_cfg, u32 *shadow_cfg_len);
+void ath11k_ce_stop_shadow_timers(struct ath11k_base *ab);
 #endif
diff --git a/drivers/net/wireless/ath/ath11k/wow.c b/drivers/net/wireless/ath/ath11k/wow.c
index 482a335..6b6bcf2 100644
--- a/drivers/net/wireless/ath/ath11k/wow.c
+++ b/drivers/net/wireless/ath/ath11k/wow.c
@@ -606,6 +606,7 @@ int ath11k_wow_op_suspend(struct ieee80211_hw *hw,
 	}
 
 	/* stop shadow hp update related timers */
+	ath11k_ce_stop_shadow_timers(ar->ab);
 	ath11k_dp_stop_shadow_timers(ar->ab);
 
 	goto exit;
-- 
2.7.4

