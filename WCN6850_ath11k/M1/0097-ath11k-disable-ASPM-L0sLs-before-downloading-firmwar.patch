From 8f20c452295633cadb10275e4f21864b3884d966 Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Wed, 20 May 2020 18:11:34 +0800
Subject: [PATCH 097/124] ath11k: disable ASPM L0sLs before downloading
 firmware

Sometimes target doesn't switch to amss state as device enters
L1ss state, so disable L0sL1s during firmware downloading.
Driver recovers the ASPM to default value in start callback
or powerdown callback.

Tested-on: QCA6390 WLAN.HST.1.0.1-01230-QCAHSTSWPLZ_V2_TO_X86-1

Change-Id: I007695340cd62dea3ee4a8191b8eb878be489da6
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/pci.c | 17 +++++++++++++++++
 drivers/net/wireless/ath/ath11k/pci.h |  2 ++
 2 files changed, 19 insertions(+)

diff --git a/drivers/net/wireless/ath/ath11k/pci.c b/drivers/net/wireless/ath/ath11k/pci.c
index 38ecfe5..50946b6 100644
--- a/drivers/net/wireless/ath/ath11k/pci.c
+++ b/drivers/net/wireless/ath/ath11k/pci.c
@@ -1032,11 +1032,22 @@ int ath11k_pci_qca6x90_powerup(struct ath11k_pci *ab_pci)
 	ab_pci->init_done = false;
 	ath11k_pci_sw_reset(ab_pci->ab, true);
 
+	/* disable L0sL1, write 0x40 to link_ctrl */
+	pci_read_config_byte(ab_pci->pdev, 0x80, &ab_pci->aspm);
+	pci_write_config_byte(ab_pci->pdev, 0x80, ab_pci->aspm & 0xfc);
+	ab_pci->restore_aspm = true;
+
 	return ath11k_pci_start_mhi(ab_pci);
 }
 
 static void ath11k_pci_qca6x90_powerdown(struct ath11k_pci *ab_pci)
 {
+	/* recover aspm */
+	if (ab_pci->restore_aspm) {
+		pci_write_config_byte(ab_pci->pdev, 0x80, ab_pci->aspm);
+		ab_pci->restore_aspm = false;
+	}
+
 	ath11k_pci_stop_mhi(ab_pci);
 	ab_pci->init_done = false;
 	ath11k_pci_force_wake(ab_pci->ab);
@@ -1273,6 +1284,12 @@ static int ath11k_pci_start(struct ath11k_base *ab)
 	ar_pci = ath11k_pci_priv(ab);
 	ar_pci->init_done = true;
 
+	/* recover aspm */
+	if (ar_pci->restore_aspm) {
+		pci_write_config_byte(ar_pci->pdev, 0x80, ar_pci->aspm);
+		ar_pci->restore_aspm = false;
+	}
+
 	ath11k_pci_ce_irqs_enable(ab);
 	ath11k_ce_rx_post_buf(ab);
 
diff --git a/drivers/net/wireless/ath/ath11k/pci.h b/drivers/net/wireless/ath/ath11k/pci.h
index 75e5d30..e441fe4 100644
--- a/drivers/net/wireless/ath/ath11k/pci.h
+++ b/drivers/net/wireless/ath/ath11k/pci.h
@@ -102,6 +102,8 @@ struct ath11k_pci {
 	u32 register_window;
 	spinlock_t window_lock;
 	bool init_done;
+	u8 aspm;
+	bool restore_aspm;
 };
 
 int ath11k_pci_get_user_msi_assignment(struct ath11k_pci *ar_pci, char *user_name,
-- 
2.7.4

