From 0b552e09f558a0d897bfa31e59ad68da0362e39d Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Fri, 3 Apr 2020 10:03:10 +0800
Subject: [PATCH 092/124] ath11k: reset MHI during power down and power up

For QCA6390, normal power up an dpower down can't bring MHI
to a workable state. This happens especially in warm reboot
and rmmod and insmod. Host needs to write a few registers to
bring MHI to normal state.

Tested-on: QCA6390 WLAN.HST.1.0.1-01230-QCAHSTSWPLZ_V2_TO_X86-1

Change-Id: I9bf0e1cfb5fc840f59b2f74de97f53d0e14a037c
Signed-off-by: Carl Huang <cjhuang@codeauroro.org>
---
 drivers/net/wireless/ath/ath11k/mhi.c |  47 ++++++++++++++-
 drivers/net/wireless/ath/ath11k/mhi.h |  11 ++++
 drivers/net/wireless/ath/ath11k/pci.c | 105 ++++++++++++++++++++++++++++++++--
 drivers/net/wireless/ath/ath11k/pci.h |  17 ++++++
 4 files changed, 174 insertions(+), 6 deletions(-)

diff --git a/drivers/net/wireless/ath/ath11k/mhi.c b/drivers/net/wireless/ath/ath11k/mhi.c
index 1f67646..3c883eb6 100644
--- a/drivers/net/wireless/ath/ath11k/mhi.c
+++ b/drivers/net/wireless/ath/ath11k/mhi.c
@@ -106,6 +106,51 @@ static struct mhi_controller_config ath11k_mhi_config = {
 	.event_cfg = ath11k_mhi_events,
 };
 
+void ath11k_mhi_set_mhictrl_reset(struct ath11k_base *ab)
+{
+	u32 val;
+
+	val = ath11k_pci_read32(ab, MHISTATUS);
+
+	ath11k_dbg(ab, ATH11K_DBG_PCI, "MHISTATUS 0x%x\n", val);
+
+	/* Observed on Hastings that after SOC_GLOBAL_RESET, MHISTATUS
+	 * has SYSERR bit set and thus need to set MHICTRL_RESET
+	 * to clear SYSERR.
+	 */
+	ath11k_pci_write32(ab, MHICTRL, MHICTRL_RESET_MASK);
+
+	mdelay(10);
+}
+
+static void mhi_reset_txvecdb(struct ath11k_base *ab)
+{
+	ath11k_pci_write32(ab, PCIE_TXVECDB, 0);
+}
+
+static void mhi_reset_txvecstatus(struct ath11k_base *ab)
+{
+	ath11k_pci_write32(ab, PCIE_TXVECSTATUS, 0);
+}
+
+static void mhi_reset_rxvecdb(struct ath11k_base *ab)
+{
+	ath11k_pci_write32(ab, PCIE_RXVECDB, 0);
+}
+
+static void mhi_reset_rxvecstatus(struct ath11k_base *ab)
+{
+	ath11k_pci_write32(ab, PCIE_RXVECSTATUS, 0);
+}
+
+void ath11k_mhi_clear_vector(struct ath11k_base *ab)
+{
+	mhi_reset_txvecdb(ab);
+	mhi_reset_txvecstatus(ab);
+	mhi_reset_rxvecdb(ab);
+	mhi_reset_rxvecstatus(ab);
+}
+
 static int ath11k_pci_get_mhi_msi(struct ath11k_pci *ab_pci)
 {
 	struct ath11k_base *ab = ab_pci->ab;
@@ -410,7 +455,7 @@ int ath11k_pci_start_mhi(struct ath11k_pci *ab_pci)
 
 void ath11k_pci_stop_mhi(struct ath11k_pci *ab_pci)
 {
-	ath11k_pci_set_mhi_state(ab_pci, ATH11K_MHI_RESUME);
+	ath11k_pci_wakeup_mhi(ab_pci);
 	ath11k_pci_set_mhi_state(ab_pci, ATH11K_MHI_POWER_OFF);
 	ath11k_pci_set_mhi_state(ab_pci, ATH11K_MHI_DEINIT);
 }
diff --git a/drivers/net/wireless/ath/ath11k/mhi.h b/drivers/net/wireless/ath/ath11k/mhi.h
index ab8bc80..5ce7202 100644
--- a/drivers/net/wireless/ath/ath11k/mhi.h
+++ b/drivers/net/wireless/ath/ath11k/mhi.h
@@ -8,6 +8,15 @@
 #include "pci.h"
 #define ATH11K_PCI_FW_FILE_NAME		"amss.bin"
 
+#define PCIE_TXVECDB (0x360)
+#define PCIE_TXVECSTATUS (0x368)
+#define PCIE_RXVECDB (0x394)
+#define PCIE_RXVECSTATUS (0x39C)
+
+#define MHISTATUS (0x48)
+#define MHICTRL (0x38)
+#define MHICTRL_RESET_MASK (0x2)
+
 enum ath11k_mhi_state {
 	ATH11K_MHI_INIT,
 	ATH11K_MHI_DEINIT,
@@ -30,4 +39,6 @@ void ath11k_pci_resume_mhi(struct ath11k_pci *ar_pci);
 void ath11k_pci_force_mhi_rddm(struct ath11k_pci *ar_pci);
 void ath11k_pci_wakeup_mhi(struct ath11k_pci *ar_pci);
 void ath11k_pci_release_mhi(struct ath11k_pci *ar_pci);
+void ath11k_mhi_set_mhictrl_reset(struct ath11k_base *ab);
+void ath11k_mhi_clear_vector(struct ath11k_base *ab);
 #endif
diff --git a/drivers/net/wireless/ath/ath11k/pci.c b/drivers/net/wireless/ath/ath11k/pci.c
index e5e6987..2b885e6 100644
--- a/drivers/net/wireless/ath/ath11k/pci.c
+++ b/drivers/net/wireless/ath/ath11k/pci.c
@@ -327,14 +327,16 @@ static inline struct ath11k_pci *ath11k_pci_priv(struct ath11k_base *ab)
 	return (struct ath11k_pci *)ab->drv_priv;
 }
 
-static void ath11k_pci_write32(struct ath11k_base *ab, u32 offset, u32 value)
+void ath11k_pci_write32(struct ath11k_base *ab, u32 offset, u32 value)
 {
 	struct ath11k_pci *ab_pci = ath11k_pci_priv(ab);
 
 	/* for offset beyond BAR + 4K - 32, may
-	 * need to wakeup MHI to access.
+	 * need to wakeup MHI to access. But if init_done
+	 * isn't, no need to do wake_up.
 	 */
 	if (ab->hw_params.wakeup_mhi &&
+	    ab_pci->init_done &&
 	    offset >= ACCESS_ALWAYS_OFF)
 		ath11k_pci_wakeup_mhi(ab_pci);
 
@@ -349,19 +351,22 @@ static void ath11k_pci_write32(struct ath11k_base *ab, u32 offset, u32 value)
 	}
 
 	if (ab->hw_params.wakeup_mhi &&
+	    ab_pci->init_done &&
 	    offset >= ACCESS_ALWAYS_OFF)
 		ath11k_pci_release_mhi(ab_pci);
 }
 
-static u32 ath11k_pci_read32(struct ath11k_base *ab, u32 offset)
+u32 ath11k_pci_read32(struct ath11k_base *ab, u32 offset)
 {
 	struct ath11k_pci *ab_pci = ath11k_pci_priv(ab);
 	u32 val;
 
 	/* for offset beyond BAR + 4K - 32, may
-	 * need to wakeup MHI to access.
+	 * need to wakeup MHI to access. But if init_done
+	 * isn't, no need to do wake_up.
 	 */
 	if (ab->hw_params.wakeup_mhi &&
+	    ab_pci->init_done &&
 	    offset >= ACCESS_ALWAYS_OFF)
 		ath11k_pci_wakeup_mhi(ab_pci);
 
@@ -376,12 +381,91 @@ static u32 ath11k_pci_read32(struct ath11k_base *ab, u32 offset)
 	}
 
 	if (ab->hw_params.wakeup_mhi &&
+	    ab_pci->init_done &&
 	    offset >= ACCESS_ALWAYS_OFF)
 		ath11k_pci_release_mhi(ab_pci);
 
 	return val;
 }
 
+void ath11k_pci_soc_global_reset(struct ath11k_base *ab)
+{
+	u32 val;
+	u32 delay;
+
+	val = ath11k_pci_read32(ab, PCIE_SOC_GLOBAL_RESET);
+
+	val |= PCIE_SOC_GLOBAL_RESET_V;
+
+	ath11k_pci_write32(ab, PCIE_SOC_GLOBAL_RESET, val);
+
+	/* TODO: exact time to sleep is uncertain */
+	delay = 10;
+	mdelay(delay);
+
+	/* Need to toggle V bit back otherwise stuck in reset status */
+	val &= ~PCIE_SOC_GLOBAL_RESET_V;
+
+	ath11k_pci_write32(ab, PCIE_SOC_GLOBAL_RESET, val);
+
+	mdelay(delay);
+
+	val = ath11k_pci_read32(ab, PCIE_SOC_GLOBAL_RESET);
+	if (val == 0xffffffff)
+		ath11k_err(ab, "%s link down error\n", __func__);
+}
+
+void ath11k_pci_clear_dbg_registers(struct ath11k_base *ab)
+{
+	u32 val;
+
+	/* read cookie */
+	val = ath11k_pci_read32(ab, PCIE_Q6_COOKIE_ADDR);
+	ath11k_dbg(ab, ATH11K_DBG_PCI, "cookie:0x%x\n", val);
+
+	val = ath11k_pci_read32(ab, WLAON_WARM_SW_ENTRY);
+	ath11k_dbg(ab, ATH11K_DBG_PCI, "WLAON_WARM_SW_ENTRY 0x%x\n", val);
+
+	/* TODO: exact time to sleep is uncertain */
+	mdelay(10);
+
+	/* write 0 to WLAON_WARM_SW_ENTRY to prevent Q6 from
+	 * continuing warm path and entering dead loop.
+	 */
+	ath11k_pci_write32(ab, WLAON_WARM_SW_ENTRY, 0);
+	mdelay(10);
+
+	val = ath11k_pci_read32(ab, WLAON_WARM_SW_ENTRY);
+	ath11k_dbg(ab, ATH11K_DBG_PCI, "WLAON_WARM_SW_ENTRY 0x%x\n", val);
+
+	/* A read clear register. clear the register to prevent
+	 * Q6 from entering wrong code path.
+	 */
+	val = ath11k_pci_read32(ab, WLAON_SOC_RESET_CAUSE_REG);
+	ath11k_dbg(ab, ATH11K_DBG_PCI, "soc reset cause:%d\n", val);
+}
+
+void ath11k_pci_force_wake(struct ath11k_base *ab)
+{
+	ath11k_pci_write32(ab, PCIE_SOC_WAKE_PCIE_LOCAL_REG, 1);
+	mdelay(5);
+}
+
+void ath11k_pci_force_wake_release(struct ath11k_base *ab)
+{
+	ath11k_pci_write32(ab, PCIE_SOC_WAKE_PCIE_LOCAL_REG, 0);
+	mdelay(5);
+}
+
+void ath11k_pci_sw_reset(struct ath11k_base *ab)
+{
+	ath11k_pci_soc_global_reset(ab);
+	ath11k_mhi_clear_vector(ab);
+	ath11k_pci_soc_global_reset(ab);
+	ath11k_mhi_set_mhictrl_reset(ab);
+	ath11k_pci_clear_dbg_registers(ab);
+}
+
 int ath11k_pci_get_msi_irq(struct device *dev, unsigned int vector)
 {
 	struct pci_dev *pci_dev = to_pci_dev(dev);
@@ -784,12 +868,18 @@ static void ath11k_pci_ce_irqs_enable(struct ath11k_base *ab)
 
 int ath11k_pci_qca6x90_powerup(struct ath11k_pci *ab_pci)
 {
+	ab_pci->init_done = false;
+	ath11k_pci_sw_reset(ab_pci->ab);
+
 	return ath11k_pci_start_mhi(ab_pci);
 }
 
 static void ath11k_pci_qca6x90_powerdown(struct ath11k_pci *ab_pci)
 {
 	ath11k_pci_stop_mhi(ab_pci);
+	ab_pci->init_done = false;
+	ath11k_pci_force_wake(ab_pci->ab);
+	ath11k_pci_sw_reset(ab_pci->ab);
 }
 
 static int ath11k_pci_get_msi_assignment(struct ath11k_pci *ab_pci)
@@ -1017,10 +1107,15 @@ static void ath11k_pci_stop(struct ath11k_base *ab)
 
 static int ath11k_pci_start(struct ath11k_base *ab)
 {
+	struct ath11k_pci *ar_pci;
+
+	ar_pci = ath11k_pci_priv(ab);
+	ar_pci->init_done = true;
+
 	ath11k_pci_ce_irqs_enable(ab);
 	ath11k_ce_rx_post_buf(ab);
-	/* Bring up other components as appropriate */
 
+	/* Bring up other components as appropriate */
 	return 0;
 }
 
diff --git a/drivers/net/wireless/ath/ath11k/pci.h b/drivers/net/wireless/ath/ath11k/pci.h
index ea80b50..a4f2caa 100644
--- a/drivers/net/wireless/ath/ath11k/pci.h
+++ b/drivers/net/wireless/ath/ath11k/pci.h
@@ -23,6 +23,20 @@
 #define WINDOW_START MAX_UNWINDOWED_ADDRESS
 #define WINDOW_RANGE_MASK 0x7FFFF
 
+#define PCIE_SOC_GLOBAL_RESET (0x3008)
+#define PCIE_SOC_GLOBAL_RESET_V 1
+
+#define WLAON_WARM_SW_ENTRY (0x1f80504)
+#define WLAON_SOC_RESET_CAUSE_REG   (0x01f8060c)
+
+#define PCIE_Q6_COOKIE_ADDR         (0x01F80500)
+#define PCIE_Q6_COOKIE_DATA         (0xC0000000)
+
+/* Register to wake the UMAC from power collapse */
+#define PCIE_SCRATCH_0_SOC_PCIE_REG 0x4040
+/* Register used for handshake mechanism to validate UMAC is awake */
+#define PCIE_SOC_WAKE_PCIE_LOCAL_REG 0x3004
+
 /* BAR0 + 4k is always accessible, and no
  * need to force wakeup.
  * 4K - 32 = 0xFE0
@@ -57,6 +71,7 @@ struct ath11k_pci {
 	unsigned long mhi_state;
 	u32 register_window;
 	spinlock_t window_lock;
+	bool init_done;
 };
 
 int ath11k_pci_get_user_msi_assignment(struct ath11k_pci *ar_pci, char *user_name,
@@ -64,4 +79,6 @@ int ath11k_pci_get_user_msi_assignment(struct ath11k_pci *ar_pci, char *user_nam
 				       u32 *base_vector);
 
 int ath11k_pci_get_msi_irq(struct device *dev, unsigned int vector);
+void ath11k_pci_write32(struct ath11k_base *ab, u32 offset, u32 value);
+u32 ath11k_pci_read32(struct ath11k_base *ab, u32 offset);
 #endif
-- 
2.7.4

