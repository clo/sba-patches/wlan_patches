From d4ad911894e88ffe18234f36caef416191f02338 Mon Sep 17 00:00:00 2001
From: Carl Huang <cjhuang@codeaurora.org>
Date: Tue, 3 Dec 2019 16:49:55 +0200
Subject: [PATCH 058/124] ath11k: put hardware to dbs mode

For QCA6390, host puts hardware to dbs mode by default so both
2G and 5G band can be used. Otherwise only 5G band can be used.
QCA6390 doesn't provide band_to_mac configuration and firmware
will do the band_to_mac map.

Tested-on: QCA6390 WLAN.HST.1.0.1-01230-QCAHSTSWPLZ_V2_TO_X86-1

Change-Id: Ife6a1b5dfce8b6e0c57eb672d8934ecd0d6c802a
Signed-off-by: Carl Huang <cjhuang@codeaurora.org>
Signed-off-by: Kalle Valo <kvalo@codeaurora.org>
---
 drivers/net/wireless/ath/ath11k/core.c | 11 +++++++++++
 drivers/net/wireless/ath/ath11k/hw.h   |  7 +++++++
 drivers/net/wireless/ath/ath11k/wmi.c  | 29 ++++++++++++++++++++++++++++-
 drivers/net/wireless/ath/ath11k/wmi.h  |  1 +
 4 files changed, 47 insertions(+), 1 deletion(-)

diff --git a/drivers/net/wireless/ath/ath11k/core.c b/drivers/net/wireless/ath/ath11k/core.c
index df1e0ce..123bed8 100644
--- a/drivers/net/wireless/ath/ath11k/core.c
+++ b/drivers/net/wireless/ath/ath11k/core.c
@@ -30,6 +30,7 @@ static const struct ath11k_hw_params ath11k_hw_params_list[] = {
 		.internal_sleep_clock = false,
 		.single_pdev_only = false,
 		.hw_ops = &ath11k_hw_ops_ipq8074,
+		.misc_caps = MISC_CAPS_BAND_TO_MAC,
 	},
 	{
 		.name = "qca6390",
@@ -37,6 +38,7 @@ static const struct ath11k_hw_params ath11k_hw_params_list[] = {
 		.internal_sleep_clock = true,
 		.single_pdev_only = true,
 		.hw_ops = &ath11k_hw_ops_qca6x90,
+		.misc_caps = 0,
 	}
 };
 
@@ -521,6 +523,15 @@ static int ath11k_core_start(struct ath11k_base *ab,
 		goto err_reo_cleanup;
 	}
 
+	// put hardware to DBS mode
+	if (ab->hw_params.single_pdev_only) {
+		ret = ath11k_wmi_set_hw_mode(ab, 1);
+		if (ret) {
+			ath11k_err(ab, "failed to send dbs mode: %d\n", ret);
+			goto err_hif_stop;
+		}
+	}
+
 	ret = ath11k_dp_tx_htt_h2t_ver_req_msg(ab);
 	if (ret) {
 		ath11k_err(ab, "failed to send htt version request message: %d\n",
diff --git a/drivers/net/wireless/ath/ath11k/hw.h b/drivers/net/wireless/ath/ath11k/hw.h
index 839681d..609f004 100644
--- a/drivers/net/wireless/ath/ath11k/hw.h
+++ b/drivers/net/wireless/ath/ath11k/hw.h
@@ -80,6 +80,8 @@
 #define ATH11K_DEFAULT_BOARD_FILE	"bdwlan.bin"
 #define ATH11K_DEFAULT_CAL_FILE		"caldata.bin"
 
+#define MISC_CAPS_BAND_TO_MAC       BIT(0)
+
 enum ath11k_hw_rate_cck {
 	ATH11K_HW_RATE_CCK_LP_11M = 0,
 	ATH11K_HW_RATE_CCK_LP_5_5M,
@@ -124,6 +126,11 @@ struct ath11k_hw_params {
 	bool internal_sleep_clock;
 	bool single_pdev_only;
 	const struct ath11k_hw_ops *hw_ops;
+	/* there are some small differences from chip to chip,
+	 * and driver needs to address the difference only once.
+	 * misc_caps is for these differences.
+	 */
+	u32 misc_caps;
 };
 
 struct ath11k_fw_ie {
diff --git a/drivers/net/wireless/ath/ath11k/wmi.c b/drivers/net/wireless/ath/ath11k/wmi.c
index 203cecb..d5a33c1 100644
--- a/drivers/net/wireless/ath/ath11k/wmi.c
+++ b/drivers/net/wireless/ath/ath11k/wmi.c
@@ -3200,6 +3200,34 @@ int ath11k_wmi_wait_for_unified_ready(struct ath11k_base *ab)
 	return 0;
 }
 
+int ath11k_wmi_set_hw_mode(struct ath11k_base *ab, int hw_mode_index)
+{
+	struct wmi_pdev_set_hw_mode_cmd_param *cmd;
+	struct sk_buff *skb;
+	struct ath11k_wmi_base *wmi_ab = &ab->wmi_ab;
+	int len;
+	int ret;
+
+	len = sizeof(*cmd);
+
+	skb = ath11k_wmi_alloc_skb(wmi_ab, len);
+	cmd = (struct wmi_pdev_set_hw_mode_cmd_param *)skb->data;
+
+	cmd->tlv_header = FIELD_PREP(WMI_TLV_TAG, WMI_TAG_PDEV_SET_HW_MODE_CMD) |
+			  FIELD_PREP(WMI_TLV_LEN, sizeof(*cmd) - TLV_HDR_SIZE);
+
+	cmd->pdev_id = WMI_PDEV_ID_SOC;
+	cmd->hw_mode_index = hw_mode_index;
+
+	ret = ath11k_wmi_cmd_send(&wmi_ab->wmi[0], skb, WMI_PDEV_SET_HW_MODE_CMDID);
+	if (ret) {
+		ath11k_warn(ab, "failed to send WMI_PDEV_SET_HW_MODE_CMDID\n");
+		dev_kfree_skb(skb);
+	}
+
+	return ret;
+}
+
 int ath11k_wmi_cmd_init(struct ath11k_base *ab)
 {
 	struct ath11k_wmi_base *wmi_sc = &ab->wmi_ab;
@@ -3223,7 +3251,6 @@ int ath11k_wmi_cmd_init(struct ath11k_base *ab)
 		init_param.hw_mode_id = WMI_HOST_HW_MODE_MAX;
 
 	init_param.num_band_to_mac = ab->num_radios;
-
 	ath11k_fill_band_to_mac_param(ab, init_param.band_to_mac);
 
 	return ath11k_init_cmd_send(&wmi_sc->wmi[0], &init_param);
diff --git a/drivers/net/wireless/ath/ath11k/wmi.h b/drivers/net/wireless/ath/ath11k/wmi.h
index b9f3e55..f5df214 100644
--- a/drivers/net/wireless/ath/ath11k/wmi.h
+++ b/drivers/net/wireless/ath/ath11k/wmi.h
@@ -4941,4 +4941,5 @@ int ath11k_wmi_send_obss_color_collision_cfg_cmd(struct ath11k *ar, u32 vdev_id,
 int ath11k_wmi_send_bss_color_change_enable_cmd(struct ath11k *ar, u32 vdev_id,
 						bool enable);
 int ath11k_wmi_pdev_lro_cfg(struct ath11k *ar, int pdev_id);
+int ath11k_wmi_set_hw_mode(struct ath11k_base *ab, int hw_mode_index);
 #endif
-- 
2.7.4

