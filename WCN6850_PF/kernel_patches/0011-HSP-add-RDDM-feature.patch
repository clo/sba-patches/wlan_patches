From 69ae728b8c6bacd1b1aa16b318c892b9a8304f7b Mon Sep 17 00:00:00 2001
From: bings <bings@codeaurora.org>
Date: Fri, 15 May 2020 06:05:23 -0400
Subject: [PATCH 60/63] HSP: add RDDM feature

Host driver uses this feature to download firmware RAM/paging
informations.

Signed-off-by: Kang Xu <kangxu@codeaurora.org>
---
 drivers/bus/mhi/core/boot.c     | 67 ++++++++++++++++++++++++++++-----
 drivers/bus/mhi/core/init.c     |  2 +
 drivers/bus/mhi/core/internal.h |  2 +
 include/linux/mhi.h             | 15 ++++++++
 4 files changed, 77 insertions(+), 9 deletions(-)

diff --git a/drivers/bus/mhi/core/boot.c b/drivers/bus/mhi/core/boot.c
index 17c636b4bc6e..2cde7d292b83 100644
--- a/drivers/bus/mhi/core/boot.c
+++ b/drivers/bus/mhi/core/boot.c
@@ -18,15 +18,47 @@
 #include <linux/wait.h>
 #include "internal.h"
 
+static void mhi_coredump_rddm_set(struct mhi_controller *mhi_cntrl)
+{
+	struct mhi_fw_crash_data *crash_data = &mhi_cntrl->fw_crash_data;
+	u32 entries = mhi_cntrl->rddm_image->entries;
+	u32 num;
+
+	mhi_cntrl->rddm_vec_entry_num = DIV_ROUND_UP(BHIE_RDDM_DUMP_SIZE,
+					mhi_cntrl->seg_len);
+	num = mhi_cntrl->rddm_vec_entry_num;
+
+	crash_data->ramdump_buf_len = (entries-1) * mhi_cntrl->seg_len +
+				      num * sizeof(struct bhi_vec_entry);
+	dev_info(&mhi_cntrl->mhi_dev->dev,
+		 "%s FW RDDM dump buffer len=%lu\n",
+		 __func__, crash_data->ramdump_buf_len);
+}
+
+static void mhi_coredump_fw_paging_set(struct mhi_controller *mhi_cntrl,
+				      size_t firmware_size)
+{
+	struct mhi_fw_crash_data *crash_data = &mhi_cntrl->fw_crash_data;
+	u32 entries = mhi_cntrl->fbc_image->entries;
+	u32 num;
+
+	mhi_cntrl->fw_vec_entry_num = DIV_ROUND_UP(firmware_size,
+						   mhi_cntrl->seg_len);
+	num = mhi_cntrl->fw_vec_entry_num;
+
+	crash_data->fwdump_buf_len = (entries-1) * mhi_cntrl->seg_len +
+				     num*sizeof(struct bhi_vec_entry);
+	dev_info(&mhi_cntrl->mhi_dev->dev,
+		 "%s FW paging dump buffer len=%lu\n",
+		 __func__, crash_data->fwdump_buf_len);
+}
+
 /* Setup RDDM vector table for RDDM transfer and program RXVEC */
 void mhi_rddm_prepare(struct mhi_controller *mhi_cntrl,
 		      struct image_info *img_info)
 {
 	struct mhi_buf *mhi_buf = img_info->mhi_buf;
 	struct bhi_vec_entry *bhi_vec = img_info->bhi_vec;
-	void __iomem *base = mhi_cntrl->bhie;
-	struct device *dev = &mhi_cntrl->mhi_dev->dev;
-	u32 sequence_id;
 	unsigned int i;
 
 	for (i = 0; i < img_info->entries - 1; i++, mhi_buf++, bhi_vec++) {
@@ -34,15 +66,27 @@ void mhi_rddm_prepare(struct mhi_controller *mhi_cntrl,
 		bhi_vec->size = mhi_buf->len;
 	}
 
-	dev_dbg(dev, "BHIe programming for RDDM\n");
+	mhi_coredump_rddm_set(mhi_cntrl);
+}
+
+void mhi_rddm_set_vector_table(struct mhi_controller *mhi_cntrl)
+{
+	struct device *dev = &mhi_cntrl->mhi_dev->dev;
+	struct image_info *rddm_image = mhi_cntrl->rddm_image;
+	u32 entries = rddm_image->entries;
+	struct mhi_buf *last_buf = &rddm_image->mhi_buf[entries - 1];
+	void __iomem *base = mhi_cntrl->bhie;
+	u32 sequence_id;
+
+	dev_info(dev, "BHIe programming for RDDM\n");
 
 	mhi_write_reg(mhi_cntrl, base, BHIE_RXVECADDR_HIGH_OFFS,
-		      upper_32_bits(mhi_buf->dma_addr));
+		      upper_32_bits(last_buf->dma_addr));
 
 	mhi_write_reg(mhi_cntrl, base, BHIE_RXVECADDR_LOW_OFFS,
-		      lower_32_bits(mhi_buf->dma_addr));
+		      lower_32_bits(last_buf->dma_addr));
 
-	mhi_write_reg(mhi_cntrl, base, BHIE_RXVECSIZE_OFFS, mhi_buf->len);
+	mhi_write_reg(mhi_cntrl, base, BHIE_RXVECSIZE_OFFS, last_buf->len);
 	sequence_id = prandom_u32() & BHIE_RXVECSTATUS_SEQNUM_BMSK;
 
 	if (unlikely(!sequence_id))
@@ -52,8 +96,8 @@ void mhi_rddm_prepare(struct mhi_controller *mhi_cntrl,
 			    BHIE_RXVECDB_SEQNUM_BMSK, BHIE_RXVECDB_SEQNUM_SHFT,
 			    sequence_id);
 
-	dev_dbg(dev, "Address: %p and len: 0x%zx sequence: %u\n",
-		&mhi_buf->dma_addr, mhi_buf->len, sequence_id);
+	dev_info(dev, "Address: %p and len: 0x%lx sequence: %u\n",
+		 &last_buf->dma_addr, last_buf->len, sequence_id);
 }
 
 /* Collect RDDM buffer during kernel panic */
@@ -153,10 +197,13 @@ int mhi_download_rddm_img(struct mhi_controller *mhi_cntrl, bool in_panic)
 {
 	void __iomem *base = mhi_cntrl->bhie;
 	u32 rx_status;
+	struct device *dev = &mhi_cntrl->mhi_dev->dev;
 
 	if (in_panic)
 		return __mhi_download_rddm_in_panic(mhi_cntrl);
 
+	mhi_rddm_set_vector_table(mhi_cntrl);
+
 	/* Wait for the image download to complete */
 	wait_event_timeout(mhi_cntrl->state_event,
 			   mhi_read_reg_field(mhi_cntrl, base,
@@ -166,6 +213,7 @@ int mhi_download_rddm_img(struct mhi_controller *mhi_cntrl, bool in_panic)
 					      &rx_status) || rx_status,
 			   msecs_to_jiffies(mhi_cntrl->timeout_ms));
 
+	dev_info(dev, "RXVEC_STATUS: 0x%x\n", rx_status);
 	return (rx_status == BHIE_RXVECSTATUS_STATUS_XFER_COMPL) ? 0 : -EIO;
 }
 EXPORT_SYMBOL_GPL(mhi_download_rddm_img);
@@ -482,6 +530,7 @@ void mhi_fw_load_handler(struct mhi_controller *mhi_cntrl)
 			       /* Vector table is the last entry */
 			       &image_info->mhi_buf[image_info->entries - 1]);
 
+	mhi_coredump_fw_paging_set(mhi_cntrl, firmware->size);
 	release_firmware(firmware);
 
 	return;
diff --git a/drivers/bus/mhi/core/init.c b/drivers/bus/mhi/core/init.c
index a713e069015d..279ee71b3423 100644
--- a/drivers/bus/mhi/core/init.c
+++ b/drivers/bus/mhi/core/init.c
@@ -909,6 +909,8 @@ int mhi_register_controller(struct mhi_controller *mhi_cntrl,
 
 	mhi_cntrl->mhi_dev = mhi_dev;
 
+	mhi_cntrl->rddm_size = BHIE_RDDM_DUMP_SIZE;
+
 	return 0;
 
 error_add_dev:
diff --git a/drivers/bus/mhi/core/internal.h b/drivers/bus/mhi/core/internal.h
index 4919a43aae26..f94b0ce13873 100644
--- a/drivers/bus/mhi/core/internal.h
+++ b/drivers/bus/mhi/core/internal.h
@@ -685,4 +685,6 @@ void mhi_unmap_single_no_bb(struct mhi_controller *mhi_cntrl,
 void mhi_unmap_single_use_bb(struct mhi_controller *mhi_cntrl,
 			     struct mhi_buf_info *buf_info);
 
+#define BHIE_RDDM_DUMP_SIZE                (4 * 1024 * 1024)
+
 #endif /* _MHI_INT_H */
diff --git a/include/linux/mhi.h b/include/linux/mhi.h
index cda7305bcf90..1c2b4b196c9a 100644
--- a/include/linux/mhi.h
+++ b/include/linux/mhi.h
@@ -283,6 +283,13 @@ struct mhi_controller_config {
 	bool m2_no_db;
 };
 
+struct mhi_fw_crash_data {
+	u8 *fwdump_buf;
+	size_t fwdump_buf_len;
+	u8 *ramdump_buf;
+	size_t ramdump_buf_len;
+};
+
 /**
  * struct mhi_controller - Master MHI controller structure
  * @cntrl_dev: Pointer to the struct device of physical bus acting as the MHI
@@ -431,6 +438,14 @@ struct mhi_controller {
 	bool fbc_download;
 	bool pre_init;
 	bool wake_set;
+
+	/* controller specific data */
+	void *priv_data;
+
+	/*RDDM related*/
+	struct mhi_fw_crash_data fw_crash_data;
+	u32 fw_vec_entry_num;
+	u32 rddm_vec_entry_num;
 };
 
 /**
-- 
2.17.1

