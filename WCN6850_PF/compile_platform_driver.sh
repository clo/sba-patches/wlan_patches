#
# To generate platform/cld driver and get ko
#

cd ./platform_patches
git init .
git am 0001-Initialize-platform-dirver-on-6490-for-X86-device.patch
git am 0002-Platform-porting-cnss_genl-to-receive-fw-log.patch

#compile platform driver cnss.ko and hsp_diag.ko
make -j8

cd ../cld/qcacld-3.0
patch -p1 < ../../Patch-Makefile.patch
make CONFIG_CNSS2=y CONFIG_CNSS_QCA6490=y CONFIG_SLUB_DEBUG_ON=n CONFIG_SLUB_DEBUG=n CONFIG_LINUX_QCMBR=y DEVELOPER_DISABLE_BUILD_TIMESTAMP=y CONFIG_RX_FISA=n -j8

